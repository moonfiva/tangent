PACKAGE := tangentGo
BIN := tangent
GO_FILES := $(shell ls *.go)
ZIPDIR := www
DOCDIR := doc
LICENSE := LICENSE.txt
README := readme.txt
READMEJP := readmejp.txt
READMEORG := readme.md
READMEJPORG := readmejp.md
DOCS := $(DOCDIR)/$(LICENSE) $(DOCDIR)/$(README) $(DOCDIR)/$(READMEJP)
COMDIR := commands
COMTXT := commands.txt
COMWINTXT := windows.txt
COMS := $(COMDIR)/$(COMTXT) $(COMDIR)/$(COMWINTXT)
SAMPLE := $(shell ls sample*.xml) $(shell find sample/ -type f -name '*.csv' -print)
MATERIALS := materials.txt
DTD := tangent.dtd
RUNLINUX := run.tcl
RUNWIN := run.bat
RUNMAC := run.command
DIRLINUX32 := linux32
DIRLINUX64 := linux64
DIRLINUXARM := linuxarm
DIRMAC86 := mac86
DIRMACARM := macarm
DIRWIN32 := win32
DIRWIN64 := win64
DIRALL := $(DIRLINUX32) $(DIRLINUX64) $(DIRLINUXARM) $(DIRMAC86) $(DIRMACARM) $(DIRWIN32) $(DIRWIN64)
BINARIES := $(BIN) $(DIRLINUX32)/$(BIN) $(DIRLINUX64)/$(BIN) $(DIRLINUXARM)/$(BIN) $(DIRMAC86)/$(BIN) $(DIRMACARM)/$(BIN) $(DIRWIN32)/$(BIN).exe $(DIRWIN64)/$(BIN).exe
ZIPLINUX32 := $(ZIPDIR)/$(PACKAGE)-linux32.zip
ZIPLINUX64 := $(ZIPDIR)/$(PACKAGE)-linux64.zip
ZIPLINUXARM := $(ZIPDIR)/$(PACKAGE)-linuxarm.zip
ZIPMAC86 := $(ZIPDIR)/$(PACKAGE)-mac86.zip
ZIPMACARM := $(ZIPDIR)/$(PACKAGE)-macarm.zip
ZIPWIN32 := $(ZIPDIR)/$(PACKAGE)-win32.zip
ZIPWIN64 := $(ZIPDIR)/$(PACKAGE)-win64.zip
ARCHIVES := $(ZIPLINUX32) $(ZIPLINUX64) $(ZIPLINUXARM) $(ZIPMAC86) $(ZIPMACARM) $(ZIPWIN32) $(ZIPWIN64)
BASEZIP := base.zip
VERTXT := $(ZIPDIR)/version.txt
VERPNG := $(ZIPDIR)/version.png
VERSIONS := $(VERTXT) $(VERPNG)
MARKDOWN := pandoc -f markdown -t plain
CP := cp -pf
WINCONVERTER := nkf -Lw -W --oc=CP932
# windows
define WINCONVERT
	$(WINCONVERTER) $(1) >$(2)/$(1)

endef # Don't remove the above blank line.

# top target
.PHONY: development version zip all

development : $(BIN) $(DOCS)

version : $(VERSIONS)

zip : $(ARCHIVES)

all : development zip version

# development is assumed on 64bit Linux PC
$(BIN): $(GO_FILES)
	$(MAKE) build-base OS=linux ARCH=amd64 OUT=$(BIN)

# target machines
.PHONY: linux32 linux64 linuxarm macarm mac86 win32 win64

# linux32
linux32 : $(ZIPLINUX32)

$(DIRLINUX32)/$(BIN): $(GO_FILES)
	$(MAKE) build-base OS=linux ARCH=386 OUT=$(DIRLINUX32)/$(BIN)

$(ZIPLINUX32) : $(DIRLINUX32)/$(BIN) $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX)
	mkdir -p $(DIRLINUX32)
	$(CP) --parents $(DOCS) $(MATERIALS) $(RUNLINUX) $(DIRLINUX32)/
	$(CP) $(BASEZIP) target.zip
	cd $(DIRLINUX32) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPLINUX32)

# linux64
linux64 : $(ZIPLINUX64)

$(DIRLINUX64)/$(BIN): $(GO_FILES)
	$(MAKE) build-base OS=linux ARCH=amd64 OUT=$(DIRLINUX64)/$(BIN)

$(ZIPLINUX64) : $(DIRLINUX64)/$(BIN) $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX)
	mkdir -p $(DIRLINUX64)
	$(CP) --parents $(DOCS) $(MATERIALS) $(RUNLINUX) $(DIRLINUX64)/
	$(CP) $(BASEZIP) target.zip
	cd $(DIRLINUX64) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPLINUX64)

# linuxarm
linuxarm : $(ZIPLINUXARM)

$(DIRLINUXARM)/$(BIN): $(GO_FILES)
	$(MAKE) build-base OS=linux ARCH=arm OUT=$(DIRLINUXARM)/$(BIN)

$(ZIPLINUXARM) : $(DIRLINUXARM)/$(BIN) $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX)
	mkdir -p $(DIRLINUXARM)
	$(CP) --parents $(DOCS) $(MATERIALS) $(RUNLINUX) $(DIRLINUXARM)/
	$(CP) $(BASEZIP) target.zip
	cd $(DIRLINUXARM) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPLINUXARM)

# mac86
mac86 : $(ZIPMAC86)

$(DIRMAC86)/$(BIN) : $(GO_FILES)
	$(MAKE) build-base OS=darwin ARCH=amd64 OUT=$(DIRMAC86)/$(BIN)

$(ZIPMAC86) : $(DIRMAC86)/$(BIN) $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX) $(RUNMAC)
	mkdir -p $(DIRMAC86)
	$(CP) --parents $(DOCS) $(MATERIALS) $(RUNLINUX) $(RUNMAC) $(DIRMAC86)/
	$(CP) $(BASEZIP) target.zip
	cd $(DIRMAC86) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPMAC86)

# macarm
macarm : $(ZIPMACARM)

$(DIRMACARM)/$(BIN) : $(GO_FILES)
	$(MAKE) build-base OS=darwin ARCH=arm64 OUT=$(DIRMACARM)/$(BIN)

$(ZIPMACARM) : $(DIRMACARM)/$(BIN) $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX) $(RUNMAC)
	mkdir -p $(DIRMACARM)
	$(CP) --parents $(DOCS) $(MATERIALS) $(RUNLINUX) $(RUNMAC) $(DIRMACARM)/
	$(CP) $(BASEZIP) target.zip
	cd $(DIRMACARM) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPMACARM)

# win32
win32 : $(ZIPWIN32)

$(DIRWIN32)/$(BIN).exe : $(GO_FILES)
	$(MAKE) build-base OS=windows ARCH=386 OUT=$(DIRWIN32)/$(BIN).exe

$(ZIPWIN32) : $(DIRWIN32)/$(BIN).exe $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX) $(RUNWIN)
	mkdir -p $(DIRWIN32)/$(DOCDIR)
	$(foreach f, $(DOCS), $(call WINCONVERT,$(f),$(DIRWIN32)))
	$(call WINCONVERT,$(MATERIALS),$(DIRWIN32))
	$(call WINCONVERT,$(RUNLINUX),$(DIRWIN32))
	$(call WINCONVERT,$(RUNWIN),$(DIRWIN32))
	chmod +x $(DIRWIN32)/$(RUNWIN)
	$(CP) $(BASEZIP) target.zip
	cd $(DIRWIN32) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPWIN32)

# win64
win64 : $(ZIPWIN64)

$(DIRWIN64)/$(BIN).exe: $(GO_FILES)
	$(MAKE) build-base OS=windows ARCH=amd64 OUT=$(DIRWIN64)/$(BIN).exe

$(ZIPWIN64) : $(DIRWIN64)/$(BIN).exe $(BASEZIP) $(DOCS) $(MATERIALS) $(RUNLINUX) $(RUNWIN)
	mkdir -p $(DIRWIN64)/$(DOCDIR)
	$(foreach f, $(DOCS), $(call WINCONVERT,$(f),$(DIRWIN64)))
	$(call WINCONVERT,$(MATERIALS),$(DIRWIN64))
	$(call WINCONVERT,$(RUNLINUX),$(DIRWIN64))
	$(call WINCONVERT,$(RUNWIN),$(DIRWIN64))
	chmod +x $(DIRWIN64)/$(RUNWIN)
	$(CP) $(BASEZIP) target.zip
	cd $(DIRWIN64) && zip -r ../target.zip *
	mkdir -p $(ZIPDIR)
	mv -f target.zip $(ZIPWIN64)

# various files
$(COMDIR)/$(COMWINTXT) : $(COMDIR)/$(COMTXT)
	$(WINCONVERTER) $(COMDIR)/$(COMTXT) >$(COMDIR)/$(COMWINTXT)

$(DOCDIR)/$(README) : $(DOCDIR)/$(READMEORG)

$(DOCDIR)/$(READMEJP) : $(DOCDIR)/$(READMEJPORG)

.SUFFIXES: .md .txt
.md.txt :
	$(CP) $< $@
	-$(MARKDOWN) $< -o $@

$(BASEZIP) : $(SAMPLE) $(DTD) $(COMS)
	rm -f $(BASEZIP)
	zip -r $(BASEZIP) $(SAMPLE) $(DTD) $(COMS)

$(VERTXT) : $(BIN)
	mkdir -p $(ZIPDIR)
	./$(BIN) -print-version >$(VERTXT)

$(VERPNG) : $(VERTXT)
	mkdir -p $(ZIPDIR)
	touch $(VERPNG)
	-convert -pointsize 16 -transparent white label:"`cat $(VERTXT)`" $(VERPNG)

$(DOCDIR)/$(LICENSE) : LICENSE
	mkdir -p $(DOCDIR)
	$(CP) LICENSE $(DOCDIR)/$(LICENSE)

# useful target
.PHONY: build-base clean re

build-base :
	GOOS=$(OS) GOARCH=$(ARCH) go build -ldflags "-w" -o $(OUT) $(GO_FILES)

clean:
	rm -f $(BIN) $(ARCHIVES) $(BASEZIP) $(COMDIR)/$(COMWINTXT) $(DOCS) $(VERSIONS)
	rm -rf $(DIRALL)

re : clean all
