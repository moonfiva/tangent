# tangentGo README.md

## Name
tangentGo

## Description

This program (tangentGo) is an analysis tool for instrumented indentation testing (IIT),
written in Go and Tcl/Tk.
It was created by Kensuke Miyahara to help users analyze obtained force-depth curves
by various instrumented indentation testers. The analysis is based on the papers[^1] by
Tatsuya Ishibashi et al.

## Link
* [Official website](https://3zip.net/t/en.html)
* [Source repository](https://gitlab.com/moonfiva/tangent)

## Build

1. Clone the source repository to your local machine.
```
$ git clone https://gitlab.com/moonfiva/tangent.git
```
2. Build a zip file containing necessary files for your OS/CPU. See below for all available OS/CPU combinations.
```
$ make linux64
```
Or build all OS/CPU targets.
```
$ make all
```

Following OS/CPU targets are supported:

+ linux32 (for Linux, 32 bit)
+ linux64 (for Linux, 64 bit)
+ linuxarm (for Linux, arm)
+ macarm (for macOS, Apple silicon)
+ mac86 (for macOS 10.13+, Intel)
+ win32 (for Windows, 32 bit)
+ win64 (for Windows, 64 bit)

You will need these tools to build:

* Go (1.19 is confirmed)
* make
* nkf (text convert for Windows)
* pandoc (Optional: markdown convert)
* ImageMagick (Optional: just to make version.png)

Building on Linux PC is confirmed (on Debian 12) and recommended.

## Binaries
Compiled binaries are available at the official website of tangentGo.

[Official website of tangentGo](https://3zip.net/t/en.html)

## Run
Extract everything in a zip file for your OS/CPU and run.
Tcl/Tk software is necessary on your system to run this program.
See tangentGo manual[^1] for more details.

## Contact
Please send a bug report to the author, [Kensuke Miyahara](<mailto:moonfiva@gmail.com>).

## License
This software is created by Kensuke Miyahara (moonfiva@gmail.com).

The copyright of this software belongs to National Institute
for Materials Science.

[National Institute for Materials Science](https://www.nims.go.jp/eng/index.html)

This software is released under the MIT License. Please check
LICENSE for details.


[^1]: See doc/readme.md
