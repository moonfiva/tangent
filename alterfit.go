package main

import (
	"math"
	"fmt"
	"strings"
//	"sort"
)

const alterFitPrecision = 10000.0

// AlterFit : complicated fitting function with power index b
type AlterFit struct {
	a, b, c float64
	updated bool
	data []Point
	corr float64
	xRaw func(Point) float64
	xbFunc func(float64, float64) float64
	xFunc func(Point, float64) float64
	yFunc func(Point) float64
	yInverse func(float64) float64
	classValue func(Point) float64
	bFactor float64
}

// NewAlterFit : create a new instance
func NewAlterFit() *AlterFit {
	t := &AlterFit {
		data: make([]Point, 0),
// default fitting function: y = a x ^ b + c (0.1 < b < 1.1) where x = d[0], y = d[1]
		xRaw: func(d Point) (float64) {return d[0]},
		xbFunc: func(f float64, b float64) (float64) {return math.Pow(f, b)},
		xFunc: func(d Point, b float64) (float64) {return math.Pow(d[0], b)},
		yFunc: func(d Point) (float64) {return d[1]},
		yInverse: func(f float64) (float64) {return f},
		classValue: func(d Point) (float64) {return d[0]},
		bFactor: 1.0,
	}
	return t
}

// SetBFactor
func (t *AlterFit) SetBFactor(c float64)  {
	t.bFactor = c
}

// SetXRaw : set xRaw
func (t *AlterFit) SetXRaw(f func(Point) float64)  {
	t.xRaw = f
}

// SetXFunc : set xFunc
func (t *AlterFit) SetXFunc(f func(Point, float64) float64)  {
	t.xFunc = f
}

// SetYFunc : set yFunc
func (t *AlterFit) SetYFunc(f func(Point) float64)  {
	t.yFunc = f
}

// SetXBFunc : set xbFunc
func (t *AlterFit) SetXBFunc(f func(float64, float64) float64)  {
	t.xbFunc = f
}

// SetYInverse : set yInverse
func (t *AlterFit) SetYInverse(f func(float64) float64)  {
	t.yInverse = f
}

// SetClass : set Class function
func (t *AlterFit) SetClassValue(f func(Point) float64)  {
	t.classValue = f
}

func (t *AlterFit) SetParam(a, b, c float64) {
	t.a = a
	t.b = b
	t.c = c
	t.corr = 1.0
	t.updated = true
}

// Add : add a new data point, d is {x1, x2, ..., y} where xn is a parameter
func (t *AlterFit) Add(d Point) {
	t.data = append(t.data, d)
	t.updated = false
}

// Y : returns calculated y value, d is {x1, x2, ... xn}, but it is ok even if d is {x1, x2, ... xn, y}
func (t *AlterFit) Y(d Point) float64 {
	return t.yInverse(t.F(d))
}

// F : returns Y calculated from x
func (t *AlterFit) F(d Point) float64 {
	return t.expectedYfromX(t.xFunc(d, t.B()))
}

// expectedYfromX : returns Y calculated from X
func (t *AlterFit) expectedYfromX(v float64) float64 {
	return t.A() * v + t.C()
}

// expectedYfromX : returns X calculated from Y
func (t *AlterFit) expectedXfromY(v float64) float64 {
	return (v - t.C()) / t.A()
}

// goodfit : find a best 'b' parameter
func (t *AlterFit) goodfit(b float64) float64 {
	s := NewStat2()
	for _,d := range t.data {
		x := t.xFunc(d, b)
		y := t.yFunc(d)
		s.Add(x, y)
	}
	c := s.Corr()
	return c*c
}

// update : internal function to calculate paramaters a,b,c
func (t *AlterFit) update() {
	if len(t.data) < 2 {
		show("%s AlterFit.update() has only %d curve(s).\n", errorMsg, len(t.data))
		panic(-1)
	}

	var max float64
	var maxIndex float64 = -1
	for i:=0; i < alterFitPrecision; i++ {
		b := float64(i) / float64(alterFitPrecision) * t.bFactor + 0.1	// from 0.1 to (bFactor + 0.1)
		x := t.goodfit(b)
//		show("b = %f, x = %f\n", b, x)
		if maxIndex < 0 {max, maxIndex = x, b}
		if max < x {max, maxIndex = x, b}
	}
	f := NewStat2()
	for _,d := range t.data {
		x := t.xFunc(d, maxIndex)
		y := t.yFunc(d)
		if !math.IsNaN(x) && !math.IsNaN(y) {
			f.Add(x, y)
		}
	}
	t.a = f.A()
	t.b = maxIndex
	t.c = f.B()
	t.corr = f.corr

	t.updated = true
}

// A : parameter A
func (t *AlterFit) A() float64 {
	if t.updated == false {t.update()}
	return t.a
}

// B : parameter B
func (t *AlterFit) B() float64 {
	if t.updated == false {t.update()}
	return t.b
}

// C : parameter C
func (t *AlterFit) C() float64 {
	if t.updated == false {t.update()}
	return t.c
}

// Corr : correlation coefficient
func (t *AlterFit) Corr() float64 {
	if t.updated == false {t.update()}
	return t.corr
}

// CorrSquared : (correlation coefficient)^2
func (t *AlterFit) CorrSquared() float64 {
	if t.updated == false {t.update()}
	return t.corr * t.corr
}

// PlotData
func (t *AlterFit) PlotData(plottype, keyword, specimen string, classGlobal *Classification) string {
	xClass := NewClassification()
	for _, d := range t.data {
		xClass.Add(t.classValue(d))
	}
	var s []string = make([]string, 0, 100)
	fitPlot := NewClassification()
	fitPlot.SetThreshold(0.97)
	for _, d := range t.data {
		var x, y float64
		switch plottype {
		case "xY" : x, y = t.xRaw(d), t.yFunc(d)
		case "xF" : fitPlot.Add(t.xRaw(d)); continue // later
		case "Xy" : x, y = t.xFunc(d, t.B()), t.yInverse(t.yFunc(d))
		case "XF" : fitPlot.Add(t.xFunc(d, t.B())); continue // later
		case "xy" : x, y = t.xRaw(d), t.yInverse(t.yFunc(d))
		case "XY" : fallthrough
		default : x, y = t.xFunc(d, t.B()), t.yFunc(d)
		}
		if !math.IsNaN(x) && !math.IsNaN(y) {
			cl := classGlobal.Search(xClass.Round(t.classValue(d)))
			if cl < 0 { // If no global class, use local class
				cl = xClass.Search(t.classValue(d))
			}
			label := fmt.Sprintf("%s, %s, %3d", keyword, specimen, cl)
			s = append(s, fmt.Sprintf("%s, %s, %s\n", label, ftoa(x), ftoa(y)))
		}
	}
// plot fitting function
	list := FillGap(fitPlot.RepresentListExpanded(), fitPlot.threshold)

	for _, x := range list {
		v := x
		if plottype == "xF" {v = t.xbFunc(v, t.B())}
		y := t.expectedYfromX(v)
		label := fmt.Sprintf("%s, %s, %3d", keyword, specimen, -1)
		s = append(s, fmt.Sprintf("%s, %s, %s\n", label, ftoa(x), ftoa(y)))
	}
	return strings.Join(s, "")
}

func (t *AlterFit) PrintParam(text string) string {
	if !t.IsValid() {return ""}
	var s []string = make([]string, 0, 3)
	if len(text) > 0 {s = append(s, text, ", ")}
	s = append(s, fmt.Sprintf("%s, %s, %s\n", ftoa(t.A()), ftoa(t.B()), ftoa(t.C())))
	return strings.Join(s, "")
}

func (t *AlterFit) Count() int {
	return len(t.data)
}

// IsValid : returns fitting is ok or not
func (t *AlterFit) IsValid() bool {
	if len(t.data) < 2 {return false}
	if math.IsNaN(t.A()) {return false}
	if math.IsNaN(t.B()) {return false}
	if math.IsNaN(t.C()) {return false}

	return true
}

