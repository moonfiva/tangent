package main

import (
	"sort"
	"strings"
)

// Constants
const thresholdDefault = 0.9
const thresholdMax = 0.99
const thresholdMin = 0.10

// Type ClassSet
type ClassSet struct {
	stat *Stat
}

// NewClassSet : create a new instance
func NewClassSet() *ClassSet {
	t := &ClassSet {
		stat: NewStat(),
	}
	return t
}

// Classfication : classifying class
type Classification struct {
	classes []*ClassSet // always sorted
	threshold float64
}

// NewClassification : create a new instance
func NewClassification() *Classification {
	t := &Classification {
		threshold : thresholdDefault,
	}
	return t
}

// Count : return number of classes
func (t *Classification) Count() int {
	return len(t.classes)
}

// IsSameClass : check if x fits a specific class
func IsSameClass(x, lowerLimit, upperLimit, threshold float64) bool {
	threshold = ProperThreshold(threshold)
	if (x < upperLimit * threshold) {return false}
	if (x * threshold > lowerLimit) {return false}
	return true
}

// ProperThreshold : check threshould value
func ProperThreshold(x float64) float64{
	if x < thresholdMin {x = thresholdMin}
	if x > thresholdMax {x = thresholdMax}
	return x
}

// SetThreshold : Set threshold
func (t *Classification) SetThreshold(x float64) {
	t.threshold = ProperThreshold(x)
}

// Search : Search fit class
func (t *Classification) Search(x float64) int {
	for i,v := range t.classes {
		if IsSameClass(x, v.stat.Min, v.stat.Max, t.threshold)  {
			return i
		}
	}
	return -1
}

// Represent : return representative value of i-th class
func (t *Classification) Represent(i int) float64 {
	return t.classes[i].stat.Ave()
}

// Round : return representative value of fit class, else return itself
func (t *Classification) Round(x float64) float64 {
	i :=  t.Search(x)
	if i < 0 {return x}
	return t.Represent(i)
}

// RepresentList : return representative data list of all class
func (t *Classification) RepresentList() []float64 {
	var s []float64 = make([]float64, 0, t.Count())
	for i, _ := range t.classes {
		s = append(s, t.Represent(i))
	}
	return s
}

// FillGap : Add some points in large gaps to float slice
func FillGap(s []float64, threshold float64) []float64 {
	threshold = ProperThreshold(threshold)
	var r []float64 = make([]float64, 0, 100)
	var last float64
	for i, v := range s {
		if i > 0 {
			for v * threshold * threshold > last {	// insert points as many
				last = last / threshold
				r = append(r, last)
			}
		}
		r = append(r, v)
		last = v
	}
	return r
}

// RepresentListExpanded - slightly expanded list
func (t *Classification) RepresentListExpanded() []float64 {
	s := t.RepresentList()
	if len(s) < 2 {return s}

	upper := s[len(s)-1] / t.threshold
	lower := s[0] - (upper - s[len(s)-1]) // same amount with upper
	if s[0] > 0 && lower < s[0]*0.8 {lower = s[0]*0.8} // limit large decrease
	s = append([]float64{lower}, s...)
	s = append(s, upper)
	return s
}

// Add : add a new data to class
func (t *Classification) Add(x float64) {
	i := t.Search(x)
	if i < 0 {	// need a new class
		s := NewClassSet()
		s.stat.Add(x)
		t.classes = append(t.classes, s)
		t.update()
	} else {
		t.classes[i].stat.Add(x) // already existing class
	}
}

// update : internal function to update (sort) classes
func (t *Classification) update() {
	sort.Slice(t.classes, func(i, j int) bool { return t.classes[i].stat.Min < t.classes[j].stat.Min })
}

// String : string conversion of Classification
func (t *Classification) String() string {
	return strings.Join(ftoaSlice(t.RepresentList()), ", ")
}

