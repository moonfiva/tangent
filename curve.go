package main

import (
	"fmt"
	"math"
	"os"
	"io"
	"bufio"
	"strings"
	"bytes"
	"unsafe"
	"strconv"
	"path/filepath"
	"archive/zip"
	"encoding/xml"
)

// Constants
var beta = 1.0
const betaDefault = 1.00
const upperDefault = 0.98
const lowerDefault = 0.80
const minCount = 20 // minimum data point
const calcHardnessDepthLimit = 0.0 // minimum depth to calculate hardness
const calcHardnessForceLimit = 0.0 // minimum force to calculate hardness
const areaFuncDepth = 1000.0
const areaFuncConstant1 = 1e3
const areaFuncConstant2 = 1e1
const SetAutoColumnSearchRow = 100
const SetAutoColumnSearchByte = 30000
const maxReadFullSize = 10000000

// Type Curve : force-depth curves
type Curve struct {
	Name string
	Count int
	MaxF, Maxh float64
	MaxFPos, MaxhPos int
	Lower, Upper float64
	data []Point
	forceColumn, depthColumn []string
	forceFactor, depthFactor float64
	skipLine int
	hr float64
	updated bool
}

// Type Curves : slice of curve pointer
type Curves []*Curve

// Type SharedString : for Excel
type SharedString struct {
	XMLName xml.Name `xml:"sst"`
	SharedStringText []SharedStringText `xml:"si"`
}

// Type SharedStringText : for Excel
type SharedStringText struct {
	Value string `xml:"t"`
}

// Type WorkSheet : for Excel
type WorkSheet struct {
	XMLName xml.Name `xml:"worksheet"`
	Cell []Cell `xml:"sheetData>row>c"`
}

// Type Cell : for Excel
type Cell struct {
	Position string `xml:"r,attr"`	
	Type string `xml:"t,attr"`	
	Value string `xml:"v"`
}

// Type WorkSheet2 : for Excel
type WorkSheet2 struct { // read by ReadTag()
	XMLName xml.Name `xml:"row"`
	Cell []Cell `xml:"c"`
}

// NewCurve : create a new instance
func NewCurve() *Curve {
	t := &Curve {
		data: make([]Point, 0),
		Lower: lowerDefault,
		Upper: upperDefault,
		depthColumn: []string {},
		forceColumn: []string {},
		depthFactor: 1.0,
		forceFactor: 1.0,
		skipLine: 0,
	}
	return t
}

// DataForAlterFit : Create a Point for AlterFit
func (t *Curve) DataForAlterFit(method string, cf float64, elastic float64) Point {
	c := Point {t.ChosenDepth(method, cf), t.MaxF, t.Maxh, t.Hr(), cf, elastic}
	return c
}

// Eit : return Young's modulus
func (t *Curve) Eit(arearoot float64, cf, elastic, p float64) float64 {
	return (1.0 - p * p) / (2.0 / math.Sqrt(math.Pi) * beta * arearoot * (t.Maxh - t.MaxF * cf / areaFuncDepth - t.Hr()) /t.MaxF - elastic)
}

// Hit : return indentation hardness from arearoot
func (t *Curve) Hit(arearoot float64) float64 {
	return t.MaxF / arearoot / arearoot
}

// ChosenDepth : return chosen depth
func (t *Curve) ChosenDepth(method string, cf float64) float64 {
	switch method {
	case "hGeo": return t.HGeo(cf)
	case "hGeo_hr": return t.Hr()
	case "hGeo_hmax": return t.MaxhCorr(cf)
	case "hGeo_Fmax": return t.MaxF
	case "hc": return t.Hc(cf)
	case "hr": return t.Hr()
	case "hmax": return t.MaxhCorr(cf)
	default:
		show("%s wrong hardness method %s.", errorMsg, method)
		return -1
	}
}

// ChosenHardness : return chosen hardness
func (t *Curve) ChosenHardness(method string, cf float64) float64 {
	switch method {
	case "hGeo": return t.HVI(cf)
	case "hGeo_hr": return t.HVI(cf)
	case "hGeo_hmax": return t.HVI(cf)
	case "hGeo_Fmax": return t.HVI(cf)
	case "hc": return t.HHc(cf)
	case "hr": return t.HHr()
	case "hmax": return t.HMCorr(cf)
	default:
		show("%s wrong hardness method %s.", errorMsg, method)
		return -1
	}
}

// CanCalcHardness : return if hardness can be calculated
func CanCalcHardness(f, h float64) bool {
	if (f <= calcHardnessForceLimit) {return false}
	if (h <= calcHardnessDepthLimit) {return false}
	return true
}

// SetUnloadFit : set fitting range
func (t *Curve) SetUnloadFit(l, u float64) {
	t.Upper = u
	t.Lower = l
}

// SetColumn : set depth column, force column, and skip line
func (t *Curve) SetColumn(h, f []string, l int) {
	t.depthColumn = h
	t.forceColumn = f
	t.skipLine = l
	t.VerifyColumn()
}

// VerifyColumn : check depth and force column size
func (t *Curve) VerifyColumn() {
	lenDepth := len(t.depthColumn)
	lenForce := len(t.forceColumn)
	if lenDepth > lenForce {t.depthColumn = t.depthColumn[:lenForce]}
	if lenDepth < lenForce {t.forceColumn = t.forceColumn[:lenDepth]}
}

// HasWrongColumn : check if column setting is ok
func (t *Curve) HasWrongColumn() bool {
	if len(t.depthColumn) == 0 {return true}
	if len(t.forceColumn) == 0 {return true}
	for _, v := range t.depthColumn {
		if ColumnToInt(v) < 0 {return true}
	}
	for _, v := range t.forceColumn {
		if ColumnToInt(v) < 0 {return true}
	}
	return false
}

// SetFactor : set depth and force factor
func (t *Curve) SetFactor(h, f float64) {
	t.depthFactor = h
	t.forceFactor = f
}

// ClearData : clear all data point
func (t *Curve) ClearData() {
	t.Name = ""
	t.Count = 0
	t.MaxF = 0
	t.Maxh = 0
	t.MaxFPos = 0
	t.MaxhPos = 0
	t.data = make([]Point, 0)
	t.updated = false
}

// Add : add a data point
func (t *Curve) Add(depthString, forceString string) {
	dTrim := strings.Trim(strings.TrimSpace(depthString),"\"")
	fTrim := strings.Trim(strings.TrimSpace(forceString),"\"")

	if len(dTrim) == 0 {return}
	if strings.IndexAny(dTrim, "0123456789") < 0 {return}

	y := atof(fTrim)
	switch strings.ToLower(dTrim) { // Direct Parameter Input
	case "-99493.1": fallthrough // Hidden Command Number for Excel, 99493 = KaKuShi KoMando in Japanese
	case "#fmax":
		t.MaxF = y * t.forceFactor
		t.updated = true
		if Debug {show("Curve.Add() set MaxF = %f\n", t.MaxF)}
		return
	case "-99493.2": fallthrough // for Excel
	case "#hmax":
		t.Maxh = y * t.depthFactor
		t.updated = true
		if Debug {show("Curve.Add() set Maxh = %f\n", t.Maxh)}
		return
	case "-99493.3": fallthrough // for Excel
	case "#hr":
		t.hr = y * t.depthFactor
		t.updated = true
		if Debug {show("Curve.Add() set hr = %f\n", t.hr)}
		return
	case "-99493.4": fallthrough // for Excel
	case "#count":
		t.Count = atoi(fTrim)
		t.updated = true
		if Debug {show("Curve.Add() set Count = %d\n", t.Count)}
		return
	}
	x := atof(dTrim)

	f := y * t.forceFactor
	h := x * t.depthFactor

	if h < 0.0 && IncludeNegative == false {return}
	if f < 0.0 && IncludeNegative == false {return}
/*
	if h == 0.0 { // skip continuous zero
		if len(t.data) > 0 {
			last := t.data[len(t.data) - 1]
			if last[0] == 0.0 {return}
		}
	}
*/
	if t.Count == 0 {
		t.MaxF, t.MaxFPos = f, 0
		t.Maxh, t.MaxhPos = h, 0
	}
	if t.MaxF < f {t.MaxF, t.MaxFPos = f, t.Count}
	if t.Maxh < h {t.Maxh, t.MaxhPos = h, t.Count}
	t.data = append(t.data, Point{h, f})
	t.Count++
	t.updated = false
}

// Hr : return tangent depth (hr) with cache
func (t *Curve) Hr() float64 {
	if t.updated == true {return t.hr}
	if !t.IsValid() {return -1}

	fit := NewStat2()
	l := t.MaxF * t.Lower
	u := t.MaxF * t.Upper

	for i:=t.MaxFPos; i < t.Count; i++ {
		if l < t.data[i][1] && t.data[i][1] < u {
			fit.Add(t.data[i][0], t.data[i][1])
		}
	}

	t.hr = fit.Offset()
	t.updated = true
	return t.hr
}

// Base : return base filename
func (t *Curve) Base() string {
	return filepath.Base(t.Name)
}

// FileName : file name considering ShowFolder flag
func (t *Curve) FileName() string {
	if(!ShowFolder) {return t.Base()}
	return t.Name
}

// Eta : elastic work / total work * 100 (%)
func (t *Curve) Eta(cf float64) float64 {
	if !t.IsValid() {return 0.0}
	total, elastic := t.Work(cf)
	if total > 0.0 {return elastic / total * 100.0}
	return 0.0
}

// Work : return plastic and elastic work
func (t *Curve) Work(cf float64) (float64, float64) {
	if !t.IsValid() {return 0.0, 0.0}
	if len(t.data) < t.Count {return 0.0, 0.0} // Care for Direct Parameter Input

	totalWork := 0.0 // total work (loading process)
	elasticWork := 0.0 // elastic work (unloading process)
	frameWork := t.MaxF * t.MaxF * cf / areaFuncDepth // apparent work by frame compliance

	for i := 0; i < t.MaxFPos; i++ {
		totalWork += (t.data[i][1] + t.data[i+1][1]) * (t.data[i+1][0] - t.data[i][0]) // * 0.5 later
	}
	totalWork -= frameWork

	for i := t.MaxFPos; i < t.Count-1; i++ {
		elasticWork += (t.data[i][0] + t.data[i+1][1]) * (t.data[i][0] - t.data[i+1][0]) // *0.5 later
	}
	elasticWork -= frameWork
	totalWork *= 0.5
	elasticWork *= 0.5

	return	totalWork, elasticWork
}

// Hc : return hc, contact depth
func (t *Curve) Hc(cf float64) float64 {
	return t.Hr() + 0.25 * (t.Maxh - t.MaxF * cf / areaFuncDepth - t.Hr())
}

// Tangent : unloading, Caution for unit!
func (t *Curve) Tangent(cf float64) float64 {
	return t.MaxF / (t.MaxhCorr(cf) - t.Hr())
}

// ElasticParam : in Length^2 / Force
func (t *Curve) ElasticParam(cf, area float64) float64 {
	return 2.0 * beta / math.Sqrt(math.Pi / area) / t.Tangent(cf)
}

// HrHmaxRatio : return ratio of tangent depth and corrected hmax
func (t *Curve) HrHmaxRatio(cf float64) float64 {
	return t.Hr() / t.MaxhCorr(cf)
}

// MaxhCorr : calculate corrected hmax
func (t *Curve) MaxhCorr(cf float64) float64 {
	return t.Maxh - t.MaxF * cf / areaFuncDepth
}

// HGeo : Geometric average of hr and hmaxcorr
func (t *Curve) HGeo(cf float64) float64 {
	return math.Sqrt(t.MaxhCorr(cf) * t.Hr())
}

// HM : martens hardness
func (t *Curve) HM() float64 {
	return t.MaxF / t.Maxh / t.Maxh / 26.43 * 1000.0
}

// HMCorr : martens hardness, calculated by hCorr
func (t *Curve) HMCorr(cf float64) float64 {
	return t.MaxF / t.MaxhCorr(cf) / t.MaxhCorr(cf) / 26.43 * 1000.0
}

// HVI : HVI hardness by Ishibashi et al., Caution for unit!
func (t *Curve) HVI(cf float64) float64 {
	return t.MaxF / t.MaxhCorr(cf) / t.Hr()
}

// HHc : hardness by hc, Caution for unit!
func (t *Curve) HHc(cf float64) float64 {
	return t.MaxF / t.MaxhCorr(cf) / t.MaxhCorr(cf)
}

// HHr : hardness by hr, Caution for unit!
func (t *Curve) HHr() float64 {
	return t.MaxF / t.Hr() / t.Hr()
}

// String : convert to string
func (t *Curve) String() string {
	if !t.IsValid() {
		if Debug {
			return fmt.Sprintf("NG, %d, %s, %s, %s", t.Count, ftoa(t.Maxh), ftoa(t.MaxF), t.FileName())
		}
		return "N/A"
	}
	return fmt.Sprintf("%d, %s, %s, %s", t.Count, ftoa(t.Maxh), ftoa(t.MaxF), t.FileName())
}

// Detail : convert more detail to string
func (t *Curve) Detail() string {
	if !t.IsValid() {return "N/A"}
	return fmt.Sprintf("%d, %s, %s, %s, %s, %s, %s, %d, %s", t.Count, ftoa(t.Maxh), ftoa(t.MaxF), t.depthColumn, ftoa(t.depthFactor), t.forceColumn, ftoa(t.forceFactor), t.skipLine, t.FileName())
}

// IsValid : check curve is ok
func (t *Curve) IsValid() bool {
	if t.Count < minCount {return false}
	if t.MaxF <= 0.0 {return false}
	if t.Maxh <= 0.0 {return false}
	return true
}

const alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

// ColumnToInt : convert excel like column string to integer
func ColumnToInt(columnString string) int {
	if len(columnString) == 0 {return -1}
	column := 0
	for _, r := range columnString { // rune loop
		n := strings.Index(alphabets, string(r))
		if n >= 0 {
			column += column * 26 + n + 1 // 1-origin
		} else {
			return atoi(columnString)
		}
	}
	return column - 1 // to make column 1-origin to 0-origin
}

// Type CellKeyword
type CellKeyword struct {
	command string
	row []string
	column []string
}

// String : 
func (t CellKeyword) String() string{
	return fmt.Sprintf("{%s, %s, %s}", t.command, t.row, t.column)
}

var keywords map[string] *CellKeyword = make(map[string] *CellKeyword)

// NewCellKeyword
func NewCellKeyword() *CellKeyword {
	t := &CellKeyword {
	}
	return t
}

// ReadCommands
func ReadCommands(path string) {
	if Debug {show("ReadCommand(): Reading keywords from %s\n", path)}

	f, err := os.Open(path)
	if err != nil {
		if Debug {show("ReadCommand(): can't open %s\n", path)}
		return
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		v := strings.Split(s.Text(), ",")
		c := strings.TrimSpace(v[0])
		if len(c) > 0 && len(v) > 1 {
			keyword := v[1] // column 1, keyword, Don't trim space
			if len(keyword) > 0 {
				x, isThere := keywords[keyword]
				if !isThere {
					x = NewCellKeyword()
					x.command = c // column 0, command, space trimed
				}
				if x.command == c {
					temp := x
					if len(v) == 3 {
						temp.column = append(temp.column, strings.TrimSpace(v[2])) // column 2, column, space trimed
						temp.row = append(temp.row, "")
					}
					if len(v) > 3 {
						temp.column = append(temp.column, strings.TrimSpace(v[2])) // column 2, column, space trimed
						temp.row = append(temp.row, strings.TrimSpace(v[3])) // column 3, row, space trimed
					}
					keywords[keyword] = temp
				} else {
					if Debug {show("%s Found same keyword %s for differenct commands: %s & %s, ignored\n", warningMsg, keyword, x.command, c)}
				}
			}
		}
	}
	if Debug {show("ReadCommand(): Keywords are %s\n", keywords)}
}

// IsCommand
func IsCommand(str string) bool {
	if len(str) == 0 {return false}

	_, isThere := keywords[str]
	return isThere
}

// conditionMatch
func conditionMatch(condition string, try int) bool {
	lenCondition := len(condition)
	if lenCondition > 0 {
		switch condition[0] { // byte
		case byteStart: // "<"
			if lenCondition > 1 {
				if try >= atoi(condition[1:]) {return false}
			}
		case byteEnd: // ">"
			if lenCondition > 1 {
				if try <= atoi(condition[1:]) {return false}
			}
		case byteEqual: // "="
			if lenCondition > 1 {
				return conditionMatch(condition[1:], try)
			}
		case byteNot: // "!"
			if lenCondition > 1 {
				if conditionMatch(condition[1:], try) {return false}
			} else {
				return false
			}
		default:
			if try != atoi(condition[0:]) {return false}
		}
	}
	return true
}

// conditionMatchSlice
func conditionMatchSlice(conditions []string, try int) bool {
	for _, c := range conditions {
		if conditionMatch(c, try) {return true}
	}
	return false
}

// SearchCommand
func (t *Curve) SearchCommand(str string, row, column int){
	if len(str) == 0 {return}

	k, isThere := keywords[str]
	if isThere {
		if !conditionMatchSlice(k.row, row) || !conditionMatchSlice(k.column, column) {
			if Debug {show("SearchCommand(): found a keyword %s @ [%d, %d] as %s, but doesn't match conditions, ignored\n", str, column, row, k.command)}
			return
		}
		if Debug {show("SearchCommand(): found a keyword: %s @ [%d, %d] as %s\n", str, column, row, k.command)}
		for i := 0; i < len(k.command); i++ { // byte loop
			switch k.command[i] {
			case 'N': 
				t.forceFactor = 1000.0
				if Debug {show("SearchCommand(): forceFactor set to %f\n", t.forceFactor)}
			case 'u': 
				t.forceFactor = 0.001
				if Debug {show("SearchCommand(): forceFactor set to %f\n", t.forceFactor)}
			case 'F':
				t.forceColumn = append(t.forceColumn, strconv.Itoa(column))
				t.skipLine = row + 1
				if Debug {show("SearchCommand(): Force column added: %d -> %s\n", column, t.forceColumn)}
			case 'n':
				t.depthFactor = 0.001
				if Debug {show("SearchCommand(): depthFactor set to %f\n", t.depthFactor)}
			case 'h':
				t.depthColumn = append(t.depthColumn, strconv.Itoa(column))
				t.skipLine = row + 1
				if Debug {show("SearchCommand(): Depth column added: %d -> %s\n", column, t.depthColumn)}
			}
		}
	}
}

// columnSplit
func columnSplit(text string, path string) []string {
	var v []string

	switch strings.ToLower(filepath.Ext(path)) {
	case ".csv":
		v = strings.Split(text, ",")
	case ".tsv":
		v = strings.Split(text, "\t")
	default:
		v = strings.Fields(text)
	}
	return v
}

// ExcelOpen
func ExcelOpen(path string) (*zip.ReadCloser, error){
	var zipfile *zip.ReadCloser

	if strings.ToLower(filepath.Ext(path)) != ".xlsx" {return zipfile, nil}
	if Debug {show("ExcelOpen(): opening %s\n", path)}
	return zip.OpenReader(path)
//	defer zipfile.Close()
}

// excelSheetCount
func excelSheetCount(zipfile *zip.ReadCloser) int {
	n := 0
	for _, f := range zipfile.File {
		matched, _ := filepath.Match("xl/worksheets/sheet*.xml", f.FileHeader.Name)
		if matched {n++}
	}
	return n
}

const byteSpace byte = ' '
const byteEnd byte = '>'
const byteStart byte = '<'
const byteEqual byte = '='
const byteNot byte = '!'
const byteQuote byte = '"'

// tagAttr() : returns specific attribute value, startAttr must match exactly, including space around "=": attr="..." and attr = "..." are different
func tagAttr(b []byte, startAttr []byte) string {
//	startAttr := []byte(attr + "=\"")
	lenAttr := len(startAttr)

	j := bytes.Index(b, startAttr)
	if j < 0 {return ""} // not found
	bodyIndex := j + lenAttr
	if len(b) <= bodyIndex {return ""}
	k := bytes.IndexByte(b[bodyIndex:], byteQuote)
	if k <= 0 {return ""}
	body := b[bodyIndex:bodyIndex + k]
	return *(*string)(unsafe.Pointer(&body))
}

// tagRemove() : remove all tags, may not work in very dirty case (ignored for speed)
func tagRemove(b []byte) string {
	i := 0
	for {
		if len(b) <= i {return ""}
		j := bytes.IndexByte(b[i:], byteEnd)
		if j < 0 {  // no end tag
			k := bytes.IndexByte(b[i:], byteStart)
			if k < 0 { // no tags at all
				body := b[i:]
				return *(*string)(unsafe.Pointer(&body))
			} else {  // start tag only
				body := b[i:i+k]
				return *(*string)(unsafe.Pointer(&body))
			}
		}
		if len(b) <= i + j + 1 {return ""}
		if (b[i + j + 1] == byteStart) {
			i += j + 1
			continue
		}
		k := bytes.IndexByte(b[i + j + 1:], byteStart)
		if k < 0 { // no start tag
			body := b[i + j + 1:]
			return *(*string)(unsafe.Pointer(&body))
		} else {
			body := b[i + j + 1:i + j + 1 + k]
			if len(b) > i + j + 1 + k { // may have somthing more
				return *(*string)(unsafe.Pointer(&body)) + tagRemove(b[i + j + 1 + k:])
			}
			return *(*string)(unsafe.Pointer(&body))
		}
	}
}

// tagIndex() : search startTag - endTag index, Format: startTag = "<example", endTag = "</example>"
func tagIndex(b []byte, startTag, endTag []byte) (index, end int) {

	const notFound = -1

	lenStart := len(startTag)
	i := 0 // current position
	for {
		if len(b) <= i {return notFound, notFound}
		j := bytes.Index(b[i:], startTag)
		if j < 0 {return notFound, notFound}
		if len(b) <= i + j + lenStart {return notFound, notFound}
		switch (b[i + j + lenStart]) { // check proper startTag
		case byteSpace:
		case byteEnd:
		default:
			i += j + lenStart + 1
			continue
		}
		k := bytes.Index(b[i + j + lenStart:], endTag)
		if k < 0 {return notFound, notFound}
		return i + j, i + j + lenStart + k + len(endTag)
	}
}

// ReadTag() : get <tag>...</tag> only and add <addTag> and </addTag>
func ReadTag(b []byte, tag, addTag string) *[]byte {
	startTag := []byte("<" + tag)
	endTag := []byte("</" + tag + ">")

	if Debug {show("ReadTag(): searching <%s>...\n", tag)}

	buf := make([]byte, 0)
	if len(addTag) > 0 {buf = append(buf, []byte("<" + addTag + ">")...)}

	i := 0 // current position
	for {
		if len(b) <= i {break}
		j, k := tagIndex(b[i:], startTag, endTag)
		if j < 0 {break}
		buf = append(buf, b[i+j:i+k]...)
//		if Debug {show("body = %s\n", tagRemove(b[i+j:i+k]))}
		i += k
	}

/*
	for {
		if len(b) <= i {break}
		j := bytes.Index(b[i:], startTag)
		if j < 0 {break}
		if len(b) <= i + j + lenStart {break}
		switch (b[i+j+lenStart]) { // check proper startTag
		case byteSpace:
		case byteEnd:
		default:
			i += j+lenStart
			continue
		}
		k := bytes.Index(b[i+j+lenStart:], endTag)
		if k < 0 {break}
//		data := b[i+j:i+j+2+k+len(endTag)]
		buf = append(buf, b[i+j:i+j+lenStart+k+lenEnd]...)
//		s := *(*string)(unsafe.Pointer(&data))
//		show("Found: %s\n", s)
		i += j+lenStart+k+lenEnd
	}
*/

	if len(addTag) > 0 {buf = append(buf, []byte("</" + addTag + ">")...)}

	if Debug {show("ReadTag(): size in = %d, out = %d\n", len(b), len(buf))}
	return &buf
}

func ExcelPrep(zipfile *zip.ReadCloser) (*map[int] string) {
	commandList := make(map[int] string)

	if Debug {show("Curve.ExcelPrep(): started\n")}

	for _, f := range zipfile.File { // read sharedString
		matched, _ := filepath.Match("xl/sharedStrings.xml", f.FileHeader.Name)
		if matched {
			rc, err := f.Open()
			if err != nil {panic(err)}
			defer rc.Close()
			buf := make([]byte, minUInt32(f.UncompressedSize, maxReadFullSize))
			_, err = io.ReadFull(rc, buf)
			if err != nil {panic(err)}

			n := 0 // string No.
			i := 0 // current position
			startTag := []byte("<si")
			endTag := []byte("</si>")
			for {
				if len(buf) <= i {break}
				j, k := tagIndex(buf[i:], startTag, endTag)
				if j < 0 {break}
				s := tagRemove(buf[i+j:i+k])
				if IsCommand(s) {
					commandList[n] = s
					if Debug {show("Curve.ExcelPrep(): Found a command @ SharedStrings[%d] = %s\n", n, s)}
				}
				n++
				i += k
			}

/*
// xml.Unmarshal() is slow
			var s SharedString
			b := ReadTag(buf, "si", "sst")
			err = xml.Unmarshal(*b, &s)
			if err != nil {panic(err)}

			if Debug {show("Curve.ExcelPrep(): %d strings found\n", len(s.SharedStringText))}
			n := 0
			for _, st := range s.SharedStringText {
				if IsCommand(st.Value) {
					commandList[n] = st.Value
					if Debug {show("Curve.ExcelPrep(): Found a command @ SharedStrings[%d] = %s\n", n, st.Value)}
				}
				n++
			}
*/
		}
	}
	return &commandList
}

func (t *Curve) SetAutoColumnExcel(zipfile *zip.ReadCloser, sheet int, commandList *map[int] string) {
	n := 0
	for _, f := range zipfile.File {
		matched, _ := filepath.Match("xl/worksheets/sheet*.xml", f.FileHeader.Name)
		if matched {
			n++
			if n < sheet {continue}
			if n > sheet {break}

			if Debug {show("Curve.SetAutoColumnExcel(): Reading Sheet No. %d (%s) ...\n", sheet, filepath.Base(f.FileHeader.Name))}

			rc, err := f.Open()
			if err != nil {panic(err)}
			defer rc.Close()

// limit reading size to SetAutoColumnSearchByte to speed up
			buf := make([]byte, minUInt32(f.UncompressedSize, SetAutoColumnSearchByte))
			_, _ = io.ReadFull(rc, buf)
			if Debug {show("Curve.SetAutoColumnExcel(): Checking...\n")}

			i := 0 // current position
			startTag := []byte("<c")
			endTag := []byte("</c>")
			typeAttr := []byte("t=\"")
			positionAttr := []byte("r=\"")

			for {
				if len(buf) <= i {break}
				j, k := tagIndex(buf[i:], startTag, endTag)
				if j < 0 {break}
				cell := buf[i+j:i+k]
				i += k
				if tagAttr(cell, typeAttr) != "s" {continue}
				pos := tagAttr(cell, positionAttr)
				row := atoi(strings.TrimLeft(pos, alphabets)) // 1-origin
				row--
				if row > SetAutoColumnSearchRow {break}

				n := atoi(tagRemove(cell))
				s, isThere := (*commandList)[n]
				if isThere {
					column := ColumnToInt(strings.TrimRight(pos, "0123456789"))
					t.SearchCommand(s, row, column)
				}
			}

/*
//xml.Unmarshal() is slow
			var w WorkSheet2
			b := ReadTag(buf, "c", "row")
			err = xml.Unmarshal(*b, &w)
			if err != nil {panic(err)}

			if Debug {show("Curve.SetAutoColumnExcel(): Total %d cells\n", len(w.Cell))}
			for _, c := range w.Cell {
				if c.Type != "s" {continue}
				row, _ := strconv.Atoi(strings.TrimLeft(c.Position, alphabets)) // 1-origin
				row--
				if row > SetAutoColumnSearchRow {break}

				n, _ := strconv.Atoi(c.Value)
				s, isThere := (*commandList)[n]
				if isThere {
					column := ColumnToInt(strings.TrimRight(c.Position, "0123456789"))
					t.SearchCommand(s, row, column)
				}
			}
*/
		}
	}
	if t.HasWrongColumn() { // if all fails
		t.SetColumn([]string {"0"}, []string {"1"}, 0)
	}
	if Debug {show("Curve.SetAutoColumnExcel(): Column set %s, %s, %d\n", t.depthColumn, t.forceColumn, t.skipLine)}
}

// SetAutoColumn
func (t *Curve) SetAutoColumn(path string) {
	file, err := os.Open(path)
	if err != nil {
		show("%s Curve.SetAutoColumn(): can't open %s\n", errorMsg, path)
		panic(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	if Debug {show("Curve.SetAutoColumn(): Setting auto column for %s\n", path)}
	l := -1
	for s.Scan() {
		block := strings.Split(s.Text(), "\r") // necessary for old mac line break (\r)
		for _, b := range block {
			l++
			if l > SetAutoColumnSearchRow {break}
			v := columnSplit(b, path)
			for i, s := range v {
				t.SearchCommand(s, l, i)
			}
		}
	}
	if t.HasWrongColumn() { // if all fails
		t.SetColumn([]string {"0"}, []string {"1"}, 0)
	}
	if Debug {show("Curve.SetAutoColumn(): Column set %s, %s, %d for %s\n", t.depthColumn, t.forceColumn, t.skipLine, path)}
}

// ReadExcel : read a SINGLE curve from a Excel sheet
func (t *Curve) ReadExcel(zipfile *zip.ReadCloser, sheet int, k int, path string) {
	if len(t.forceColumn) < k {return}
	if len(t.depthColumn) < k {return}

	n := 0
	for _, f := range zipfile.File { // read number
		matched, _ := filepath.Match("xl/worksheets/sheet*.xml", f.FileHeader.Name)
		if matched {
			n++
			if n < sheet {continue}
			if n > sheet {break}

			t.ClearData()
			t.Name = path + ", " + strconv.Itoa(sheet)
			dc := ColumnToInt(t.depthColumn[k])
			fc := ColumnToInt(t.forceColumn[k])

			if Debug {show("Curve.ReadExcel(): Reading Sheet No. %d (%s), dc = %d, fc = %d\n", sheet, filepath.Base(f.FileHeader.Name), dc, fc)}
			rc, err := f.Open()
			if err != nil {panic(err)}
			defer rc.Close()

			buf := make([]byte, minUInt32(f.UncompressedSize, maxReadFullSize))
			_, err = io.ReadFull(rc, buf)
			if err != nil {panic(err)}

			var fValue, hValue string
			fLast, hLast := -1, -1

			i := 0 // current position
			startTag := []byte("<c")
			endTag := []byte("</c>")
			typeAttr := []byte("t=\"")
			positionAttr := []byte("r=\"")

			for {
				if len(buf) <= i {break}
				j, k := tagIndex(buf[i:], startTag, endTag)
				if j < 0 {break}
				cell := buf[i+j:i+k]
				i += k
				if tagAttr(cell, typeAttr) == "s" {continue}
				pos := tagAttr(cell, positionAttr)
				row := atoi(strings.TrimLeft(pos, alphabets)) // 1-origin
				row--
				if row < t.skipLine {continue}
				column := ColumnToInt(strings.TrimRight(pos, "0123456789"))
				v := tagRemove(cell)
				if column == fc {
					fValue, fLast = v, row
					if hLast == fLast {t.Add(hValue, fValue)}
				}
				if column == dc {
					hValue, hLast = v, row
					if hLast == fLast {t.Add(hValue, fValue)}
				}
			}

/*
// xml.Unmarshal() is slow
			var w WorkSheet2
			b := ReadTag(buf, "c", "row")
			err = xml.Unmarshal(*b, &w)
			if err != nil {panic(err)}

			if Debug {show("Curve.ReadExcel(): Total %d cells\n", len(w.Cell))}
			for _, c := range w.Cell {
				if c.Type == "s" {continue}
				row, _ := strconv.Atoi(strings.TrimLeft(c.Position, alphabets)) // 1-origin
				row--
				if row < t.skipLine {continue}
				column := ColumnToInt(strings.TrimRight(c.Position, "0123456789"))
				if column == fc {
					fValue, fLast = c.Value, row
					if hLast == fLast {t.Add(hValue, fValue)}
				}
				if column == dc {
					hValue, hLast = c.Value, row
					if hLast == fLast {t.Add(hValue, fValue)}
				}
			}
*/
		}
	}
}

// Read : read a SINGLE curve from a data file (.csv, .tsv, .txt)
func (t *Curve) Read(filename string, k int) {
	if len(t.forceColumn) <= k {return}
	if len(t.depthColumn) <= k {return}

	file, err := os.Open(filename)
	if err != nil {
		show("%s Curve.Open() can't open %s\n", errorMsg, filename)
		panic(err)
	}
	defer file.Close()


	t.ClearData()
	t.Name = filename
	dc := ColumnToInt(t.depthColumn[k])
	fc := ColumnToInt(t.forceColumn[k])
	if Debug {show("Curve.Read(): Reading %s... dc = %d, fc = %d\n", filename, dc, fc)}

	s := bufio.NewScanner(file)
	l := -1
	for s.Scan() {
		block := strings.Split(s.Text(), "\r") // necessary for old mac line break (\r)
		for _, b := range block {
			l++
			if (l < t.skipLine) {continue}
			v := columnSplit(b, filename)
			if len(v) > dc && len(v) > fc {
				t.Add(v[dc], v[fc])
			}
		}
	}
	if Debug {show("Curve.Read(): Reading %d lines finished: %s\n", l+1, t)}
}

// Open : read curve(s) from file(s)
func (t *Curve) Open(path ...string) Curves {
	var curves Curves
	var fileMatched []string
	for i := 0; i < len(path) - 1; i++ { // don't ignore <folder> in <dataset>, can ignore <folder> in <config>
		filetemp := filepath.Join(path[i:]...)
		if Debug {show("Trying %s\n", filetemp)}
		fileMatched, _ = filepath.Glob(filetemp)
		if len(fileMatched) > 0 {break}
	}
	if len(fileMatched) == 0 {if Debug {show("%s No matched files: %v\n", warningMsg, path[:])}}

	for _, filename := range fileMatched {
		if Debug {show("Curve.Open(): Reading this file: %s\n", filename)}
		e := strings.ToLower(filepath.Ext(filename))

		newcurve := *t // needs for scope
		if e == ".xlsx" { // Excel file can have multiple sheets
			zipfile, err := ExcelOpen(filename)
			if err != nil {continue}
			sn := excelSheetCount(zipfile)
			commandList := ExcelPrep(zipfile)
			for j:= 0; j < sn; j++ {
				newcurve = *t
				if newcurve.HasWrongColumn() {
					newcurve.SetAutoColumnExcel(zipfile, j+1, commandList)
				}
				for i := 0; i < len(newcurve.forceColumn); i++ {
					newcurve.ReadExcel(zipfile, j+1, i, filename)
					if newcurve.IsValid() {
						ok := newcurve
						ok.depthColumn = []string {ok.depthColumn[i]}
						ok.forceColumn = []string {ok.forceColumn[i]}
						curves = append(curves, &ok)
					}
				}
			}
			zipfile.Close()
		} else {
			newcurve = *t
			if newcurve.HasWrongColumn() {
				newcurve.SetAutoColumn(filename)
			}
			for i := 0; i < len(newcurve.forceColumn); i++ {
				newcurve.Read(filename, i)
				if newcurve.IsValid() {
					ok := newcurve
					ok.depthColumn = []string {ok.depthColumn[i]}
					ok.forceColumn = []string {ok.forceColumn[i]}
					curves = append(curves, &ok)
				}
			}
		}
	}
	return curves
}

// PlotData
func (t *Curve) PlotData(specimen string) string {
	var s []string = make([]string, 0, 30)
	const unloadingDivider = 7

	if !t.IsValid() {return ""}
	if len(t.data) < t.Count {return ""} // Care for Direct Parameter Input
	step := t.MaxFPos / 60
	if step < 1 {step = 1}
	diff := step
	if Debug {show("Curve.PlotData(): MaxFPos = %d, step = %d, diff = %d\n", t.MaxFPos, step, diff)}
	s = append(s, fmt.Sprintf("Fh, %s, 00, 00, %s, %s, %s, %s\n", specimen, ftoa(t.MaxF), ftoa(t.Maxh), ftoa(t.Hr()), t.Base())) // reset curve
	for i := 0; i + diff/2 < t.MaxFPos; i += diff { // loading, n = 8-10
		s = append(s, fmt.Sprintf("Fh, %s, %s, %s, %d\n", specimen, ftoa(t.data[i][0]), ftoa(t.data[i][1]), i))
		if t.MaxFPos < 67 * step {
			diff += step // 0 2 5 9 14 20 27 35 46 56 | 67
		} else {
			diff += 2 * step  // y = x^2 - 1, 0 3 8 15 24 35 48 63 | 80 99 | 120 143 168 195
		}
	}

	diff = (t.Count - t.MaxFPos) / (unloadingDivider + 1)
	if diff < 1 {diff = 1}
	for i := t.MaxFPos; i + diff < t.Count; i += diff { // unloading, n = unloadingDivider
		s = append(s, fmt.Sprintf("Fh, %s, %s, %s, %d\n", specimen, ftoa(t.data[i][0]), ftoa(t.data[i][1]), i))
	}

	s = append(s, fmt.Sprintf("Fh, %s, %s, %s, %d\n", specimen, ftoa(t.data[t.Count - 1][0]), ftoa(t.data[t.Count - 1][1]), t.Count - 1)) // last
	return strings.Join(s, "")
}

// ClassByForce
func (t *Curve) ClassByForce(forceClass *Classification) int {
	return forceClass.Search(t.MaxF)
}

// ClassByForce
func (cs Curves) ClassByForce() *Classification {
	forceClass := NewClassification()
	for _, c := range cs {
		forceClass.Add(c.MaxF)
	}
	return forceClass
}

// String
func (cs Curves) String() string {
	var s []string = make([]string, 0, 30)
	for _, c := range cs {
		s = append(s, fmt.Sprintf("%s\n", c))
	}
	return strings.Join(s, "")
}

// CheckBadCurve
func (cs Curves) CheckBadCurve(specimen string) Curves {
	forceClass := cs.ClassByForce()
	var hmaxStat []Stat = make([]Stat, len(forceClass.classes))
	var hrStat []Stat = make([]Stat, len(forceClass.classes))
	for _, c := range cs {
		i := forceClass.Search(c.MaxF)
		hmaxStat[i].Add(c.Maxh)
		hrStat[i].Add(c.Hr())
	}
	var poorForce []bool = make([]bool, len(forceClass.classes))
	for i, v := range forceClass.classes {
		if hmaxStat[i].Count > 2 && hmaxStat[i].Ave() > 0 && hmaxStat[i].Sdev() > 0 {
			variation := hmaxStat[i].Sdev() / hmaxStat[i].Ave() * 100
			if variation > 10 {
				if ShowResult {
					show("%s Poor %s level, %s, %s, (%.1f%% variation in hmax)\n", warningMsg, forceWord, specimen, ftoa(v.stat.Ave()), variation)
				}
				poorForce[i] = true
			}
		}
		if hrStat[i].Count > 2 && hrStat[i].Ave() > 0 && hrStat[i].Sdev() > 0 {
			variation := hrStat[i].Sdev() / hrStat[i].Ave() * 100
			if variation > 10 {
				if ShowResult {
					show("%s Poor %s level, %s, %s, (%.1f%% variation in hr)\n", warningMsg, forceWord, specimen, ftoa(v.stat.Ave()), variation)
				}
				poorForce[i] = true
			}
		}
	}
	var newcurves Curves
	bad := 0
	for _, c := range cs {
		var badCurve bool = false
		i := forceClass.Search(c.MaxF)
		if hmaxStat[i].Count > 2 && hmaxStat[i].Ave() > 0 && hmaxStat[i].Sdev() > 0 {
			variation := math.Abs(c.Maxh - hmaxStat[i].Ave()) / hmaxStat[i].Ave() * 100
			if variation > 10 {
				if ShowResult {
					show("%s Bad curve, %s, %s, (%.1f%% difference in hmax)\n", warningMsg, specimen, c.FileName(), variation)
				}
				badCurve = true
			}
		}
		if hrStat[i].Count > 2 && hrStat[i].Ave() > 0 && hrStat[i].Sdev() > 0 {
			variation := math.Abs(c.Hr() - hrStat[i].Ave()) / hrStat[i].Ave() * 100
			if variation > 10.0 {
				if ShowResult {
					show("%s Bad curve, %s, %s, (%.1f%% difference in hr)\n", warningMsg, specimen, c.FileName(), variation)
				}
				badCurve = true
			}
		}
		if poorForce[i] == true {
			if badCurve == false {
				if ShowResult {
					show("%s Bad curve, %s, %s, (poor %s level %s)\n", warningMsg, specimen, c.FileName(), forceWord, ftoa(forceClass.classes[i].stat.Ave()))
				}
			}
			badCurve = true
		}
		if badCurve == false {
			newcurves = append(newcurves, c)
		} else {
			bad++
		}
	}
	if SkipBadCurve == true {
		if bad > 0 {
			show("%s %d bad curve(s) removed from %s\n", warningMsg, bad, specimen)
		}
		return newcurves
	} else {
		return cs
	}
}

