# tangentGo manual

## [1] Introduction

This program is an analysis tool for instrumented indentation testing (IIT)
based on tangent depth analysis. It works offline.

This program calculate Young's modulus and other parameters from
force-depth curves according to the following papers.

1. T. ISHIBASHI, Y. YOSHIKAWA, K. MIYAHARA, T. YAMAMOTO, S. KATAYAMA,
M. YAMAMOTO: J. Mater. Test. Res. Vol.61, No.2, 60 (2016)
2. T. ISHIBASHI, Y. YOSHIKAWA, S. KATAYAMA, K. MIYAHARA, M. OHKI,
T. YAMAMOTO, M. YAMAMOTO: J. Mater. Test. Res. Vol.62, No.2, 68 (2017)
3. T. ISHIBASHI, Y. YOSHIKAWA, M. YAMAMOTO, K. MIYAHARA, T. YAMAMOTO,
M. OHKI, S. KATAYAMA: J. Mater. Test. Res. Vol.62, No.3, 164 (2017)

From v0.11+, HVI(IV) is supported which estimate Vickers Hardness
of specimen according to the following paper.

4. T. ISHIBASHI, Y. YOSHIKAWA, K. MIYAHARA, T. YAMAMOTO, S. TAKAGI,
M. FUJITSUKA, S. KATAYAMA: J. Mater. Test. Res. Vol.65, No.1, 4 (2020)

From v0.13+, another method for Young's modulus is supported.

This program is open source and it is still under development.
Please feel free to send bug reports to the author.

## [2] Install

### [2.1] Preparation

You need Tcl/Tk software on your system to run this program.

* For macOS users, it seems macOS has a built-in Tcl/Tk support and
you need nothing to do.

* For Linux users, most Linux distributions support Tcl/Tk
as a default or additional package.
If you type `wish` in your terminal and a small window opens, it is fine.
Otherwise, install Tcl/Tk packages through your package manager.

	+ (Example) If you are Debian users, `apt install tcl tk tklib`

* For Windows users, please search for a Tcl/Tk executable and
install it on your system. For example, you can use freeWrap
available here:
[freeWrap](https://freewrap.dengensys.com/)

	+ If you use freeWrap, rename freewrap.exe as wish.exe and store it
in tangentGo folder or somewhere in your PATH.

### [2.2] Extracting files

Please extract all files in the downloaded ZIP to a directory.
You can overwrite update files over older files or you may start in a new
directory.

### [2.3] Before first run (macOS users only)

For macOS users, you may need this on the first run (only once).

***
```
Open a terminal and input following commands

cd (tangentGo directory)
chmod +x run.command
```
***

You can drag tangentGo directory name for easy input.
If a security warning popups when you click run.command, please
refer your system setting.

## [3] Run
### [3.1] Starting tangentGo
Move to the directory and start the program by

* run.bat (Windows)
* run.command (macOS)
* run.tcl (Linux)

depending on your system.

### [3.2] If buttons are missing (macOS only)
There seems bugs which occur on specific macOS versions.
Please try either of following workarounds:

+ Change Dark Mode/Light Mode
+ Resize the window
+ Switch to full screen view and return


### [3.3] Sample analysis

When tangentGo starts, please open sample setting (sample.xml)
from File->Open menu.
Next, choose Run->Run and you will see analysis results if
your install is successful.

Note: The attached sample data is created virtually and not
by actual experiments.

## [4] Further details

### [4.1] Experimental data (.csv, .tsv .txt, .xlsx)
Experimental data files are force-depth curves obtained by instrumented
indentation testers. Following file formats are supported:

* .csv	comma(,) separated, hereafter the same
* .tsv	tab(\\t)
* .txt	space, tab, and any space character
* .xlsx	cell (stored in a column)

Other formats (including .xls) are not supported. Proprietary format
may be supported if enough information is provided.

### [4.2] Setting file (.xml)

You can change setting and input specimen information
in a tangentGo main window. For each of specimen, please
specify experimental data files by pressing [+] button.
Don't forget to save a setting file (.xml).
You can browse current settings by Run->Browse current XML.

### [4.3] Using output of analysis

#### [4.3.1] Built-in graph buttons

Various graph buttons are available in the Run output window.
You can choose specimen(s) and force(s) to plot.

#### [4.3.2] Sending output result to spread sheet software

Output results are shown in the Run output window. You can
copy to clipboard and then paste into spread sheet software.
You can also save to a file.

### [4.4] Batch operation (for experts only)

You can run the core analysis program, tangent.exe (Windows)
or tangent (macOS & Linux) from command line for batch analysis.

For example,
```
tangent *.xml
```
will analyze all xml files in the current directory.

Please use redirection to save since output is sent to the
standard output and standard error output.

## [5] Known bugs

* (Windows/macOS) Font size of menu text does not change.
-> No solutions yet.

When you send a bug report, information shown by About->
System Info will be quite helpful.

## [6] History

Version history is available in About->History menu.

## [7] License
This software is created by Kensuke Miyahara (moonfiva@gmail.com).

The copyright of this software belongs to National Institute
for Materials Science.

This software is released under the MIT License. Please check
LICENSE.txt for details.
