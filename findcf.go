package main


import (
//	"strconv"
	"fmt"
	"strings"
)

// FindCF : find an optimum CF value
type FindCF struct {
	data [][3]float64
	optCF float64
	method string
	updated bool
	fmax float64
	fmaxClass *Classification
}

// NewCF : create a new instance
func NewFindCF() *FindCF {
	t := &FindCF {
		data: make([][3]float64, 0, 30),
		fmaxClass: NewClassification(),
	}
	return t
}

// SetMode : set calculation method
func (t *FindCF) SetMethod(m string) {
	t.method = m
}

const cfDepth = 1000.0
const cfPrecision = 3000.0

// Add : add a new curve data
func (t *FindCF) Add(f, h, hr float64) {
	d := [3]float64{f, h, hr}
	if t.fmax < f || len(t.data) == 0 {t.fmax = f}
	t.fmaxClass.Add(f)
	t.data = append(t.data, d)
	t.updated = false
}

func (t *FindCF) AddCurve(curve *Curve) {
	if curve.IsValid() {
		t.Add(curve.MaxF, curve.Maxh, curve.Hr())
	}
}

func fHC(x, y, z, c float64) float64 {
	d := (y - z) / x * float64(cfDepth) - c
	return	x * d * d
}

func (t *FindCF) cv(c float64) float64 {
	s := NewStatWeight()
	for _, d := range t.data {
		weight := 1.0
		switch t.method {
		case "large" : weight = d[0] / (d[1] - d[2])
		case "max2" :	i := t.fmaxClass.Search(d[0])
				if i < len(t.fmaxClass.classes) - 2 {weight = 0}
		case "linear" : weight = d[0]
		case "square" : weight = d[0] * d[0]
		case "cube" : weight = d[0] * d[0] * d[0]
		case "percent" : if t.fmax > d[0] * 3 {weight = 0}
		}
		s.Add(fHC(d[0], d[1], d[2], c), weight)
	}
	return s.CV()
}

func (t *FindCF) update() {
	if len(t.data) < 2 {return}
	var minCF float64
	var minimum float64 
	for i:=0; i < cfPrecision; i++ {
		c := float64(i) / cfPrecision * 3.0 // 0.0 <= c < 3.0
		k := t.cv(c)
		if i == 0 {minimum, minCF = k, c}
		if minimum > k {minimum, minCF = k, c}
	}
	t.optCF = minCF
	t.updated = true
}

func (t *FindCF) PlotData(specimen string, classLocal, classGlobal *Classification) string {
	var s []string = make([]string, 0, 200)
	s = append(s, fmt.Sprintf("CV, %s, %6.3f\n", specimen, t.OptCF()))
	for _, d := range t.data {
		cl := classGlobal.Search(classLocal.Round(d[0]))
		s = append(s, fmt.Sprintf("CF, %s, %3d, %10.3f, %10.3f\n", specimen, cl, d[0], fHC(d[0], d[1], d[2], t.OptCF())))
	}
	return strings.Join(s, "")
}

// OptCF : find optimum CF with cache
func (t *FindCF) OptCF() float64 {
	if t.updated == false {t.update()}
	return t.optCF
}

func (t *FindCF) IsValid() bool {
	if len(t.data) < 3 {return false}
	if t.OptCF() > 2.99 {return false}
	c := t.cv(t.OptCF())
	if c > 0.2 {return false}
	return true
}

