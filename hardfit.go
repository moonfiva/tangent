package main

import (
	"math"
)

func hardCalcCV(hardnessSeries *Series, c float64) (float64, float64, float64) {
	localfit := NewStat2()
	for _, p := range *hardnessSeries {
		localfit.Add(math.Log(p[0]), -math.Log(p[1] - c)) // -ln(y - c) vs ln(x)
	}
	return localfit.CorrSquared(), localfit.A(), localfit.B()
}

// Specially tuned fitting for C_H, assuming one local maximum
// fitting by c -> b order, not by b -> c order
func HardFit(hardnessSeries *Series) (float64, float64, float64) {
hardnessSeries.Sort(1)
cUpper := hardnessSeries.Min(1) // c < cUpper must be satisfied
cLower := 0.0
cBest := (cLower + cUpper) / 2.0
fitBest, aBest, bBest := hardCalcCV(hardnessSeries, cBest)

for range(make([]int, 14)) { // 1/2^14 < 1/10^4
	if Debug {show("cBest=%f, cLower=%f, cUpper=%f, fitBest=%f, b=%f\n", cBest, cLower, cUpper, fitBest, aBest)}
	cLowerHalf := (cLower + cBest) / 2.0
	fit, a, b := hardCalcCV(hardnessSeries, cLowerHalf)
	if (fit > fitBest) {	// optimum c must be lower
		cUpper = cBest
		cBest, fitBest, aBest, bBest = cLowerHalf, fit, a, b
	} else {
		cUpperHalf := (cUpper + cBest) / 2.0
		fit, a, b := hardCalcCV(hardnessSeries, cUpperHalf) // get one more point info for next range
		if fit > fitBest { // optimum c must be higher
			cLower = cBest
			cBest, fitBest, aBest, bBest = cUpperHalf, fit, a, b
		} else { // cBest is still the best, narrowing the range
			cLower = cLowerHalf
			cUpper = cUpperHalf
		}
	}
}
	return math.Exp(-bBest), aBest, cBest  // y = a x^(-b) + c <=> -ln(y - c) = b ln(x) - ln a
}