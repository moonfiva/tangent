package main

import (
	"fmt"
	"os"
	"bufio"
	"strings"
)

const YoungDefault = 0.0 // must be invalid
const PoissonDefault = 0.3
const HardnessDefault = -1.0

// Type Material : Material parameter
type Material struct {
	Name string
	Young, Poisson, Hardness float64
}

// NewMaterial : creat a new instance
func NewMaterial() *Material {
	t := &Material {
		Young: YoungDefault,
		Poisson: PoissonDefault,
		Hardness: HardnessDefault,
	}
	return t
}

// Type MaterialData : Database of Material
type MaterialData map [string]*Material

// NewMaterialData : create a new instance
func NewMaterialData() *MaterialData {
	t := MaterialData {}
	return &t
}

// SetParam : set material parameter with float64
func (t *Material) SetParam(s string, v ...float64) {
	if len(s) > 0 {
		t.Name = s
	}
	if len(v) > 0 {
		t.SetYoung(v[0])
		if len(v) > 1 {
			t.SetPoisson(v[1])
			if len(v) > 2 {
				t.SetHardness(v[2])
			}
		}
	}
}

// SetPoisson : set Poisson
func (t *Material) SetPoisson(x float64) {
	if IsValidPoisson(x) {t.Poisson = x}
}

// SetPoissonStr : set Poisson with string, ignore blank
func (t *Material) SetPoissonStr(str string) {
	if len(str) > 0 {
		t.SetPoisson(atof(str))
	}
}

// SetYoung : set Young's modulus
func (t *Material) SetYoung(x float64) {
	if IsValidYoung(x) {t.Young = x}
}

// SetYoung : set Young's modulus with string, ignore blank
func (t *Material) SetYoungStr(str string) {
	if len(str) > 0 {
		t.SetYoung(atof(str))
	}
}

// SetHardness : set Hardness
func (t *Material) SetHardness(x float64) {
	if IsValidHardness(x) {t.Hardness = x}
}

// SetHardness : set Hardness with string
func (t *Material) SetHardnessStr(str string) {
	if len(str) > 0 {
		t.SetHardness(atof(str))
	}
}

// IsValidPoisson : check Poisson's ratio
func IsValidPoisson(x float64) bool {
	if x < 0.0 {return false}
	if x > 0.5 {return false}
	return true
}

// IsValidYoung : check Young's modulus
func IsValidYoung(x float64) bool {
	if x <= 0.0 {return false}
	return true
}

// IsValidHardness : check Hardness
func IsValidHardness(x float64) bool {
	if x < 0.0 {return false}
	return true
}

// IsValid : check validness of material
func (t *Material) IsValid() bool {
	if len(t.Name) == 0 {return false}
	if !IsValidPoisson(t.Poisson) {return false}
	if !IsValidYoung(t.Young) {return false}
//	if !IsValidHardness(t.Hardness) {return false} // skip hardness check for material
	return true
}

// ElasticParam : return elastic parameter, in Length^2 / Force
func (t *Material) ElasticParam() float64 {
	if !t.IsValid() {return 0.0}
	return (1.0 - t.Poisson * t.Poisson) / t.Young
}

// String : string conversion of material
func (t *Material) String() string {
	return fmt.Sprintf("%s, %.1f, %.02f, %.02f", t.Name, t.Young, t.Poisson, t.Hardness)
}

// Add : add a new Material into database
func (t *MaterialData) Add(name string, v ...float64) {
	s := strings.ToLower(strings.TrimSpace(name)) // keyword is TrimSpaced & ToLowered
	m := NewMaterial()
	m.SetParam(name, v...)
	if m.IsValid() {(*t)[s] = m}
}

// ReadMaterials : read MaterialData from file
func (t *MaterialData) ReadMaterials(filename string) {
	if Debug {show("ReadMaterialFile(): Reading materials from %s\n", filename)}

	materialFile, err := os.Open(filename)
	if err != nil {
		show("%s %s not found.\n", warningMsg, filename)
		return
	}
	defer materialFile.Close()

	s := bufio.NewScanner(materialFile)
	for s.Scan() {
		v := strings.Split(s.Text(), ",")
		if len(v) > 1 {
			s := strings.TrimSpace(v[0])
			if s[0] == '#' {continue}
			var x Point = make(Point, 0, 10)
			for _, y := range v[1:] {
				x = append(x, atof(strings.TrimSpace(y)))
			}
			t.Add(s, x...)
		}
	}
	if Debug {show("%s", t.PlotData())}
}

// PlotData : return Material Data
func (t *MaterialData) PlotData() string {
	var s []string = make([]string, 0, 30)

	for _, d := range *t {
		s = append(s, fmt.Sprintf("Ma, %s\n", d.String()))
	}
	return strings.Join(s, "")
}

// Refer : find a Material from name
func (t *MaterialData) Refer(text string) *Material {
	s := strings.ToLower(strings.TrimSpace(text))
	x, isThere := (*t)[s]
	if isThere {
		if Debug {show("%s matched the database: %s\n", text, x)}
		return x
	}

	m := NewMaterial()
	m.SetParam(text) // return a dummy material which must be !IsValid()
	return m
}

