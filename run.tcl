#!/bin/sh
# the next line restarts using wish \
exec wish "$0" ${1+"$@"}

#
# packages
#
package require Tk
catch {package require Plotchart}
set canPlot [namespace exists ::Plotchart]
catch {package require http}
catch {package require tooltip}
catch {package require autoproxy}
if {[namespace exists autoproxy]} {autoproxy::init}

#
# top window
#
wm geometry . +0+0
wm resizable . 1 0
wm protocol . WM_DELETE_WINDOW {writePref; confirm exit {Exit $progname?}}
option add *tearOff 0
# necessary to avoid 0.3 shown as 0.30000000000000004
set tcl_precision 15

#
# common subroutines
#

# show : show info message
proc show {text {title ""}} {
tk_messageBox -type "ok" -title $title -message $text -parent [myFocusCurrent]
}

# confirm : confirm to exec command
proc confirm {command {message "Are you sure?"}} {
global progname
	set answer [tk_messageBox -type "yesno" -parent [myFocusCurrent] -title "[word Confirm]" -message "[subst [word $message]]"]
	if {$answer == yes} {
		$command
	}
}

# swap : swap x,y
proc swap {x y} {
upvar $x xup
upvar $y yup

set tmp $xup
set xup $yup
set yup $tmp
}

# trimList : trim list
proc trimList {l} {
set t {}
foreach s $l {
	lappend t [string trim $s]
}
return $t
}

# toClip : copy widget content to clipboard
proc toClip {fromtext} {
clipboard clear
clipboard append [$fromtext get 1.0 end]
}

# openURL : open url with browser
proc openURL {url} {
global tcl_platform
if {$tcl_platform(platform) eq "windows"} {
	exec {*}[list {*}[auto_execok start] {}] $url &
} else {
	if {$tcl_platform(os) eq "Darwin"} {
		exec {*}[list open] $url &
	} else {
		exec {*}[list xdg-open] $url &
	}
}
}

# myFocus : focus window
proc myFocus {w} {
raise $w
focus $w
}

# myFocusCurrent : focus & return current window
proc myFocusCurrent {} {
set window [focus]
if {[string length $window] == 0} {set window .}
myFocus $window
return $window
}


# myTooltip : set tooltip
proc myTooltip args {
if {[namespace exists tooltip]} {tooltip::tooltip {*}$args}
}

# myInputReturn : in myInput, set input text to target and return
proc myInputReturn {v {command {}}} {
global wInput
global inputString
upvar $v target
set target $inputString
destroy $wInput
if {[string length $command] > 0} {$command}
}

# myInputDefault : in myInput, set default as input
proc myInputDefault {} {
global inputString
global inputDefault
set inputString $inputDefault
}

# myInputClear : in myInput, clear input string
proc myInputClear {} {
global inputString
set inputString {}
}

# myInput : Input text from keyboard, with default text available
proc myInput {v name default {command {}}} {
global wInput
global wInputOk
global inputString
global inputDefault
upvar $v target
set inputString $target
set inputDefault $default
if {[string length $name] == 0} {set name $v}
if {[winfo exists $wInput]} {destroy $wInput}
toplevel $wInput
wm title $wInput "[word Input]"
set wInputLabel [label $wInput.label -text $name]
set wInputEntry [entry $wInput.entry -textvariable inputString -width 40]
set wInputFooter [frame $wInput.footer]
set wInputOk [ttk::button $wInputFooter.ok -text "OK" -width 4 -command "myInputReturn $v $command"]
set wInputDefault [ttk::button $wInputFooter.default -text "[word Default]" -command "myInputDefault"]
set wInputClear [ttk::button $wInputFooter.clear -text "[word Clear]" -command "myInputClear"]
set wInputCancel [ttk::button $wInputFooter.cancel -text "[word Cancel]" -command "destroy $wInput"]
pack $wInputFooter -side bottom -anchor e
pack $wInputEntry -side bottom -expand 1 -fill both
pack $wInputLabel -side bottom -anchor w
pack $wInputOk $wInputCancel $wInputClear $wInputDefault -side right
bind $wInput <Return> {$wInputOk invoke}
windowPosition $wInput . cover
myFocus $wInputEntry
}

# myCursor : set cursor for all windows
proc myCursor {cursor} {
foreach w [wm stackorder .] {
	$w configure -cursor $cursor
}
}

# myBusy : set busy for specified window list
proc myBusy args {
foreach w $args {
	if {[winfo exist $w]} {
		catch {tk busy hold $w -cursor watch}
	}
}
update
}

# myNoBusy : set no busy 
proc myNoBusy {} {
if {[catch {set dummy [tk busy current]}] != 0} {return}
foreach w [tk busy current] {
	tk busy forget $w
	$w configure -cursor {}
}
update
}

# myTempDir : find temporary directory
proc myTempDir {} {
set tmpdir [pwd]
if {[file exists /tmp]} {set tmpdir /tmp}
catch {set tmpdir $::env(TMP)}
catch {set tmpdir $::env(TEMP)}
return $tmpdir
}

# myTempFile : create a temporary file in current or target directory
proc myTempFile {{dir "."}} {
return [file join "$dir" "[pid]_[clock seconds]"]
}

# splitParam : split text with "_"
# position is 0 (leftpart) or 1 (right part) normally
proc splitParam {text {position 0}} {
if {[string length $text] == 0} {return ""}
set word [split $text "_"]
if {$position >= [llength $word]} {set position 0}
return [lindex $word $position]
}

# special : return special unicode character
proc special {c} {
return [string map { \
% "\uFF05" \
-- "\u2015" \
- "\u2013" \
2 "\u221A" \
. "\u00B7" \
} $c]
}

# italic : return italic letter (only h is supported))
proc italic {c} {
return [string map { \
h "\u210E" \
} $c]
}

# greek : return greek letter
proc greek {c} {
return [string map { \
A "\u0391" \
B "\u0392" \
G "\u0393" \
D "\u0394" \
E "\u0395" \
Z "\u0396" \
H "\u0397" \
Q "\u0398" \
I "\u0399" \
K "\u039A" \
L "\u039B" \
M "\u039C" \
N "\u039D" \
X "\u039E" \
O "\u039F" \
P "\u03A0" \
R "\u03A1" \
S "\u03A3" \
T "\u03A4" \
U "\u03A5" \
F "\u03A6" \
C "\u03A7" \
Y "\u03A8" \
W "\u03A9" \
a "\u03B1" \
b "\u03B2" \
g "\u03B3" \
d "\u03B4" \
e "\u03B5" \
z "\u03B6" \
h "\u03B7" \
q "\u03B8" \
i "\u03B9" \
k "\u03BA" \
l "\u03BB" \
m "\u03BC" \
n "\u03BD" \
x "\u03BE" \
o "\u03BF" \
p "\u03C0" \
r "\u03C1" \
s "\u03C3" \
t "\u03C4" \
u "\u03C5" \
f "\u03C6" \
c "\u03C7" \
y "\u03C8" \
w "\u03C9" \
} $c]
}

# sub : return subscript character
proc sub {c} {
return [string map { \
A "\u1D00" \
B "\u0299" \
C "\u1D04" \
D "\u1D05" \
E "\u1D07" \
F "\uA730" \
G "\u0262" \
H "\u029C" \
I "\u026A" \
J "\u1D0A" \
K "\u1D0B" \
L "\u029F" \
M "\u1D0D" \
N "\u0274" \
O "\u1D0F" \
P "\u1D18" \
Q "\uA7AF" \
R "\u0280" \
S "\uA731" \
T "\u1D1B" \
U "\u1D1C" \
V "\u1D20" \
W "\u1D21" \
X "x" \
Y "\u028F" \
Z "\u1D22" \
i "\u1D62" \ 
s "\u209B" \ 
} $c]
}

# super : return superscript character (limited)
proc super {c} {
return [string map { \
- "\u207b" \
0 "\u2070" \
1 "\u00B9" \
2 "\u00B2" \
3 "\u00b3" \
4 "\u2074" \
5 "\u2075" \
6 "\u2076" \
7 "\u2077" \
8 "\u2078" \
9 "\u2079" \
b "\u1d47" \
} $c]
}

# unit : return unit of given parameter
proc unit {s} {
if {[string length $s] > 0} {
	switch [string range $s 0 0] {
	"h" {return "[greek m]m"}
	"F" {return "mN"}
	"L" {return "mN"}
	"P" {return "mN"}
	"E" {return "GPa"}
	"H" {return "GPa"}
	"C" {return "[greek m]m/N"}
	}
}
return ""
}

# termUpdate : set technical terms for force and depth
proc termUpdate {} {
global termDic
global termForce
global termDepth

switch $termForce {
0 {
	dict unset termDic FORCE
	dict unset termDic Force
	dict unset termDic force
	dict unset termDic F
}
1 {
	dict set termDic FORCE LOAD
	dict set termDic Force Load
	dict set termDic force load
	dict set termDic F P
}
}

switch $termDepth {
0 {
	dict unset termDic DEPTH
	dict unset termDic Depth
	dict unset termDic depth
}
1 {
	dict set termDic DEPTH DISPLACEMENT
	dict set termDic Depth Displacement
	dict set termDic depth displacement
}
}
}

# term : proper term of force or depth
proc term {x} {
global termDic
if {[dict exists $termDic $x]} {return [dict get $termDic $x]}
return $x
}

# dictInit : set term dictionary and each language dictionary
proc dictInit {} {
global translationDics
global termDic

# term dictionary
set termDic [dict create]
termUpdate

# language dictionary
set jpdic [dict create \
{$homePage} {https://3zip.net/t/} \
{$readme} {$readme$language} \
{about} 情報 \
{add a new specimen} 試料の追加 \
{add} 追加 \
{all} 全部 \
{alternative} 別方法 \
{analyze} 解析 \
{analysis} 解析 \
{apparent hardness} 見かけの硬さ \
{are you sure?} よろしいですか？ \
{area function} 面積関数 \
{arrange windows} ウィンドウの整列 \
{auto} 自動 \
{bad curve} 不良曲線 \
{beginner} 初心者 \
{browse current xml}  現在のXMLを表示 \
{browse saved xml} 保存されたXMLを表示 \
{calculation} 算出 \
{calculating} 計算中 \
{calibration} 補正 \
{cancel} 中止 \
{check update} 更新チェック \
{choose a standard specimen} 標準試料を1つ選択 \
{choose any number of standard specimens} 任意の数の標準試料を選択 \
{choose at least 3 standard specimens} 標準試料を3つ以上選択 \
{clear all?} 全消去してよろしいですか？ \
{clear selected} 選択試料をクリア \
{clear} クリア \
{clipboard} クリップボード \
{close} 閉じる \
{coefficient} 係数 \
{color} 色 \
{column setting} カラム設定 \
{column} カラム \
{confirm} 確認 \
{contact depth} 接触深さ \
{conversion} 変換 \
{copy} コピー \
{copyright} 著作権 \
{current} 現在 \
{curve} 押し込み曲線 \
{database} データベース \
{data files} データファイル \
{debug} デバッグ \
{debugger} デバッガー \
{default} 既定 \
{delete selected} 選択試料を削除 \
{depth} 押し込み深さ \
{direct input} 直接入力 \
{displacement} 変位 \
{doc} 文書 \
{document} 文書 \
{draw} 描画 \
{edit} 編集 \
{elastic work ratio} 弾性変形の割合 \
{equivalent hardness} 等価硬さ \
{error} エラー \
{example} 例 \
{exchange two specimens} 選択試料を入れ替え \
{exit} 終了 \
{exit $progname?} {$prognameを終了しますか？} \
{expert} エキスパート \
{factor} 倍率 \
{file} ファイル \
{fixed} 固定 \
{fixed value} 固定値 \
{font size} フォントサイズ \
{font} フォント \
{for Edit} 編集用 \
{force} 試験力 \
{frame compliance} フレームコンプライアンス \
{fitting} フィッティング \
{free input} 自由入力 \
{function} 関数 \
{graph} グラフ \
{hardness} 硬さ \
{help} ヘルプ \
{history} 履歴 \
{home page} ホームページ \
{how to use} 使い方 \
{how-to} 使い方 \
{indenter} 圧子 \
{indentation hardness} 押し込み硬さ \
{info} 情報 \
{input} 入力 \
{interpolation} 補間 \
{label} ラベル \
{largest} 最大 \
{large} 大 \
{latest} 最新 \
{leave blank to calc} 計算する場合は空白 \
{licenses} ライセンス \
{LINE} 行 \
{line} 線 \
{load} 荷重 \
{lower limit} 下限 \
{mean} 平均 \
{memo} メモ \
{menu} メニュー \
{method} 方法 \
{mode} モード \
{necessary} 必須 \
{new update avilable} ホームページに更新版があります \
{new} 新規 \
{no data found} データが見つかりません \
{no document} ドキュメントがありません \
{no help messages} ヘルプメッセージがありません \
{no main program} メインプログラムがありません \
{no update necessary} 更新は必要ありません \
{normal} 標準 \
{not necessary} 不要 \
{not recommended} 非推奨 \
{open} 開く \
{option} オプション \
{optional} オプション \
{original paper} 原論文 \
{plot data} プロットデータ \
{plot} プロット \
{poisson} ポアソン比 \
{poisson's ratio} ポアソン比 \
{projected area} 投影面積 \
{purpose} 目的 \
{raw} 生 \
{read} 読み込み \
{redraw} 再描画 \
{reduced modulus} 複合弾性率 \
{reference specimen} 標準試料 \
{remove all} 全てを削除 \
{remove} 削除 \
{risky} リスク大 \
{run} 実行 \
{save as} 別名で保存 \
{save} 保存 \
{saved xml} {保存されたXML} \
{select} 選択 \
{setting} 設定 \
{shortcut} ショートカット \
{show graph plot data} グラフのプロットデータ \
{show plot data} プロットデータ \
{single-byte characters only} 半角文字をお使いください \
{skip} スキップ \
{skip bad} 不良をスキップ \
{slope} 傾き \
{smallest} 最小 \
{small} 小 \
{sort} ソート \
{specimen name} 試料名 \
{specimen} 試料 \
{standard} 標準 \
{standard specimen} 標準試料 \
{subtitle} {副題} \
{summary only} {集計結果のみ} \
{support} {サポート} \
{system info} システム情報 \
{tangent depth} 接線深さ \
{term} 用語 \
{test} テスト \
{text} テキスト \
{tips} 小技集 \
{title} 表題 \
{to clipboard} クリップボードへ \
{treated as 0.3 if empty} 省略すると0.3と扱われます \
{unit} 単位 \
{unload} 除荷 \
{unloading} 除荷の \
{unloading curve} 除荷曲線 \
{unload fitting} 除荷フィット範囲 \
{update check failed} 更新チェック失敗 \
{update} 更新 \
{upper limit} 上限 \
{useful} 便利な \
{value} 値 \
{variable} 変数 \
{version} バージョン \
{view} 表示 \
{website} ホームページ \
{window} ウィンドウ \
{x-text} X軸名 \
{y-text} Y軸名 \
{young's modulus} ヤング率 \
]
# dict of dictionary of all languages
set translationDics [dict create jp $jpdic]
}

# help : show help messages
proc help {topic} {
global tcl_version
global tcl_patchLevel
global tcl_platform
global firstdir
global execfile
global progname
global version
global notice
global fontSizeChange
global maxSpec
global execError
global plotSymbol plotLine plotMean plotName plotNameFh plotXPos plotYPos plotTPos plotScale
global specList
global vName
global matdata
global fontList
global fontName
global language
global termDic

set h {}
switch $topic {
"howto" { append h "[word Help]: [word {How to use}]\n\n"; switch $language {
"jp" {
append h [subst { 1. まず，各試料の名前を入力してください。全角文字を使うと$prognameでグラフを描画し、画像保存した場合に試料名が消えてしまうため、できれば半角文字のみでの入力をおすすめします。

 2. 各試料のすぐ左の[word File](\[+\])ボタンを押し，[word {Data files}](押し込み曲線)を追加してください。試料名を入力していない場合には，ファイル名を参考に自動入力されます。

 3. メニューから[word Curve]→[word Plot]を選択すると，押し込み曲線が表示されます。表示されない場合には，[word {Data files}]がきちんと読み込めていないので，ファイル形式やカラム設定(ヘルプ参照)を確認してください。また，環境が対応していれば，押し込み曲線ボタン([term F]-h)が表示され、グラフを見ることができます。

 4. 各試料のポアソン比を入力してください。([word {treated as 0.3 if empty}])

 5. 試料のうち，標準試料(BK7やFused silicaなど)を１つ選び，ヤング率を入力し，ヤング率の欄のすぐ左側のボタンを選択状態にしてください。

 6. C[sub F]補正用の試料(標準試料と同じで可)を1個以上選び，C[sub F]のチェックボックスをonにしてください。C[sub F]の値が既知で$prognameでの補正計算が不要の場合は，C[sub F]欄に値([unit C])を入力してください。

 7. ([word {NOT recommended}]) [word Analysis]の欄で，ヤング率の計算方法を変更できます。(デフォルト：tangent depth hrを使用)

 8. ([word Optional]) 硬さ標準試料がある場合には，その試料の硬さ値(例: 200HVなら200)を入力し，硬さの欄のすぐ左側のボタンを選択状態にしてください。
(この機能を使う時は [word Menu] → [word View] → [word Mode] → [word Expert]を選択してください。)


 9. ([word {NOT recommended}]) 右上の[word Analysis]の欄で，硬さの計算方法を変更できます。(デフォルト：hrとhmaxの幾何平均hGeoを使用)

10. ([word Optional]) さらに別方法によるヤング率の計算も行う場合には，5,で選択した標準試料に加えて、追加で2つ以上のヤング率の既知の試料(合計で3つ以上)の試料について，E'ボタンを選択状態にしてください。
(この機能を使う時は [word Menu] → [word View] → [word Mode] → [word Expert]を選択してください。)

11. メニューから[word Run]→[word Run]で解析を実行できます。実行結果はcsv形式と互換性があるため，[word Copy]を押してそのままExcel等の表計算ソフトにペーストできます。また，環境が対応していれば，解析結果や途中経過をグラフで確認できるボタンが表示されます。

12. 以上で入力した設定内容は，いつでも[word File]→[word Save]で保存できますので、適宜保存してください。}]
}
default {
append h [subst { 1. Enter specimen names.

 2. For each specimen, press [word File] (\[+\]) button on the left to add [word {Data files}].  Specimen name is automatically set if it is not specified yet.

 3. Select [word Curve] menu -> [word Plot] to check if [word {Data files}] are properly loaded.  If no data is shown, something is wrong.  Please check file format or manual column setting (see help).  A button to draw [term force]-[term depth] curves is also displayed if your system is supported.

 4. Input Poisson's ratio of all specimens ([word {treated as 0.3 if empty}]).

 5. Input Young's modulus (E) for a reference specimen (ex. BK7 or fused silica) and turn its E-checkbutton on.

 6. Turn C[sub F]-box on for any numbers (1+) of uniform specimens (ex. BK7 and fused silica) OR specify C[sub F] value ([unit C]) in C[sub F] input area.

 7. ([word {NOT recommended}]) Choose different [word Analysis] for E calculation (default: hr).

 8. ([word Optional]) Input Hardness (ex. 200HV -> 200) for a reference specimen for hardness and turn H-checkbutton on.
(Follow [word Menu] -> [word View] -> [word Mode] -> [word Expert] to activate this function.)

 9. ([word {NOT recommended}]) Choose different [word Analysis] for H calculation (default: hGeo).

10. ([word Optional]) Input additional Young's modulus for two or more reference specimens and turn three or more E' box on for alternative Young's modulus calculation.
(Follow [word Menu] -> [word View] -> [word Mode] -> [word Expert] to activate this function.)

11. Select [word Run] menu -> [word Run] to get results. The results are in csv format and you can paste to Excel and other spread sheet programs. Buttons to see Graphs will be displayed if your system is supported.

12. Don't forget [word File] -> [word Save] to save current conditions.}]
}}}
"graph" { append h "[word Help]: [word Graph]\n\n"; switch $language {
"jp" {
append h [subst {　Runウィンドウで結果が表示された時，グラフを表示できます。（macOSやLinuxの場合には，あらかじめtklibをインストールしておく必要があります。）グラフの表示は簡易的なもので、機能は限定的なものです。例えば，押し込み曲線は間引いて表示されます。

できること:
+ 表示する試料の選択 (選択なしの場合は全試料を表示します)
+ 表示するデータを[word [term force]]で選択 (選択なしの場合は全試験力)
+ サイズの変更 ([word Redraw] [word necessary])
+ カラー／モノクロの切り替え
+ ラベルのon/off
+ X,Y軸テキストと表題・副題のon/off
+ 画像の保存 (*.ps)

できないこと:
- 記号の自由な変更
- 軸の範囲の変更 (オートスケールのため)
- X,Y軸のテキスト内容
- グラフのタイトル変更
- フォントの変更
- 試料名の全角表示 (画像保存で消えます)

([word MEAN]) 同じ試験力のデータを平均してプロットします。}]
}
default {
append h [subst {  Graphical charts are avilable in Run window for quick check. You need tklib installed if your system is macOS or Linux. Plotting function of this program is very simple and limited.  For example, [term force]-[term depth] curves are plotted sparsely.

You CAN:
+ choose specimen(s) to draw (all if none is selected)
+ choose data to draw by test [term force] (all if none)
+ change size of graph ([word Redraw] [word necessary])
+ choose color or monochrome graph
+ turn label on or off
+ turn X- and Y- axis label, title, subtitle on or off
+ save it as a postscript image file (*.ps)

You CANNOT:
- change symbols freely
- change axis range (full autoscale)
- change axis text
- change title
- change font
- use multi-byte characters in specimen names (will be lost if image is saved)

(MEAN)  Values at the same force level are averaged and then plotted.
}]
}}}
"column" { append h "[word Help]: [word {Column setting}]\n\n"; switch $language {
"jp" {
append h [subst {注: v0.17から自動設定機能が追加されたため，[word {Column setting}]を手動で行う必要はなくなりました。しかし，自動設定でうまくいかない場合は，手動で[word {Column setting}]を行う必要があります。自動設定で設定された実際の[word {Column setting}]の値は，[word Curve]→[word Plot]で確認できます。

[word {Data files}] (.csv, .tsv, .txt, .xlsx):
  0.002679,  0.010000,  0.10, ...
  0.004581,  0.020000,  0.20, ...
  0.006191,  0.030000,  0.30, ...
  0.007631,  0.040000,  0.40, ...
...

　[word {Column setting}]は，上記のようなデータファイルの中で，何列めが[word [term depth]]や[word [term FORCE]]のデータかを指定します。指定は，数字(0, 1, 2, ...)もしくはExcel形式(A, B, C, ..., Z, AA, AB, ...)のどちらでも可能です。一つのファイルについて，複数の押し込み曲線が含まれる場合には，カンマ(,)で区切って，[word Column]を複数指定することもできますが，[word [term depth]]と[word [term FORCE]]の[word Column]の数は等しくする必要があります。

[word Example]:
 [word [term DEPTH]] [word Column]: 0, [word [term FORCE]] [word Column]: 1 ([word default])
→[word [term DEPTH]](h)は最初の[word Column]から，[word [term FORCE]]([term F])は2番めの[word Column]から読み込みます。

 [word [term DEPTH]] [word Column]: B, [word [term FORCE]] [word Column]: C
→[word [term DEPTH]](h)は2番めの[word Column]から，[word [term FORCE]]([term F])は3番めの[word Column]から読み込みます。

 [word [term DEPTH]] [word Column]: B,E [word [term FORCE]] [word Column]: C,F
→2本の押し込み曲線(h, [term F])を，1本めを(B, C)の[word Column]から，2本めを(E, F)の[word Column]から読み込みます。}]
}
default {
append h [subst {Note: From v0.17, automatic [word {Column setting}] is available.  You don't have to set column parameters if you are lucky.  You can check actual values by automatic [word {Column setting}] in [word Curve] -> [word Plot] screen.

[word {Data files}] (.csv, .tsv, .txt, .xlsx):
  0.002679,  0.010000,  0.10, ...
  0.004581,  0.020000,  0.20, ...
  0.006191,  0.030000,  0.30, ...
  0.007631,  0.040000,  0.40, ...
...

  Column parameters, which specify [word [term DEPTH]] and [word [term FORCE]] columns in [word {Data files}], can be set by either simple numbers (0, 1, 2, ...) or Excel-style alphabets (A, B, C, ..., Z, AA, AB, ...). You may specify multiple columns separated by comma(,). The same amount of [word [term depth]]- and [word [term force]]- columns should be specified, though.

[word Example]
 [word [term DEPTH]] [word Column]: 0, [word [term FORCE]] [word Column]: 1 ([word default])
->read [term depth] (h) from the 1st column, [term force] ([term F]) from the 2nd column

 [word [term DEPTH]] [word Column]: B, [word [term FORCE]] [word Column]: C
->read h from the 2nd, [term F] from the 3rd, respectively

 [word [term DEPTH]] [word Column]: B,E [word [term FORCE]] [word Column]: C,F
->read 2 curves of (h, [term F]) from (B, C) and (E, F)}]
}}}
"bad" { append h "[word Help]: [word {Bad Curve}] - [word Skip]\n\n"; switch $language {
"jp" {
append h [subst {一定の基準により，[word {bad curve}]を自動的に解析からはずすことができます。

　現在の条件: hmaxまたはhrに，10%以上の変動あり

※基準は将来変更される可能性があります。}]
}
default {
append h [subst {Bad curves can be automatically removed from analysis if this setting is turned on.

  Current rule: 10%+ variation in hmax or hr

Please note that the criteria will be modified in the future.}]
}}}
"info" { switch $language {
default {
append h [subst {*** [word {System Info}] ***
Tcl/Tk Version = $tcl_version
patchLevel = $tcl_patchLevel
Platform = $tcl_platform(platform)
Machine = $tcl_platform(machine)
OS = $tcl_platform(os)
OS Version = $tcl_platform(osVersion)
Directory = $firstdir
$progname = $execfile
Version = $version
Notice = $notice
Plotchart = [package versions Plotchart]
tooltip = [package versions tooltip]
autoproxy = [package versions autoproxy]
}]
if {[namespace exists autoproxy]} {
append h [subst {Proxy = [::autoproxy::cget -proxy_host]:[::autoproxy::cget -proxy_port]
}]
}
append h [subst {Screen = [winfo screenwidth .] x [winfo screenheight .]
Encoding Names = [lsort -dictionary -unique [encoding names]]
Encoding System = [lsort -dictionary -unique [encoding system]]

*** [word Graph] ***
plotScale = $plotScale
plotTPos = $plotTPos
plotXPos = $plotXPos
plotYPos = $plotYPos
plotName = $plotName
plotNameFh = $plotNameFh
plotSymbol = $plotSymbol
plotLine = $plotLine
plotMean = $plotMean

*** [word Window] ***
windows = [wm stackorder .]
}]
foreach w [lreverse [wm stackorder .]] {
append h "\n($w)\n"
append h "wm geometry = [wm geometry $w]\n"
append h "winfo geometry = [winfo geometry $w]\n"
lassign [trueGeometry $w] width height x y
append h "true geometry = ${width}x${height}+$x+$y\n"
append h "request = [winfo reqwidth $w]x[winfo reqheight $w]\n"
}
append h [subst {
*** [word Variable] ***
termDic = $termDic
specList = $specList
}]
foreach i $specList {
append h "$i = $vName($i)\n"
}
append h [subst {
*** [word Database] ***
}]
dict for {k v} $matdata {append h "$v\n"}
append h [subst {
*** [word Font] ***
fontName = $fontName
fontList = $fontList
Font Size Changed = $fontSizeChange
}]
foreach f [font names] {
append h "$f = [font configure $f -family], [font configure $f -size], [font actual $f -family], [font metrics $f -fixed], [font configure $f -slant]\n"
}
append h "Font families = [lsort -dictionary -unique [font families]]\n"
}}}
"ISO" { append h "[word Help]: ISO14577\n\n"; switch $language {
"jp" {
append h [subst {Q. $prognameはISO14577に対応していますか？
A. いいえ

理由:
  ISO14577では，押し込み曲線の解析にhc (contact depth)に基づいた特定の解析方法を使う必要があります。しかし，tangent depth法 ($progname) では，よりよい結果を得るために，hr (tangent depth)に基づいて独自の解析を行います。実際に，tangent depth法 ($progname) は，市販の11の試験機で得られた実験データの解析において，試験機付属の解析よりばらつきが1/5に改善した実績を有しています。
  また，ISO14577準拠ではありませんが，メイン画面の[word Analysis]で，hr (tangent depth)ではなくhc (contact depth)に基づいた解析を選ぶことができます([word {NOT recommended}])。}]
}
default {
append h [subst {Q. Does $progname support ISO14577?
A. No.

Reason:
  In ISO14577, a specific method based on hc (contact depth) is required in analysis of indentation curves. However, tangent depth analysis ($progname) uses its own method based on hr (tangent depth) for better results.  It is shown that tangent depth analysis ($progname) shows 1/5 coefficient of variation in analysis of indentation curves of 11 commercial indentation testers.
  Though it is not ISO14577 comliant, you can choose hc (contact depth) instead of hr (tangent depth) at [word Analysis] in the main window ([word {NOT recommended}]) to compare difference of hc and hr.}]
}}}
"matdata" { append h "[word Help]: [word Database]\n\n"; switch $language {
"jp" {
append h [subst {　$prognameのディレクトリのmaterials.txtに標準材料のヤング率やポアソン比などの材料特性値を記録しておき，それと同一の試料名にすることで，これらのパラメータを自動的に入力できます。(試料名は半角文字をお使いください。)

*** 現在登録中のデータ ***\n[word {Specimen name}], [word {Young's modulus}], [word {Poisson's ratio}], [word Hardness]
}]
dict for {k v} $matdata {append h "$v\n"}
}
default {
append h [subst { You may record material properties (Young's modulus and Poisson's ratio) of reference materials in materials.txt in $progname directory. These parameters are automatically set if you input the same specimen name.

*** Current Data ***\n[word {Specimen name}], [word {Young's modulus}], [word {Poisson's ratio}], [word Hardness]
}]
dict for {k v} $matdata {append h "$v\n"}
}}}
"parameters" { append h "[word Help]: [greek b], [greek h]IT\n\n"; switch $language {
"jp" {
append h [subst {[greek b]は以下の式における追加パラメータです。

  ([word Unloading][word Slope]) = [greek b] [special .] (2 / [special 2][greek p]) [special .] Er [special .] [special 2]A

  A: [word {Projected Area}]

  Er: [word {Reduced Modulus}]
  1 / Er = (1 - [greek n][sub s][super 2]) / E[sub s] + (1 - [greek n][sub i][super 2]) / E[sub i]

[greek b]のデフォルトは1.0です。メイン画面の[word Analysis]のところで，[greek b]の値を変更できます。


[greek h]ITは，弾性変形による仕事の全体の仕事に対する比です。

  [greek h]IT = Welastic / (Welastic + Wplastic)

なお，[greek h]ITは，押し込み曲線が完全に除荷されていない状況でも，そのまま
計算されることに注意ください。つまり，最後のデータの次に，試験力が0で
最後と同じ押し込み深さのデータがあるものとして扱われます。
}]
}
default {
append h [subst {[greek b] is an additional parameter in a following equation.

  ([word Unloading] [word Slope]) = [greek b] [special .] (2 / [special 2][greek p]) [special .] Er [special .] [special 2]A

  A: [word {Projected Area}]

  Er: [word {Reduced Modulus}]
  1 / Er = (1 - [greek n][sub s][super 2]) / E[sub s] + (1 - [greek n][sub i][super 2]) / E[sub i]

The default value of [greek b] is 1.0. You can change [greek b] in [word Analysis] in a main window.


[greek h]IT is the ratio of elastic work over total work.

  [greek h]IT = Welastic / (Welastic + Wplastic)

Please note that [greek h]IT is calculated even if force is not completely unloaded.
It is assumed that the next data of the last has force of zero and
depth of the last.
}]
}}}
"analysis" { append h "[word Help]: [word Analysis]\n\n"; switch $language {
"jp" {
append h [subst {1. C[sub F] ([word {frame compliance}] [word fitting])
C[sub F]は，以下のパラメータfが一定になるように決められます。

  f = [term F]max * ((hmax - hr) / [term F]max - C[sub F])[super 2] = const.

以下は詳細のオプションです。
  equal : 全ての押し込み曲線を均等に扱います ([word default])
  large : ([term F]max / hr)の重み付けにより，より大きな[word [term force]]の押し込み曲線を重視します
  max2  : 1番めと2番めに大きい[term F]maxのみの押し込み曲線のみを使用します ([word risky])

2. [word {Young's Modulus}](E[sub IT])および[word {Indentation Hardness}](H[sub IT])
まず，[word Indenter]の[word {Area Function}] A(x) ([word {projected area}])を，標準試料を使い，xの関数として求めます。

  [special 2]A(x) = [special 2][greek p] * [term F]max / 2 / Er / (hmax - [term F]max * C[sub F] - hr)

パラメータのxは以下から選択します。
  hr: [word {tangent depth}] ([word default])
  hc: [word {contact depth}] ([word {NOT recommended}])

  Er: [word {Reduced Modulus}]
  1 / Er = (1 - [greek n][sub s][super 2]) / E[sub s] + (1 - [greek n][sub i][super 2]) / E[sub i]

A(x)は以下の関数にフィッティングされます。

  log(10[special 2]A(x)) = a[special .](log(1000x))[super b] + c

次に，各試料のE[sub IT](=E[sub s])とH[sub IT]を，A(x)と各[word {unloading curve}]を使って，算出します。

  Er = [special 2][greek p] * [term F]max / 2 / [special 2]A(x) / (hmax - [term F]max * C[sub F] - hr)

  E[sub IT] = E[sub s] = (1 - [greek n][sub s][super 2]) / (1 / Er - (1 - [greek n][sub i][super 2]) / E[sub i])

  H[sub IT] = [term F]max / A(x)

3. [word Alternative] [word {Young's Modulus}] [word Calculation]
  (to be updated)

4. [word {Equivalent Hardness}] (Heq)
まず，[word {Apparent Hardness}]HIをy(後述)を使って計算します。

  HI(y) = [term F] / y[super 2] (arb. unit)

次に，[word Hardness][word Conversion][word Coefficient] C[sub H]を，[word {reference specimen}]とその[word hardness]の値(ビッカースなど)を使って，
z(後述)の関数として求めます。

  C[sub H](z) = \[Reference Hardness\] / HI(y)

yとzは次の通りです。

[word method] (y, z)
hGeo (hGeo, hGeo) ... [word $progname] [word default]
hGeo_hr (hGeo, hr)
hGeo_Fmax (hGeo, [term F]max) ... [word {original paper}]
hr (hr, hr)
hc (hc, hc)
hmax (hmax, hmax)

  hGeo = [special 2](hmax * hr)

C[sub H](z)は以下の関数にフィッティングされます。

  C[sub H](z)[super -1] = a[special .]z[super b] + c

各試料の[word {Equivalent Hardness}](Heq)は，次の式で計算できます。

  Heq = C[sub H](z) * HI(y)

[word Example]: 等価ビッカース硬さHVI(IW)を求めるには, hGeo ([word default])を選択し，
[word {reference specimen}]のビッカース硬さをHeqの欄に入力してください。}]
}
default {
append h [subst {1. C[sub F] ([word {frame compliance}] [word fitting])
C[sub F] is determined so that the following parameter f becomes constant.

  f = [term F]max * ((hmax - hr) / [term F]max - C[sub F])[super 2] = const.

These are analyzing options.
  equal : use all curves of reference specimens evenly ([word default])
  large : curves with larger [term force] have more priority according to ([term F]max / hr)
  max2  : use curves with [term F]max at 1st & 2nd largest levels only (risky)

2. Young's Modulus (E[sub IT]) and Indentation Hardness (H[sub IT])
Indenter Area Function A(x) (projected area) is calculated as a function of x,
using a reference specimen,

  [special 2]A(x) = [special 2][greek p] * [term F]max / 2 / Er / (hmax - [term F]max * C[sub F] - hr)

The parameter x is selected from the following:
  hr: tangent depth ([word default])
  hc: contact depth ([word {NOT recommended}])

  Er: [word {Reduced Modulus}]
  1 / Er = (1 - [greek n][sub s][super 2]) / E[sub s] + (1 - [greek n][sub i][super 2]) / E[sub i]

A(x) is fitted to a following type of function.

  log(10[special 2]A(x)) = a[special .](log(1000x))[super b] + c

E[sub IT] (=E[sub s]) and H[sub IT] of specimens are then calculated by A(x)
and each unloading curve.

  Er = [special 2][greek p] * [term F]max / 2 / [special 2]A(x) / (hmax - [term F]max * C[sub F] - hr)

  E[sub IT] = E[sub s] = (1 - [greek n][sub s][super 2]) / (1 / Er - (1 - [greek n][sub i][super 2]) / E[sub i])

  H[sub IT] = [term F]max / A(x)

3. Alternative Young's Modulus Calculation
  (to be updated)

4. Equivalent Hardness (Heq)
First, Apparent Hardness HI is calculated by depth of y (see below).

  HI(y) = [term F] / y[super 2] (arb. unit)

Then, Hardness Coefficient C[sub H] is calculated as a function of z (see below)
using a reference specimen and its reference hardness (Vickers, etc.).

  C[sub H](z) = \[Reference Hardness\] / HI(y)

y and z are as follows

[word method] (y, z)
hGeo (hGeo, hGeo) ... [word $progname] [word default]
hGeo_hr (hGeo, hr)
hGeo_Fmax (hGeo, [term F]max) ... [word {original paper}]
hr (hr, hr)
hc (hc, hc)
hmax (hmax, hmax)

  hGeo = [special 2](hmax * hr)

C[sub H](z) is fitted to a following type of function.

  C[sub H](z)[super -1] = a[special .]z[super b] + c

[word {Equivalent Hardness}] (Heq) of each specimen can be calculated as follows.

  Heq = C[sub H](z) * HI(y)

[word Example]: If you want Equivalent Indenting Vickers Hardness HVI(IW),
choose hGeo ([word default]) and input HV in H column of a reference specimen.}]
}}}
"debug" { append h "[word Help]: [word Debug]\n\n"; switch $language {
"jp" {
append h [subst {[word Run]→[word Run] ([word Debug])を選ぶと，[word Debug]情報を表示させることができます。

[word About]→[word {System Info}]が時には役に立つかもしれません。

フィッティングに関して，メイン画面に[word Interpolation]設定があります([word View]→[word Mode]→[word Debugger]が必要)。[word Interpolation]設定をonにすると，線形補間が使用されるので，標準試料については，ある意味「完璧な」フィッティングになり，[word {Young's modulus}] E (A([italic h]))や[word {Equivalent Hardness}] Heq (C[sub H])が事実上一定の値になります。範囲外の値については，通常のフィッティング関数が使用されます(外挿なし)。この設定は，[word Debug]用なので，実際の解析には使わないでください。
}]
}
default {
append h [subst {You can choose [word Run] -> [word Run] ([word Debug]) to show debugging messages.

[word About] -> [word {System Info}] might be helpful in some cases.

As for fitting, there are '[word Interpolation]' settings for A([italic h]) and C[sub H] in a main window ([word View] -> [word Mode] -> [word Debugger] necessary).  If '[word Interpolation]' setting is on, linear interpolation is applied which gives a 'perfect' fitting, resulting 'flat' values in general for [word {Young's modulus}] E (A([italic h])) or [word {Equivalent Hardness}] Heq (C[sub H]) for the reference material.  A normal fitting function is used for values out of range (i.e. No extrapolation). These setting are for debug purpose and do not use them for real use.
}]
}}}
"option" { append h "[word Help]: [word Option]\n\n"; switch $language {
"jp" {
append h [subst {　Optionの欄に特定の文字列を入力することで，出力を一部変更することができます。
(ほとんどの方には不要の機能です。)

入力可能なオプション:
[exec $execfile -ho]

[word Example]:
  -nを指定すると，フォルダ名が省略され，ファイル名だけが表示されます。}]
}
default {
append h [subst {  You may add options in Option input space to control output if necessary.
(Most of you probably won't need this feature.)

List of available options:
[exec $execfile -ho]}]
}}}
"plot" { append h "[word Help]: [word {Plot data}]\n\n"; switch $language {
"jp" {
append h [subst {　押し込み曲線やヤング率など各種グラフにおけるプロットデータの内容は，[word Curve]→[word {Show plot data}]や[word Run]→[word {Show graph plot data}]で確認することができます。表示はcsv形式と互換性があるため，そのままコピーして表計算ソフトなどにペースト可能です。

プロットデータの一覧:
 AE [word {Young's Modulus}] ([word Alternate]) E' [word Fitting]
 Ah [word {Area Function}] ([word indenter], [word {projected area}]) A([italic h])
 AP [word {Area Function}] A([italic h])の[word Coefficient]
 CF [word {Frame Comliance}] C[sub F] [word Calibration]
 CV [word {Frame Comliance}] C[sub F] [word Value] ([word Mean], [word Specimen])
 EA [word {Young's Modulus}] ([word Alternate]) E'
 EL [word {Elastic Work Ratio}] [greek h][sub IT](%)
 ET [word {Young's Modulus}] E[sub IT]
 Fh [word Curve]
 HC [word {Equivalent Hardness}] [word Conversion][word Coefficient] (C[sub H])
 Hf C[sub H] [word Fitting]
 HI [word {Apparent Hardness}]
 HQ [word {Equivalent Hardness}] Heq
 HR [word {Equivalent Hardness}] [word {Standard specimen}]
 HT [word {Indentation Hardness}] H[sub IT]
 PO [word {Poisson's Ratio}] [greek n]

　1つめの列は，そのプロットが含まれるグラフの種別を表しています。2列めが試料名，3列めが[word [term force]]のグループ, 4,5列めがグラフのX,Yデータです。}]
}
default {
append h [subst { Plot data in any chart can be shown by [word Curve]->[word {Show plot data}] or [word Run]->[word {Show graph plot data}]. Output is in csv format and can be pasted in spreadsheet softwares.

List of plot data:
 AE [word Fitting] [word Function] of [word {Young's Modulus}] ([word Alternate]) E'
 Ah [word {Area Function}] ([word indenter], [word {projected area}]) A([italic h])
 AP [word Coefficient] of [word {Area Function}] A([italic h])
 CF [word {Frame Comliance}] C[sub F] [word Calibration]
 CV [word {Frame Comliance}] C[sub F] [word Value] ([word Mean], [word Specimen])
 EA [word {Young's Modulus}] ([word Alternate]) E'
 EL [word {Elastic Work Ratio}] [greek h][sub IT](%)
 ET [word {Young's Modulus}] E[sub IT]
 Fh [word [term Force]]-[word [term Depth]] [word Curve]
 HC [word Coefficient] of [word {Equivalent Hardness}] [word Fitting] (C[sub H])
 Hf C[sub H] [word Fitting] [word Function]
 HI [word {Apparent Hardness}]
 HQ [word {Equivalent Hardness}] Heq
 HR [word Hardness] [word {Standard specimen}]
 HT [word {Indentation Hardness}] H[sub IT]
 PO [word {Poisson's Ratio}] [greek n]

 The first column shows the kind of chart. The second column is specimen name. The third column is [term force] group. The fourth and fifth columns are the X and Y data.}]
}}}
"tips" { append h "[word Help]: [word Tips]\n\n"; switch $language {
"jp" {
append h [subst {試料数（メイン画面の行数）は最大$maxSpecまでです。不足する場合はご連絡ください。各試料の[word {Data files}]の数は，特に制限ありません。

違う行で，同じ試料名を使うこともできます。この場合，解析時は別々に扱われ，グラフのプロット時にはまとめて一つの試料として扱われます。

試料名に全角文字を使うと，Postscriptで保存した際に試料名が消えてしまいます(仕様)。[word {single-byte characters only}]。

試料名が未入力の場合，[word {Data files}]の名前から自動的に試料名が設定されます。

試料名を空白にすることで，その試料・データを解析対象からはずすことができます。

試料のポアソン比を省略すると0.3と扱われます。

登録された試料名(BK7など)を入力すると，ヤング率やポアソン比など自動で入力されます。

$prognameフォルダにあるmaterials.txtを編集することで，自動入力の設定を自由に変更できます。

自動設定されたカラム設定内容を確認するには，[word Curve]→[word Plot]の実行画面を参照ください。

押し込み曲線の表示([word Curve]→[word Plot])は，解析条件を全く設定していなくても実行可能です。

各試料の[word {Data files}]の一覧画面で，表示したいファイルを1つまたは複数選択して[term F]-hボタンを押すと，押し込み曲線を表示できます。

フレームコンプライアンス(C[sub F])を計算して適用するには，メイン画面のC[sub F]の欄を空白のままにします。

ヤング率とH[sub IT]の解析を行うには，標準試料のヤング率E(とポアソン比)を入力し，ヤング率Eのすぐ左にあるチェックボタンを選択状態にします。

ビッカース硬さなどHVI(IW)の硬さの解析を行うには，硬さの標準試料について硬さ値H(ビッカース硬さなど)を入力し，硬さ値Hのすぐ左にあるチェックボタンを選択状態にします。

補正前硬さ(HI)は，任意単位です。絶対値に意味はありません。

別方法によるヤング率の解析を行うには，ヤング率の解析を設定した後で，さらに2つの追加の標準試料のヤング率E(とポアソン比)を入力し，合計3つ以上の試料について，一番右側の+のチェックボックスを選択状態にします。

実行結果が多すぎる場合には，[word Run]→[word Run] ([word {Summary only}])をお試しください。ファイルごとの結果表示を省略し，集計結果のみを表示します。

実行結果の表示でフォルダ名を非表示にしたい場合は，[word Option]欄に-nと入力します。

グラフでプロットする試料を選択できます。全く選択がない場合には，全ての試料をプロットします。

グラフの[word Title]ボタンは，[word Title]を変更できます。

グラフの[word SubTitle]ボタンは，[word SubTitle]の表示を切り替えます。

グラフの[word Color]ボタンは，カラー→モノクロ(試料毎に異なる記号)→モノクロ(全て同じ記号)を切り替えます。

グラフの[word Line]ボタンは，記号のみ→記号および線→線のみを切り替えます。

グラフのラベル(試料名)は，なるべく重ならないように自動的にずらして表示されます。

グラフの文字サイズは，[word View]→[word {Font size}]と連動しています。

グラフのプロットデータをクリックすると，情報を表示します。別の場所(X軸など)をクリックすると消えます。

ウインドウが行方不明になってしまったら，[word View]→[word {Arrange windows}]でリセットされます。

レシピファイル(設定ファイル)である*.xmlファイルは，エディタで開いて直接編集可能です。

メニューの[word Run]→[word {Browse current XML}]で，現時点でのレシピファイルの内容を確認できます。

更新プログラムをダウンロードしたら，中の新しいファイルを展開して古いフォルダのファイルに上書きするか，新たに別のフォルダに展開して使ってください。}]
}
default {
append h [subst {Maximum number of specimen is $maxSpec. Please contact me if you need more. There is no special limitation on number of [word {Data files}].

You can use the same specimen name for multiple lines. In this case, they are treated indivisually in analysis and summarised in plot.

Multibyte characters in specimen name will disappear in saved postscript images. Please use [word {single-byte characters only}].

Specimen name is automatically set from the name of [word {Data files}].

If specimen name is blank, it is excluded from analysis.

Poisson's ratio is treated as 0.3 if empty.

Young's modulus and Poisson's ratio will be filled for registered materials.

You can edit materials.txt in $progname directory to change parameters of registered materials.

You can check automatically set column setting by [word Curve]->[word Plot].

You can do [word Curve]->[word Plot] without setting analysis conditions.

You can plot [term force]-[term depth] curves from the list of [word {Data files}].

Please leave blank in C[sub F] to calculate frame compliance (C[sub F]).

Input Young's modulus (and Poisson's ratio) of reference specimen and turn its E-checkbutton on to calculate Young's modulus and indentation hardness.

Input hardness (ex. HV) for a reference specimen for hardness and turn H-checkbutton on to calculate HVI(IW) hardness.

Apparent Hardness (HI) is in arbitrary unit. Its absolute value is not important.

Input additional Young's modulus for two or more reference specimens and turn three or more '+' box on for alternative Young's modulus calculation.

If results are too long, please try [word Run]->[word Run] ([word {Summary only}]).

You can omit folders in Run output by entering -n in Option box.

You can choose plotting specimens in any graphs. All specimens are plotted if none is selected.

[word Title] button can change [word Title].

[word Subtitle] button toggles [word Subtitle].

[word Color] button switches Color->Monochrome (different symbols)->Monochrome (same symbols).

[word Line] button switches Symbol only->Symbol&Line->Line only mode.

You can click each plot data for details.  You can dismiss them by clicking somewhere else (ex. X-axis).

Label texts (specimen name) in a graph are displayed avoiding overlap.

Font size in a graph is linked to [word View]->[word {Font size}] setting.

[word View]->[word {Arrange windows}] will reset position and size of all windows.

Setting file (*.xml) is editable by a normal text editor.

You can see current setting by [word Run]->[word {Browse current XML}].

When updating, please unzip all files and then overwrite older files or just start in a new directory.}]
}}}
"unit" { append h "[word Help]: [word Unit]\n\n"; switch $language {
"jp" {
append h [subst {パラメータ ([word Unit])
E  ([unit E]) = (10[super 9] Pa) [word {Young's modulus}]
C[sub F] ([unit C]) = (10[super -6] m/N) [word {Frame Compliance}]
h  ([unit h]) = (10[super -6] m) [word [term Depth]]
F  ([unit F]) = (10[super -3] N) [word [term Force]]

注: 実験データがnmで記録されている場合には，[word [term DEPTH]]の[word Factor]に0.001を指定して，[unit h]単位に合わせてください。カラム設定が自動の場合([word Default])，押し込み曲線の[word {Data files}]から，自動で単位変換されることがあります。}]
}
default {
append h [subst {Parameter ([word Unit])
E  ([unit E]) = (10[super 9] Pa) [word {Young's modulus}]
C[sub F] ([unit C]) = (10[super -6] m/N) [word {Frame Compliance}]
h  ([unit h]) = (10[super -6] m) [word [term Depth]]
F  ([unit F]) = (10[super -3] N) [word [term Force]]

Note: If your [term depth] data is in nm, you should specify 0.001 in [word Factor] of [word [term DEPTH]]. If auto column setting is enabled (default), unit conversion may happen according to [word {Data files}].}]
}}}
"history" { append h "[word Help]: [word Version] [word history]\n\n"; switch $language {
"jp" {
append h [subst {v0.50
本ソフトウェアをMIT Licenseで公開しました。

v0.42
C_Hのフィッティングを改善しました。

v0.41
A(h)などにおいてフィッティング度合いを把握するため，同じグラフ内に
フィッティング関数もプロットできるようにしました。

v0.40
macOSでフォントがイタリックにならない問題などを修正しました。

v0.39
これまではβ=1.0固定でしたが，βの値を変更できるようになりました。

v0.38
Tcl/Tkのバージョンが8.6より古い場合(例:macOS)にも動作するよう修正しました。

v0.37
グラフの各データ点をクリックすると，その点の情報を表示するようになりました。
例えば，数値を読み取ったり，他からずれた押し込み曲線を簡単に特定できます。

v0.36
プロットするデータを試験力で選択できるようになりました。

v0.35
デバッグ用の機能を追加しました。

v0.34
結果のグラフで，同じ試験力の結果を平均する機能を追加しました。

v0.33
不良な押し込み曲線をスキップする機能を追加しました。

v0.32
CFのフィッティング方法にlargeを追加しました。従来の方法(equal)に比べて，
試験力の大きいデータをより重視します。標準試料で低い試験力の押し込み曲線が
不良の場合には，largeを選択してみてください。

v0.31
設定のレイアウトを見直しました。

v0.30
ツールチップを表示するようにしました。対応する場所では，カーソルを置くと
説明のメッセージが表示されます。

v0.29
ポアソン比のグラフ表示に対応しました。グラフのプロットデータリストの
種別記号（2文字）を変更しました。

v0.28
グラフのオートスケールの計算方法を見直しています。不具合がありましたら
お知らせください。

v0.27
日本語表示に対応しました。メニューの[word View]→[word Language]で表示言語を
選択できます。一部，英語のみの部分もあります。また，限定的ですが，
フォントの変更も可能になりました。

v0.26
実験データファイルの追加画面で，選択したファイルの押し込み曲線表示が
可能になりました。

v0.25g
メニューから[word About]→[word {Check Update}]で，更新版があるかどうかを確認できます。
（ネットワーク環境によっては不可）

v0.24
特にWindowsでの文字表示を改善しました。これまでより見やすくなったと思いますが，
問題ありましたらご報告よろしくお願いいたします。}]
}
default {
append h [subst {v0.50
This software is released under MIT License.

v0.42
C_H fitting is improved.

v0.41
In order to check fitting error (ex. A(h)), fitted function can
now be plotted in the same graph.

v0.40
A font trouble (italic ignored) on macOS is fixed.

v0.39
Now you can change β. (β = 1.0 if not specified.)

v0.38
Fixed a bug which prevented tangentGo from running on macOS.

v0.37
Graph is now clickable. You can click each plot data for details.
By this function, you can easily read values, spot bad curves, etc.

v0.36
Now you can choose which data to plot by test force.

v0.35
A dubugging funtion is added.

v0.34
In result graphs, values at the same force level can be averaged and plotted.

v0.33
Bad cuves can be skipped automatically if you turn "Skip bad" function on.

v0.32
New CF fitting method 'large' is added. If you choose large,
curves with larger force have more priority. All curves are
treated equally if you choose 'equal'.

v0.31
Setting UI is redesigned.

v0.30
Tooltip is now supported. A short message is shown when you
point supported objects.

v0.29
A graph plot of Poisson's ratio is supported. Two-letter label
in plot data list is changed.

v0.28
Auto-scale algorithm of graph plot is modified.

v0.27
Japanese is supported. You can switch languages in View ->
Language menu. Font change is also supported but is limited.

v0.26
F-h plot is available in a file-list window of each specimen.}]
}}}
}
if {[string length $h] == 0} {return [word {No help messages}]}
return $h
}

# translate : translate word for language
proc translate {x} {
global translationDics
global language

if {[string eq $language 0]} {return $x}
if {![dict exists $translationDics $language]} {return $x}
set dictionary [dict get $translationDics $language]
if {[dict exists $dictionary $x]} {return [dict get $dictionary $x]}
# check lower letters
set try [string tolower $x]
if {[dict exists $dictionary $try]} {return [dict get $dictionary $try]}
return $x
}

# word : translate word, substituting some global variables
proc word {x} {
global language
global progname
global homePage
global readme
return [subst [translate $x]]
}

# hardnessName : return hardness name for each depth
proc hardnessName {s} {
switch [splitParam $s] {
"hr" {return "H(hr)"}
"hc" {return "H(hc)"}
"hmax" {return "HM"}
"hGeo" {return "HI"}
default {return "H"}
}
}

# cleanText : remove formatting (%?) part from text
proc cleanText {t} {
set text $t
# escape %% to "
regsub -all {%%} $text {"} text
while {[string last "%" $text] > -1} {
	set i [string last "%" $text]
	set j $i
	incr j
	if {$j < [string length $text]} {
		set text [string replace $text $i $j]
	} else {
		set text [string replace $text $i $i]
	}
}
regsub -all {"} $text {%} text
return $text
}

# canvasTextInit : initialize canvasText
proc canvasTextInit {} {
global subFontRatio fontkind fontCanvas fontColorCanvas anchorCanvas
global tcl_platform
set fixedsize [expr int([font configure TkFixedFont -size]*1.1+0.5)]
set fixedsizesmall [expr int($subFontRatio*$fixedsize+0.6)]

if {[lsearch -exact [font names] MyFixed] != -1} {
	font delete MyFixed
	font delete MyFixedItalic
	font delete MyFixedSmall
	font delete MyFixedSmallItalic
}

set family [font configure TkFixedFont -family]
# use Consolas if found
if {[lsearch -exact [font families] Consolas] > -1} {set family Consolas}

# Default fixed font on macOS, Monaco, can not be italic.
# So, use Courier instead
if {$tcl_platform(os) eq "Darwin"} {set family Courier}

font create MyFixed -family $family -size $fixedsize
font create MyFixedItalic -family $family -size $fixedsize -slant italic
font create MyFixedSmall -family $family -size $fixedsizesmall
font create MyFixedSmallItalic -family $family -size $fixedsizesmall -slant italic

set fontkind [list N i B b P p]
set fontCanvas [dict create \
N "MyFixed" \
i "MyFixedItalic" \
B "MyFixedSmall" \
b "MyFixedSmallItalic" \
P "MyFixedSmall"  \
p "MyFixedSmallItalic" \
]

# for debug
set fontColorCanvas [dict create \
N "black" \
i "red" \
B "blue" \
b "green" \
P "orange"  \
p "purple" \
]

set anchorCanvas [dict create \
N {w} \
i {w} \
B {nw} \
b {nw} \
P {sw} \
p {sw} \
]
}

# canvasText : print decorative text in canvas, fixed fonts necessary
# %N : Normal
# %i : italic
# %B : subscript
# %b : subscript (italic)
# %P : superscript
# %p : superscript (italic)
# %_ : flush text buffers at this point (NG for scaled canvas)
# %% : escape for %
#
# x (and y) can be relative (decimal number) to canvas size
# align can be l (left:default), c (center) or r (right)
proc canvasText {canvas x y t {align "l"}} {
global fontkind fontCanvas anchorCanvas fontColorCanvas
lassign [pack info $canvas] dummy parent; # parent = window
set text "%N$t"; # always start with Normal
set isSmall "bBpP"
set isLarge "iN"
#set isItalic "ibp"
# preprocess some words
regsub -all {%%} $text [special %] text; #escape %%
regsub -all {hr} $text {%ih%br%N} text
regsub -all {hc} $text {%ih%bc%N} text
regsub -all {hGeo} $text {%ih%bGeo%N} text
regsub -all {hmax} $text {%ih%bmax%N} text
regsub -all {Fmax} $text {%iF%bmax%N} text
regsub -all {Pmax} $text {%iP%bmax%N} text
regsub -all {EIT} $text {%iE%BIT%N} text
regsub -all {HIT} $text {%iH%BIT%N} text
regsub -all {Heq} $text {%iH%beq%N} text
regsub -all {HM} $text {%NHM} text
regsub -all "[greek h]IT" $text "%i[greek h]%BIT%N" text
set currentPos 0; # current position in $t
set totalWidth 0; # total width (= pixel obtained by font measure)
set buffer 0; # text buffer number (%_ to flush)
set outputText [dict create]; # output text
set offset [dict create]; # starting position (pixel) of each buffer
dict set offset 0 0
set lastWidth [dict create]; # last position (pixel) for each format
set fontWidth [dict create]; # font width (pixel) for each format
foreach kind $fontkind {
	set w [font measure [dict get $fontCanvas $kind] -displayof $parent { }]
	dict set fontWidth $kind $w
}
while {[string first "%" $text $currentPos] > -1} {
	set start [string first "%" $text $currentPos]
	incr start
	set f [string index $text $start]
	if {[string eq $f {_}]} {
		incr buffer
		dict set offset $buffer $totalWidth
		set f N; # set normal after flush
	}
	set end [string first "%" $text $start]
	if {$end < 0} {set end [string length $text]}
	set currentPos $end
	incr end -1
	incr start
	set currentText [string range $text $start $end]
	regsub -all [special %] $currentText {%} currentText; # return alternative %
	set currentLength [string length $currentText]
	if {$currentLength < 1} {continue}
	if {![dict exists $fontWidth $f]} {set f N}; # set normal for illegal font kind
	set w [dict get $fontWidth $f]
	set key [list $f $buffer]
	if {![dict exists $outputText $key]} {
		dict set outputText $key {}
		dict set lastWidth $key [dict get $offset $buffer]
	}
	set diff [expr (int(ceil(($totalWidth - [dict get $lastWidth $key]) * 1.0 / $w)))]
	set diffText [string repeat { } $diff]
	set diffWidth [expr $diff * $w]; # faster (only for fixed font) 
	set currentWidth [expr $currentLength * $w]; # faster (only for fixed font)
	set oldtext [dict get $outputText $key]
	dict set outputText $key "$oldtext$diffText$currentText"
	set totalWidth [expr (max([dict get $lastWidth $key] + $diffWidth, $totalWidth) + $currentWidth)]
	dict set lastWidth $key $totalWidth
}
if {[string first {.} $x] > -1} {set x [expr int([winfo width $canvas]*$x)]}
if {[string first {.} $y] > -1} {set y [expr int([winfo height $canvas]*$y)]}
if {[string equal $align c]} {set x [expr (([winfo width $canvas]-$totalWidth) * $x / [winfo width $canvas])]}
if {[string equal $align r]} {incr x -$totalWidth}
dict for {key text} $outputText {
	lassign $key kind buffer
	$canvas create text [expr ($x+[dict get $offset $buffer])] $y -text $text -anchor [dict get $anchorCanvas $kind] -font "[dict get $fontCanvas $kind]"
}
}

# canvasSample : show canvasText sample in test window for debug
proc canvasSample {} {
global wTest
if {![winfo exists $wTest]} {
	toplevel $wTest
	wm title $wTest "(Debug) Canvas Text"
	canvas $wTest.c -background white -height 320
	pack $wTest.c -expand 1 -fill both -side bottom
	windowPosition $wTest . cover
} else {
	$wTest.c delete all
	myFocus $wTest
}
set texts {"This is Normal.  This is %iItalic." "This is %PSuperscript.  %NThis is %pSuperscript (italic)." "This is %BSubscript.  %NThis is %bSubscript (italic)." "Buffer%_ ABC%BABC%PABC%_ Test" {Young's Modulus,%_ EIT} {log%be%N(10 %iA%P1/2%N)} "%ie%piπ%N = -1" "%ix%P1/2%N = [special 2]%ix"}
set y 30
$wTest.c create text 0 1 -text "Normal" -anchor nw
$wTest.c create text 70 1 -text "MyItalic" -font MyItalic -anchor nw
$wTest.c create text 140 1 -text "MyFixedItalic" -font MyFixedItalic -anchor nw
foreach t $texts {
	canvasText $wTest.c 0 $y [subst $t]
	incr y 30
}
}

# fontSizeSet : change font size to s
proc fontSizeSet {s} {
global fontSizeChange
fontSize [expr ($s-$fontSizeChange)]
}

# fontSize : change font size with diff
proc fontSize {diff} {
global fontSizeChange
global fontSizeInput; # menu variable
incr fontSizeChange $diff
if {$fontSizeChange > 4} {set fontSizeChange 4}; #fontSizeChange max
if {$fontSizeChange < -4} {set fontSizeChange -4;}; #fontSizeChange min
set fontSizeInput $fontSizeChange
foreach f [font names] {
	set s [font configure $f -size]
	if {$s < 0} {
		incr s [expr (-$diff)]
	} else {
		incr s $diff
	}
	font configure $f -size $s
}
writePref
}

# fontListInit : initialize font list
# font in Tcl/Tk
# font name: predefined (TkDefaultFont, TkFixedFont, ...) and created
#    another format1 = family [size [option]] ex. {times 12 {bold italic}}
#    another format2 = ex. {-family times -size 12 -weight bold -slant italic}
# font family: predefined (Courier, Times, Helvetica) and installed font
proc fontListInit {} {
global tcl_platform
global fontList
global fontName
# default Tk font
set fontName TkDefaultFont
set fontList $fontName
# add Meiryo if found
if {[lsearch -glob [font families] "Meiryo*"] > -1} {
	lappend fontList Meiryo
	set fontName Meiryo
}
# add some common fonts
lappend fontList Courier Times Helvetica Arial Georgia Noto
}

# splitGeometry : get each parameter (x, y, posx, posy) from geometry string
# caution: no error check
proc splitGeometry {g} {
scan $g {%dx%d%[-+]%d%[-+]%d} sx sy sgn1 px sgn2 py
return [list $sx $sy [expr "$sgn1$px"] [expr "$sgn2$py"]]
}

# trueGeometry : return true (real)? geometry
proc trueGeometry {window} {
set g [splitGeometry [wm geometry $window]]
lassign $g wx wy posx posy

set rootx [winfo rootx $window]
set rooty [winfo rooty $window]; # menubar not included
set wx [winfo reqwidth $window]
set wy [winfo reqheight $window]
set sizex [expr ($wx+$rootx-$posx)]
set sizey [expr ($wy+$rooty-$posy)]

return [list $sizex $sizey $posx $posy]
}

# windowPosition : set window position (thiswin) relative to relwin
# direction: bottom, right, left, cover
# call update before calling this proc if necessary
proc windowPosition {thiswin relwin direction} {

if {![winfo exists $thiswin]} {return}
if {![winfo exists $relwin]} {return}

set sx [winfo screenwidth $relwin]
set sy [winfo screenheight $relwin]
incr sy -64; #keep bottom area

set grelwin [trueGeometry $relwin]
lassign $grelwin sizex sizey newx newy
set gthiswin [trueGeometry $thiswin]
lassign $gthiswin tx ty

switch $direction {
bottom {incr newy $sizey}
right {incr newx $sizex}
left {incr newx [expr (-$tx)]}
cover {incr newx [expr ($sizex/2)]
	incr newy [expr ($sizey/2)]}
}
if {[expr ($newx+$tx)] > $sx} {set newx [expr ($sx-$tx)]}
if {$newx < 0} {set newx 0}
if {[expr ($newy+$ty)] > $sy} {set newy [expr ($sy-$ty)]}
if {$newy < 0} {set newy 0}
wm geometry $thiswin "+$newx+$newy"
update; # Windows need this
myFocus $thiswin
}

# windowClear : delete windows except exception
proc windowClear {{exception ""}} {
foreach w [lreplace [wm stackorder .] end end] {; # always keep .
	if {[lsearch -exact $exception $w] == -1} {destroy $w}
}
}

# windowSize : modify window size
# method: {width, height, both}(-{max, min} or nothing)
# max: don't make it smaller
# min: don't make it bigger
# nothing (not max or min): set required size
# {} (not width, height, both): do nothing
proc windowSize {w {method both}} {
set x [winfo width $w]
set y [winfo height $w]
set rx [winfo reqwidth $w]
set ry [winfo reqheight $w]
set mx [expr max($x, [winfo reqwidth $w])]
set my [expr max($y, [winfo reqheight $w])]
set nx [expr min($x, [winfo reqwidth $w])]
set ny [expr min($y, [winfo reqheight $w])]
switch $method {
both {wm geometry $w ${rx}x${ry}}
both-max {wm geometry $w ${mx}x${my}}
both-min {wm geometry $w ${nx}x${ny}}
height {wm geometry $w ${x}x${ry}}
height-max {wm geometry $w ${x}x${my}}
height-min {wm geometry $w ${x}x${ny}}
width {wm geometry $w ${rx}x${y}}
width-max {wm geometry $w ${mx}x${y}}
width-min {wm geometry $w ${nx}x${y}}
default {}
}
}

# windowSizeAll : update size of all windows
proc windowSizeAll {{method both}} {
update
foreach w [wm stackorder .] {
	windowSize $w $method
}
}

# myWindowSize : custom window size for this program
proc myWindowSize {} {
global wRun wGraph wPick wClass wHelp wData wInput wTest
update
foreach w [wm stackorder .] {
# braces can not be used if case has a variable
	switch -exact $w "
	. {set method both}
	$wRun {set method both-max}
	#$wGraph {set method both}
	$wPick {set method both}
	$wClass {set method both}
	$wHelp {set method both-max}
	$wData* {set method both-max}
	$wInput {set method both}
	$wTest {set method both-max}
	default {set method {}}
	"
	windowSize $w $method
}
}

# windowRearrange : Rearrange positions of all windows
proc windowRearrange {} {
global wRun wGraph wPick wClass wHelp
global wGraphRedraw
myWindowSize
wm geometry . +0+0
if {[winfo exist $wGraph]} {
	if {[winfo exist $wGraphRedraw]} {$wGraphRedraw invoke}
	windowPosition $wGraph . right
	if {[winfo exist $wPick]} {windowPosition $wPick $wGraph left}
	if {[winfo exist $wClass]} {windowPosition $wClass $wPick left}
}
if {[winfo exist $wRun]} {windowPosition $wRun . bottom}
if {[winfo exist $wHelp]} {windowPosition $wHelp . cover}
}

# mainWindowTitle : set main window title
proc mainWindowTitle {} {
global xmlFileName
global progname
global author
global version
set filename [file tail $xmlFileName]
if {[string length $filename] == 0} {
	wm title . "$progname $version $author"
} else {
	wm title . $filename
}
}

# checkBadChar : check if text has comma (,) or any other inappropriate characters
proc checkBadChar {text} {
if {[string first {,} $text] > -1} {return false}
return true
}

# readPref : read preference
proc readPref {} {
global execdir
global prefFile
global optString
global fontName
global fontSizeChange
global language
global userModeLevel
global termForce
global termDepth

set openfile [file join $execdir $prefFile]
if {![file readable $openfile]} {return}
set fd [open $openfile r]
while {![eof $fd]} {
	gets $fd line
	set v [split $line ,]
	lassign [trimList $v] var value
	if {[info exists $var]} {
		set $var $value
	}
}
}

# writePref : write preference
proc writePref {} {
global execdir
global prefFile
global optString
global fontName
global fontSizeChange
global language
global userModeLevel
global termForce
global termDepth

set openfile [file join $execdir $prefFile]
set fd [open $openfile w]
puts $fd "optString, $optString"
puts $fd "fontName, $fontName"
puts $fd "fontSizeChange, $fontSizeChange"
puts $fd "language, $language"
puts $fd "userModeLevel, $userModeLevel"
puts $fd "termForce, $termForce"
puts $fd "termDepth, $termDepth"
close $fd
myWindowSize
}

# fontInit : initialize fonts
proc fontInit {} {
global subFontRatio
global fontName

if {[lsearch -exact [font names] MyDefault] != -1} {
	font delete MyDefault
	font delete MyItalic
	font delete MySmall
	font delete MySmallItalic
}

set size [font configure TkDefaultFont -size]
set smallsize [expr int($subFontRatio*$size+0.6)]

# get current font family name
if {[lsearch -exact [font names] $fontName] == -1} {
	set family $fontName
} else {
	set family [font configure $fontName -family]
}

# set current font in widget except canvas which needs fixed size font
font create MyDefault -family $family -size $size
font create MyItalic -family $family -size $size -slant italic
font create MySmall -family $family -size $smallsize
font create MySmallItalic -family $family -size $smallsize -slant italic
option add *font MyDefault
ttk::style configure TButton -font MyDefault
ttk::style configure MyItalic.TButton -font MyItalic
writePref
}

# menuInit : initialize menus
proc menuInit {} {
global language
global userModeLevel
global termForce
global termDepth
global fontList
global fontName
global readme
global progname
if {[winfo exists .m]} {destroy .m}
set option ""
if {![string eq $language 0]} {; # disable underline if not English
set option "-underline -1"
}
menu .m
.m add cascade -label "[word File] " -underline 0 {*}$option -menu .m.f
.m add cascade -label "[word View] " -underline 0 {*}$option -menu .m.v
.m add cascade -label "[word Specimen] " -underline 0 {*}$option -menu .m.e
.m add cascade -label "[word Curve] " -underline 0 {*}$option -menu .m.c
.m add cascade -label "[word Run] " -underline 0 {*}$option -menu .m.r
.m add cascade -label "[word Help] " -underline 0 {*}$option -menu .m.help
.m add cascade -label "[word About]" -underline 0 {*}$option -menu .m.about

menu .m.f
.m.f add command -label "[word Open]" -underline 0 {*}$option -command readxml
.m.f add command -label "[word New]" -underline 0 {*}$option -command "confirm renew {Clear all?}"
.m.f add separator
.m.f add command -label "[word Save]" -underline 0 {*}$option -command overwriteXML
.m.f add command -label "[word {Save as}]" -underline 5 {*}$option -command saveasXML
.m.f add separator
.m.f add command -label "[word Exit]" -underline 0 {*}$option -command {confirm exit {Exit $progname?}}

menu .m.e
.m.e add command -label "[word {Add a new specimen}]" -underline 0 {*}$option -command addCheck
.m.e add command -label "[word {Clear selected}]" -underline 0 {*}$option -command clearCheck
.m.e add command -label "[word {Delete selected}]" -underline 0 {*}$option -command delCheck
.m.e add command -label "[word {Exchange two specimens}]" -underline 0 {*}$option -command swapCheck

menu .m.v
.m.v add command -label "[word {Arrange windows}]" -underline 0 {*}$option -command {windowRearrange}
.m.v add cascade -label "[word Font]" -underline 0 {*}$option -menu .m.v.font
.m.v add cascade -label "[word {Font size}]" -underline 5 {*}$option -menu .m.v.f
.m.v add cascade -label "[word Term]" -underline 0 {*}$option -menu .m.v.term
.m.v add cascade -label "[word Mode]" -underline 0 {*}$option -menu .m.v.mode
.m.v add cascade -label "[word Language]" -underline 0 -menu .m.v.l

menu .m.v.font
foreach f $fontList {
	.m.v.font add radiobutton -label "$f" -variable fontName -value $f -command {fontInit}
}

menu .m.v.f
.m.v.f add radiobutton -label "[word Largest]" -variable fontSizeInput -value 3 -command {fontSizeSet $fontSizeInput}
.m.v.f add radiobutton -label "[word Large]" -variable fontSizeInput -value 1 -command {fontSizeSet $fontSizeInput}
.m.v.f add radiobutton -label "[word Normal]" -variable fontSizeInput -value 0 -command {fontSizeSet $fontSizeInput}
.m.v.f add radiobutton -label "[word Small]" -variable fontSizeInput -value -1 -command {fontSizeSet $fontSizeInput}
.m.v.f add radiobutton -label "[word Smallest]" -variable fontSizeInput -value -2 -command {fontSizeSet $fontSizeInput}

menu .m.v.l
.m.v.l add radiobutton -label {English} -underline 0 -variable language -value 0 -command mainInit
.m.v.l add radiobutton -label {Japanese} -underline 0 -variable language -value jp -command mainInit

menu .m.v.mode
.m.v.mode add radiobutton -label "[word Normal]" -underline 0 {*}$option -variable userModeLevel -value 0 -command mainInit
.m.v.mode add radiobutton -label "[word Expert]" -underline 0 {*}$option -variable userModeLevel -value 1 -command mainInit
.m.v.mode add radiobutton -label "[word Debugger]" -underline 0 {*}$option -variable userModeLevel -value 2 -command mainInit

menu .m.v.term
.m.v.term add radiobutton -label {Force} -underline 0 -variable termForce -value 0 -command {termUpdate; mainInit}
.m.v.term add radiobutton -label {Load} -underline 0 -variable termForce -value 1 -command {termUpdate; mainInit}
.m.v.term add separator
.m.v.term add radiobutton -label {Depth} -underline 0 -variable termDepth -value 0 -command {termUpdate; mainInit}
.m.v.term add radiobutton -label {Displacement} -underline 1 -variable termDepth -value 1 -command {termUpdate; mainInit}

menu .m.c
.m.c add command -label "[word Plot]" -underline 0 {*}$option -command forceAnalyze
.m.c add command -label "[word {Show plot data}]" -underline 0 {*}$option -command forceDepth

menu .m.r
.m.r add command -label "[word Run]" -underline 0 {*}$option -command runTemporary
.m.r add command -label "[word Run] ([word {Summary only}])" -underline 5 {*}$option -command runSummary
.m.r add command -label "[word {Show graph plot data}]" -underline 5 {*}$option -command runError
.m.r add separator
.m.r add command -label "[word Run] ([word Debug])" -underline 5 {*}$option -command runDebug
.m.r add command -label "[word {Browse current XML}]" -underline 0 {*}$option -command seeCurrent
.m.r add command -label "[word Run] ([word {saved XML}])" -underline 7 {*}$option -command runFile
.m.r add command -label "[word {Browse saved XML}]" -underline 13 {*}$option -command seeXML

menu .m.help
.m.help add command -label "[word {How to use}]" -underline 0 {*}$option -command {openHelp [help howto]}
.m.help add separator
.m.help add command -label "[word {Analysis}]" -underline 0 {*}$option -command {openHelp [help analysis]}
.m.help add command -label "[word {Bad curve}]" -underline 0 {*}$option -command {openHelp [help bad]}
.m.help add command -label "[word {Column setting}]" -underline 0 {*}$option -command {openHelp [help column]}
.m.help add command -label "[word {Database}]" -underline 0 {*}$option -command {openHelp [help matdata]}
.m.help add command -label "[word {Debug}]" -underline 1 {*}$option -command {openHelp [help debug]}
.m.help add command -label "[word {Graph}]" -underline 0 {*}$option -command {openHelp [help graph]}
.m.help add command -label "[word {ISO14577}]" -underline 0 {*}$option -command {openHelp [help ISO]}
.m.help add command -label "[word {Option}]" -underline 0 {*}$option -command {openHelp [help option]}
.m.help add command -label "[word {Plot data}]" -underline 0 {*}$option -command {openHelp [help plot]}
.m.help add command -label "[word {Tips}]" -underline 0 {*}$option -command {openHelp [help tips]}
.m.help add command -label "[word {Unit}]" -underline 0 {*}$option -command {openHelp [help unit]}
.m.help add command -label "[word Value] ([greek b], [greek h]IT)" -underline 0 {*}$option -command {openHelp [help parameters]}

menu .m.about
.m.about add command -label "${readme}.txt" -underline 0 {*}$option -command openReadMe
.m.about add command -label "[word {System Info}]" -underline 0 {*}$option -command {openHelp [help info]}
.m.about add command -label "[word {Copyright}]" -underline 0 {*}$option -command license
.m.about add command -label "[word {Website}]" -underline 0 {*}$option -command {openURL [word {$homePage}]}
.m.about add command -label "[word {Check Update}]" -underline 6 {*}$option -command {updateCheck}
.m.about add command -label "[word History]" -underline 0 {*}$option -command {openHelp [help history]}
.m.about add command -label "[word {About}]" -underline 0 {*}$option -command about

myTooltip .m.about -index 3 "[word {$homePage}]"

. configure -menu .m
myWindowSize
}

# hideColumn : hide some columns in specimen table
proc hideColumn {} {
global wSpecSetting
global hiddenColumn

set new [concat [grid slaves $wSpecSetting -column 7] [grid slaves $wSpecSetting -column 8] [grid slaves $wSpecSetting -column 9]]
if {[string length $new] > 0} {
	grid remove {*}$new
	lappend hiddenColumn {*}$new
}
}

# showColumn : unhide columns in specimen table
proc showColumn {} {
global wSpecSetting
global hiddenColumn

if {[string length $hiddenColumn] > 0} {
	grid {*}$hiddenColumn
	set hiddenColumn {}
}
}

# updateColumn : update columns in specimen table
proc updateColumn {} {
global userModeLevel
if {$userModeLevel == 0} { # Normal
	hideColumn
} else { # Expert, Debugger
	showColumn
}
}

# mainInit : initialize main window
proc mainInit {} {
global configName
global xmlFileName
global depthColumn depthFactor
global forceColumn forceFactor
global skipLine
global cfMode cfModeList
global youngMode youngModeList
global hardnessMode hardnessModeList
global unloadLower unloadUpper
global optString
global specList
global interpolateAh interpolateCh
global userModeLevel
global beta
# widget names necessary outside of the procedure
global wDepthColumn
global wDepthFactor
global wForceColumn
global wForceFactor
global wSkip
global wSpecSetting
global wEMode
global wHMode
# widget names used in this procedure
global wAutoCheck
global wAutoHelp
global wAutoSetting
global wBadCurveSetting
global wBadCurveHelp
global wBeta
global wBetaLabel
global wBetaHelp
global wCfLabel
global wCfMode
global wCfCheckLabel
global wCfFixedLabel3
global wCfFixedSetting
global wCfFixed
global wDebugSetting
global wDepthColumnLabel
global wDepthFactorLabel
global wDepthLabel1
global wDepthLabel3
global wDepthSetting
global wELabel
global wFileLabel
global wFittingSetting
global wForceColumnLabel
global wForceFactorLabel
global wForceLabel1
global wForceLabel2
global wForceLabel3
global wForceSetting
global wFrom
global wHLabel
global wHMode
global wHardnessLabel
global wLineSetting
global wMemo
global wMemoLabel
global wMethodHelp
global wMethodSetting
global wNoviceHelp
global wNoviceHowto
global wNovicePlot
global wNoviceRun
global wNoviceReadme
global wOption
global wOptionHelp
global wOptionSetting
global wPoissonLabel
global wSelectLabel
global wSkipLabel
global wSkipBad
global wSpacer
global wSpecLabel
global wTo
global wUnloadSetting
global wYoungAlternateLabel
global wYoungLabel
global hiddenColumn

set expand "-expand 1 -fill x"
set padding "-padx 2 -pady 1"
set wide "-padx 3 -pady 2"
set border 1
writePref
if {![winfo exists .setting]} {
set wSetting [frame .setting]
pack $wSetting {*}$expand -side top -anchor n

set wInfo [frame $wSetting.info]
pack $wInfo {*}$expand -side top

set wMemoLabel [label $wInfo.memolabel]
set wMemo [entry $wInfo.memo -width 16 -textvariable configName]
set wXml [label $wInfo.xml -textvariable xmlFileName -anchor w]
pack $wMemoLabel -side left
pack $wMemo $wXml {*}$expand -side left

set wSeparator1 [ttk::separator $wSetting.sep1 -orient horizontal]
pack $wSeparator1 {*}$expand -side top

set wFileSetting [frame $wSetting.file]
pack $wFileSetting {*}$expand -side top

set wAutoSetting [labelframe $wFileSetting.auto -bd $border -labelanchor n]
set wAutoCheck [checkbutton $wAutoSetting.check -variable autocolumn -command autoColumnStateUpdate]
set wAutoHelp [ttk::button $wAutoSetting.help -text "?" -width 1 -command {openHelp [help column]}]
pack $wAutoCheck $wAutoHelp {*}$padding -side left

set wBadCurveSetting [labelframe $wFileSetting.bad -bd $border -labelanchor n]
set wSkipBad [checkbutton $wBadCurveSetting.skipbad -variable skipBad]
set wBadCurveHelp [ttk::button $wBadCurveSetting.help -text "?" -width 1 -command {openHelp [help bad]}]
pack $wSkipBad $wBadCurveHelp {*}$padding -side left

set wDepthSetting [labelframe $wFileSetting.depth -bd $border -labelanchor n]
set wDepthLabel [frame $wDepthSetting.depthlabel]
set wDepthLabel1 [label $wDepthLabel.1]
set wDepthLabel2 [label $wDepthLabel.2 -font MyItalic -text "h"]
set wDepthLabel3 [label $wDepthLabel.3]
pack $wDepthLabel1 $wDepthLabel2 $wDepthLabel3 -side left
$wDepthSetting configure -labelwidget $wDepthLabel
set wDepthColumnLabel [label $wDepthSetting.columnlabel]
set wDepthColumn [entry $wDepthSetting.column -textvariable depthColumn -justify right -width 8]
set wDepthFactorLabel [label $wDepthSetting.factorlabel]
set wDepthFactor [entry $wDepthSetting.factor -textvariable depthFactor -justify right -width 5]
pack $wDepthColumnLabel {*}$padding -side left
pack $wDepthColumn {*}$expand {*}$padding -side left
pack $wDepthFactorLabel {*}$padding -side left
pack $wDepthFactor {*}$expand {*}$padding -side left

set wForceSetting [labelframe $wFileSetting.force -bd $border -labelanchor n]
set wForceLabel [frame $wForceSetting.forcelabel]
set wForceLabel1 [label $wForceLabel.1 -padx 0 -anchor sw]
set wForceLabel2 [label $wForceLabel.2 -padx 0 -font MyItalic -anchor s]
set wForceLabel3 [label $wForceLabel.3 -padx 0 -anchor se]
pack $wForceLabel1 $wForceLabel2 $wForceLabel3 {*}$padding -side left
$wForceSetting configure -labelwidget $wForceLabel
set wForceColumnLabel [label $wForceSetting.columnlabel]
set wForceColumn [entry $wForceSetting.column -textvariable forceColumn -justify right -width 8]
set wForceFactorLabel [label $wForceSetting.factorlabel]
set wForceFactor [entry $wForceSetting.factor -textvariable forceFactor -justify right -width 5]
pack $wForceColumnLabel {*}$padding -side left
pack $wForceColumn {*}$expand {*}$padding -side left
pack $wForceFactorLabel {*}$padding -side left
pack $wForceFactor {*}$expand {*}$padding -side left

set wLineSetting [labelframe $wFileSetting.line -bd $border -labelanchor n]
set wSkipLabel [label $wLineSetting.skiplabel]
set wSkip [entry $wLineSetting.skip -textvariable skipLine -justify right -width 3]
pack $wSkipLabel $wSkip {*}$padding -side left

set wDebugSetting [labelframe $wFileSetting.debug -bd $border -labelanchor n]
set wCanvasSample [ttk::button $wDebugSetting.canvas -text "G" -width 2 -command {canvasSample}]
set wInputSample [ttk::button $wDebugSetting.input -text "I" -width 2 -command {myInput configName [word Memo] {}}]
pack $wCanvasSample $wInputSample {*}$padding -side left

pack $wAutoSetting $wBadCurveSetting {*}$padding -anchor s -side left
pack $wDepthSetting $wForceSetting {*}$expand {*}$padding -anchor s -side left
pack $wLineSetting {*}$padding -anchor s -side left

set wAnalysisSetting [frame $wSetting.analysis]
pack $wAnalysisSetting {*}$expand -side top

set wSpacer [label $wAnalysisSetting.spacer]

set wOptionSetting [labelframe $wAnalysisSetting.option -bd $border -labelanchor n]
set wOption [entry $wOptionSetting.option -width 10 -textvariable optString -validate key -vcmd "checkBadChar %P"]
set wOptionHelp [ttk::button $wOptionSetting.help -text "?" -width 1 -command {openHelp [help option]}]
pack $wOption {*}$expand {*}$padding -side left
pack $wOptionHelp {*}$padding -side left

set wUnloadSetting [labelframe $wAnalysisSetting.unload -bd $border -labelanchor n]
set wFrom [entry $wUnloadSetting.from -textvariable unloadLower -justify right -width 5]
set wToLabel [label $wUnloadSetting.tolabel -text "[special {-}]"]
set wTo [entry $wUnloadSetting.to -textvariable unloadUpper -justify right -width 5]
pack $wFrom {*}$expand {*}$padding -side left
pack $wToLabel -side left
pack $wTo {*}$expand {*}$padding -side left

set wCfFixedSetting [labelframe $wAnalysisSetting.cffixed -bd $border -labelanchor n]
set wCfFixedLabel [frame $wCfFixedSetting.cffixedlabel]
set wCfFixedLabel1 [label $wCfFixedLabel.1 -font MyItalic -padx 0 -text "C"]
set wCfFixedLabel2 [label $wCfFixedLabel.2 -font MySmallItalic -padx 0 -anchor s -height 2 -text "F "]
set wCfFixedLabel3 [label $wCfFixedLabel.3]
pack $wCfFixedLabel1 $wCfFixedLabel2 $wCfFixedLabel3 -side left
$wCfFixedSetting configure -labelwidget $wCfFixedLabel
set wCfFixed [entry $wCfFixedSetting.cf -textvariable cfSpecified -justify right -width 6]
set wCfFixedUnit [label $wCfFixedSetting.cfunit -text "[unit C]"]
pack $wCfFixed $wCfFixedUnit {*}$padding -side left

set wMethodSetting [labelframe $wAnalysisSetting.method -bd $border -labelanchor n]
set wBetaLabel [label $wMethodSetting.betalabel -font MyItalic -text " [greek b]"]
set wBeta [entry $wMethodSetting.beta -textvariable beta -justify right -width 6]
set wBetaHelp [ttk::button $wMethodSetting.betahelp -text "?" -width 1 -command {openHelp [help parameters]}]
set wCfLabel [frame $wMethodSetting.cflabel]
set wCfLabel1 [label $wCfLabel.1 -font MyItalic -padx 0 -text " C"]
set wCfLabel2 [label $wCfLabel.2 -font MySmallItalic -padx 0 -anchor s -height 2 -text "F "]
pack $wCfLabel1 $wCfLabel2 -side left
set wCfMode [ttk::combobox $wMethodSetting.cf -textvariable cfMode -value $cfModeList -width 6]
$wCfMode state readonly
set wELabel [label $wMethodSetting.elabel -font MyItalic -text " E "]
set wEMode [ttk::combobox $wMethodSetting.e -textvariable youngMode -value $youngModeList -width 4 -font MyItalic]
$wEMode state readonly
set wHLabel [frame $wMethodSetting.hlabel]
set wHLabel1 [label $wHLabel.1 -font MyItalic -padx 0 -text " H"]
set wHLabel2 [label $wHLabel.2 -font MySmallItalic -padx 0 -anchor s -height 2 -text "eq "]
pack $wHLabel1 $wHLabel2 -side left
set wHMode [ttk::combobox $wMethodSetting.h -textvariable hardnessMode -value $hardnessModeList -width 9 -font MyItalic]
$wHMode state readonly
set wMethodHelp [ttk::button $wMethodSetting.help -text "?" -width 1 -command {openHelp [help analysis]}]
pack $wBetaLabel -side left
pack $wBeta {*}$expand -side left
pack $wBetaHelp {*}$padding -side left
pack $wCfLabel -side left
pack $wCfMode {*}$expand -side left
pack $wELabel -side left
pack $wEMode {*}$expand -side left
pack $wMethodHelp {*}$padding -side left

set wFittingSetting [labelframe $wAnalysisSetting.fitting -bd $border -labelanchor n]
set wFittingAhLabel [label $wFittingSetting.ahlabel -font MyItalic -padx 0 -text "A(h) "]
set wFittingAh [checkbutton $wFittingSetting.ah -variable interpolateAh]
set wFittingChLabel [frame $wFittingSetting.chlabel]
set wFittingChLabel1 [label $wFittingChLabel.1 -font MyItalic -padx 0 -text "C"]
set wFittingChLabel2 [label $wFittingChLabel.2 -font MySmallItalic -padx 0 -anchor s -height 2 -text "H "]
set wFittingCh [checkbutton $wFittingSetting.ch -variable interpolateCh]
set wFittingHelp [ttk::button $wFittingSetting.help -text "?" -width 1 -command {openHelp [help debug]}]

set wNoviceHelp [labelframe $wAnalysisSetting.novice -bd $border -labelanchor n]
set wNovicePlot [button $wNoviceHelp.plot -anchor s -command {.m.c invoke 0}]
set wNoviceRun [button $wNoviceHelp.run -anchor s -command {.m.r invoke 0}]
set wNoviceHowto [button $wNoviceHelp.howto -anchor s -command {.m.help invoke 0}]
set wNoviceReadme [button $wNoviceHelp.readme -anchor s -command {.m.about invoke 0}]
pack $wNovicePlot $wNoviceRun $wNoviceHowto $wNoviceReadme {*}$expand {*}$wide -side left

pack $wFittingChLabel1 $wFittingChLabel2 -side left
pack $wFittingAh $wFittingAhLabel $wFittingCh $wFittingChLabel $wFittingHelp -side left

pack $wCfFixedSetting {*}$padding -anchor s -side left
pack $wMethodSetting {*}$padding -anchor s -side left

set wSpecSetting [frame $wSetting.spec]
pack $wSpecSetting {*}$expand {*}$padding -side top

set wSelectLabel [label $wSpecSetting.select]
set wFileLabel [label $wSpecSetting.file]
set wSpecLabel [label $wSpecSetting.specname]
set wPoissonLabel [label $wSpecSetting.poisson -font MyItalic -text [greek n]]
set wYoungLabel [label $wSpecSetting.young -anchor e -font MyItalic -text "_____E____"]
set wYoungAlternateLabel [label $wSpecSetting.alternative -font MyItalic -text "E'"]
set wHardnessLabel [frame $wSpecSetting.heq]
set wHardnessLabel1 [label $wHardnessLabel.1 -anchor e -font MyItalic -padx 0 -text " ____H"]
set wHardnessLabel2 [label $wHardnessLabel.2 -anchor e -font MySmallItalic -padx 0 -anchor s -height 2 -text "eq"]
set wHardnessLabel3 [label $wHardnessLabel.3 -anchor e -font MyItalic -text "___"]
pack $wHardnessLabel1 $wHardnessLabel2 $wHardnessLabel3 -side left

set wCfCheckLabel [frame $wSpecSetting.cf]
set wCfCheckLabel1 [label $wCfCheckLabel.1 -font MyItalic -padx 0 -text " C"]
set wCfCheckLabel2 [label $wCfCheckLabel.2 -font MySmallItalic -padx 0 -anchor s -height 2 -text "F"]
pack $wCfCheckLabel.1 $wCfCheckLabel.2 -side left

grid $wSelectLabel $wFileLabel $wSpecLabel $wCfCheckLabel $wPoissonLabel $wYoungLabel - $wYoungAlternateLabel $wHardnessLabel - -sticky ew
grid columnconfigure $wSpecSetting 2 -weight 1
set hiddenColumn {}
}; # end of new .setting

# check userModeLevel
if {$userModeLevel == 0} { # Normal
	pack forget $wUnloadSetting
	pack forget $wOptionSetting
	pack forget $wHLabel
	pack forget $wHMode
	pack $wNoviceHelp {*}$expand {*}$padding -anchor s -side left
} else { # Expert, Debugger
	pack forget $wNoviceHelp
	pack $wHLabel -side left -before $wMethodHelp
	pack $wHMode {*}$expand -side left -before $wMethodHelp
	pack $wUnloadSetting {*}$padding -anchor s -side left
	pack $wOptionSetting {*}$expand {*}$padding -anchor s -side left
}
if {$userModeLevel > 1} { # Debugger
	pack $wFittingSetting {*}$padding -anchor s -side left
	pack $wDebugSetting {*}$padding -anchor s -side left
} else {
	pack forget $wFittingSetting
	pack forget $wDebugSetting
}

# text which needs translation
$wMemoLabel configure -text "[word MEMO]"
$wAutoSetting configure -text "[word Read]"
$wAutoCheck configure -text "[word Auto]"
$wBadCurveSetting configure -text "[word {Bad Curve}]"
$wSkipBad configure -text "[word Skip]"
$wDepthLabel1 configure -text "[word [term DEPTH]] ("
$wDepthLabel3 configure -text ") [word {Setting}]"
$wDepthColumnLabel configure -text "[word Column]"
$wDepthFactorLabel configure -text "[word Factor]"
$wForceLabel1 configure -text "[word [term FORCE]] ("
$wForceLabel2 configure -text "[term F]"
$wForceLabel3 configure -text ") [word {Setting}]"
$wForceColumnLabel configure -text "[word Column]"
$wForceFactorLabel configure -text "[word Factor]"
$wLineSetting configure -text "[word LINE]"
$wDebugSetting configure -text "[word Debug]"
$wSkipLabel configure -text "[word Skip]"
$wFittingSetting configure -text "[word Interpolation]"
$wMethodSetting configure -text "[word Analysis]"
$wUnloadSetting configure -text "[word {UNLOAD fitting}]"
$wOptionSetting configure -text "[word Option]"
$wCfFixedLabel3 configure -text "[word {Setting}]"
$wNoviceHelp configure -text "[word Menu] [word Shortcut]"
$wNovicePlot configure -text "[word Plot]"
$wNoviceRun configure -text "[word Run]"
$wNoviceHowto configure -text "[word How-to]"
$wNoviceReadme configure -text "[word Document]"

$wSelectLabel configure -text "[word Select]"
$wFileLabel configure -text "[word File]"
$wSpecLabel configure -text "[word {Specimen name}] ([word necessary])"

# tooltip
myTooltip $wMemo "[word {Free input}]"
myTooltip $wAutoSetting "[word {Column setting}] [word Auto] on/off"
myTooltip $wAutoHelp "[word {Column setting}] [word Help]"
myTooltip $wBadCurveSetting "[word {Bad curve}] [word setting]"
myTooltip $wBadCurveHelp "[word {Bad curve}] [word setting] [word Help]"
myTooltip $wDepthSetting "[word [term Depth]] [word Read] [word setting]"
myTooltip $wForceSetting "[word [term Force]] [word Read] [word setting]"
myTooltip $wLineSetting "[word LINE] [word Skip] [word setting]"

myTooltip $wFrom "[word {Lower Limit}]"
myTooltip $wTo "[word {Upper Limit}]"
myTooltip $wOption "[word Option] [word {Direct input}]"
myTooltip $wOptionHelp "[word Option] [word Help]"
myTooltip $wOptionSetting "[word Option] [word setting]"
myTooltip $wCfFixedSetting "[word {Frame compliance}] [word {Fixed value}]"
myTooltip $wCfFixed "[word {Frame compliance}] [word {Direct input}], [word {Leave blank to calc}]"
myTooltip $wMethodSetting "[word Analysis] [word Setting]"
myTooltip $wBeta "[greek b] [word Setting]"
myTooltip $wBetaHelp "[greek b] [word Setting] [word Help]"
myTooltip $wCfLabel "[word {frame compliance}]"
myTooltip $wCfMode "[word {frame compliance}] [word Fitting]"
myTooltip $wELabel "[word {Young's modulus}]"
myTooltip $wEMode "[word {Young's modulus}] [word Calculation] [word Setting]"
myTooltip $wHLabel "[word {Equivalent Hardness}]"
myTooltip $wHMode "[word {Equivalent Hardness}] [word Calculation] [word Setting]"
myTooltip $wMethodHelp "[word Analysis] [word Help]"
myTooltip $wFittingSetting "[word Interpolation] [word Setting] ([word Debug])"
myTooltip $wNoviceHelp "[word Useful] [word Shortcut]"
myTooltip $wNovicePlot "[word Curve]"
myTooltip $wNoviceRun "[word Analyze]"
myTooltip $wNoviceHowto "[word Help]"
myTooltip $wNoviceReadme "readme.txt"

myTooltip $wSelectLabel "[word {for Edit}]"
myTooltip $wFileLabel "[word {Data files}]"
myTooltip $wSpecLabel "[word {Specimen name}]: [word {single-byte characters only}]"
myTooltip $wCfCheckLabel "[word {Frame compliance}] [word Calculation]: [word {Choose any number of standard specimens}]]"
myTooltip $wPoissonLabel "[word {Poisson's ratio}]: [word {treated as 0.3 if empty}]"
myTooltip $wYoungLabel "[word {Young's modulus}] [word Calculation]: [word {Choose a standard specimen}]"
myTooltip $wYoungAlternateLabel "[word {Young's modulus}] ([word Alternate]) [word Calculation]: [word {Young's modulus}] (E) [word {Calculation}] [word necessary]"
myTooltip $wHardnessLabel "[word {Equivalent Hardness}] [word Calculation]: [word {Choose a standard specimen}]]"

foreach i $specList {updateSpec $i}
updateColumn
menuInit
}

# autoColumnStateUpdate : update main widget status by auto column setting
proc autoColumnStateUpdate {} {
global autocolumn
global wDepthColumn
global wDepthFactor
global wForceColumn
global wForceFactor
global wSkip

if {![winfo exist $wDepthColumn]} {return}
set columnset [list $wDepthColumn $wDepthFactor $wForceColumn $wForceFactor $wSkip]

if {$autocolumn == 1} {
	foreach x $columnset {$x configure -state disabled}
} else {
	foreach x $columnset {$x configure -state normal}
}
}

# youngStateUpdate : update main widget status by Young's modulus
proc youngStateUpdate {} {
global specList
global vRefArea vRefAreaLast
global youngMode youngModeList
global wSpecSetting
global wEMode

if {[lsearch -exact $youngModeList $youngMode] == -1} {set youngMode [lindex $youngModeList 0]}
if {$vRefArea == $vRefAreaLast} {set vRefArea 0}
set vRefAreaLast $vRefArea
if {$vRefArea == 0} {
	$wEMode state disabled
} else {
	$wEMode state !disabled
}
}

# poissonStateUpdate : update main widget status by Poisson's ratio
proc poissonStateUpdate {widget text} {
global vRefArea

set w [regsub -- "-young" $widget "-poiref"]
if {$text > 0} {
	$w configure -state normal
	if [string equal $vRefArea "0"] {
		set vRefArea [regsub -all -- {[^0-9]} $widget {}]
		youngStateUpdate
	}
} else {
	$w deselect
	$w configure -state disabled
}
return true
}

# hardStateUpdate : update main widget status by equivalent hardness Heq
proc hardStateUpdate {} {
global specList
global vRefHeq vRefHeqLast
global hardnessMode hardnessModeList
global wSpecSetting
global wHMode

if {[lsearch -exact $hardnessModeList $hardnessMode] == -1} {set hardnessMode [lindex $hardnessModeList 0]}
if {$vRefHeq == $vRefHeqLast} {set vRefHeq 0}
set vRefHeqLast $vRefHeq
if {[winfo exists $wHMode]} {
	if {$vRefHeq == 0} {
		$wHMode state disabled
	} else {
		$wHMode state !disabled
	}
}
}

# checkMatData : check material database
proc checkMatData {widget text} {
global matdata
global vPoisson
global vYoung
global vHardness
global readingXML

if {[checkBadChar $text] == false} {return false}
if [info exists readingXML] {return true}; # ignore material data when reading from file
set keyword [string tolower $text]

if {![dict exists $matdata $keyword]} {return true}
set v [dict get $matdata $keyword]
set i [regsub -all -- {[^0-9]} $widget {}]

if {[string length "$vYoung($i)$vPoisson($i)$vHardness($i)"] == 0} {
	if {[llength $v] > 1} {set vYoung($i) [lindex $v 1]}
	if {[llength $v] > 2} {set vPoisson($i) [lindex $v 2]}
	if {[llength $v] > 3} {set vHardness($i) [lindex $v 3]}
	hardStateUpdate
}
return true
}

# numFileStateUpdate : show number of files
proc numFileStateUpdate {i} {
global specList
global vNumFile
global vFileList
if {[lsearch -exact $specList $i] > -1} { 
	set n [llength $vFileList($i)]
	if {$n > 0} {
		set vNumFile($i) ($n)
	} else {
		set vNumFile($i) {[+]}
	}
}
}

# specimen list
# $i = specimen number, unique, 1-origin
# specList = list of current specimen numbers, can be ordered manually by exchanging specimens
# specimen widget = .di.$i-*** in grid row $i (1-origin)
# grid row can not be removed or re-numbered, but become invisible if all slave widgets are removed
# new grid row can be added only at the bottom

# clearSpec : clear specimen
proc clearSpec {i} {
global specList
global vCheck
global vName
global vCf
global vPoisson
global vRefArea
global vYoung
global vRefHeq
global vHardness
global vPoiref
global vFileList

if {$vRefArea == $i} {set vRefArea 0}
if {$vRefHeq == $i} {set vRefHeq 0}

if {[lsearch -exact $specList $i] < 0} {return}
set vCheck($i) 0
set vName($i) {}
set vCf($i) 0
set vPoisson($i) {}
set vYoung($i) {}
set vHardness($i) {}
set vPoiref($i) 0
set vFileList($i) {}
numFileStateUpdate $i
}

# delSpec : delete specimen
proc delSpec {i} {
global specList
global wSpecSetting

set k [lsearch -exact $specList $i]
if {$k < 0} {return}
clearSpec $i
set specList [lreplace $specList $k $k]
showColumn
destroy {*}[grid slaves $wSpecSetting -row $i]
updateColumn
}

# updateSpec : update specimen widget
proc updateSpec {i} {
global wSpecSetting

myTooltip $wSpecSetting.${i}-sel "[word {for Edit}]"
myTooltip $wSpecSetting.${i}-file "[word {Data files}]"
myTooltip $wSpecSetting.${i}-spec "[word {single-byte characters only}]"
myTooltip $wSpecSetting.${i}-cf "[word {Frame compliance}] [word Calculation]: [word {Choose any number of standard specimens}]]"
myTooltip $wSpecSetting.${i}-poi "[word {Poisson's ratio}] [word Input]: [word {treated as 0.3 if empty}]"
myTooltip $wSpecSetting.${i}-area "[word {Young's modulus}] [word Calculation]: [word {Choose a standard specimen}]"
myTooltip $wSpecSetting.${i}-young "[word {Standard specimen}] [word {Young's modulus}] [word Input]"
myTooltip $wSpecSetting.${i}-poiref "[word {Young's modulus}] ([word Alternate]) [word Calculation]: [word {Choose at least 3 standard specimens}], [word {Poisson's ratio}] [word {not necessary}]"
myTooltip $wSpecSetting.${i}-hard "[word {Equivalent Hardness}] [word Calculation]: [word {Choose a standard specimen}]]"
myTooltip $wSpecSetting.${i}-hardness "[word {Standard specimen}] [word Hardness] [word Input]"
updateColumn
}

# addSpec : add a specimen
proc addSpec {i} {
global specList
global vCheck
global vNumFile
global vName
global vCf
global vPoisson
global vRefArea
global vYoung
global vRefHeq
global vHardness
global vPoiref
global wSpecSetting

if {[lsearch -exact $specList $i] > -1} {return}
lappend specList $i
clearSpec $i

checkbutton $wSpecSetting.${i}-sel -variable vCheck($i)
ttk::button $wSpecSetting.${i}-file -textvariable vNumFile($i) -command "fileSelect $i" -width 5
entry $wSpecSetting.${i}-spec -textvariable vName($i) -validate key -vcmd "checkMatData %W %P"
checkbutton $wSpecSetting.${i}-cf -variable vCf($i)
entry $wSpecSetting.${i}-poi -textvariable vPoisson($i) -justify right -width 5
radiobutton $wSpecSetting.${i}-area -variable vRefArea -value $i -command youngStateUpdate
entry $wSpecSetting.${i}-young -textvariable vYoung($i) -justify right -width 6 -validate key -vcmd "poissonStateUpdate %W %P"
radiobutton $wSpecSetting.${i}-hard -variable vRefHeq -value $i -command hardStateUpdate
entry $wSpecSetting.${i}-hardness -textvariable vHardness($i) -justify right -width 6
checkbutton $wSpecSetting.${i}-poiref -variable vPoiref($i)
grid $wSpecSetting.${i}-sel $wSpecSetting.${i}-file $wSpecSetting.${i}-spec $wSpecSetting.${i}-cf $wSpecSetting.${i}-poi $wSpecSetting.${i}-area $wSpecSetting.${i}-young $wSpecSetting.${i}-poiref $wSpecSetting.${i}-hard $wSpecSetting.${i}-hardness -sticky ew -row $i
grid configure $wSpecSetting.${i}-spec -padx 6
updateSpec $i
poissonStateUpdate $wSpecSetting.${i}-young $vYoung($i)
}

# clearCheck : clear checked specimen
proc clearCheck {} {
global specList
global vCheck

foreach i $specList {if {$vCheck($i) == 1} {clearSpec $i}}
}

# delCheck : delete checked specimen
proc delCheck {} {
global specList
global vCheck

foreach i $specList {if {$vCheck($i) == 1} {delSpec $i}}
myWindowSize
}

# addCheck : add a specimen if not already max
proc addCheck {} {
global maxSpec
global specList
global wSpecSetting

if {[llength $specList] < $maxSpec} {
	lassign [grid size $wSpecSetting] c r
	addSpec $r
}
myWindowSize
}

# swapCheck : swap checked specimens
proc swapCheck {} {
global specList
global vCheck
global vNumFile
global vName
global vCf
global vPoisson
global vRefArea
global vYoung
global vRefHeq
global vHardness
global vPoiref
global vFileList

set j {}
set k {}
foreach i $specList {
	if {$vCheck($i) == 1} {
		set vCheck($i) 0
		if {[string length $j] == 0} {
			set j $i
		} else {
			set k $i
		}
	}
}
if {[string length $k] == 0} {return}
swap vCf($j) vCf($k)
swap vPoisson($j) vPoisson($k)
swap vYoung($j) vYoung($k)
swap vHardness($j) vHardness($k)
swap vPoiref($j) vPoiref($k)
swap vFileList($j) vFileList($k)
swap vNumFile($j) vNumFile($k)
swap vName($j) vName($k); #must be last due to database function
if {$vRefArea == $j} {
	set vRefArea $k
	youngStateUpdate
} else {
	if {$vRefArea == $k} {
		set vRefArea $j
		youngStateUpdate
	}
}
if {$vRefHeq == $j} {
	set vRefHeq $k
	hardStateUpdate
} else {
	if {$vRefHeq == $k} {
		set vRefHeq $j
		hardStateUpdate
	}
}
}

# fileSelect : select data files
proc fileSelect {x} {
global vName
global vFileList
global savedFileList
global wData wList
set t $wData$x
if {![winfo exists $t]} {
	toplevel $t -height 200
	wm title $t "$vName($x): [word {Data files}]"
	set width 50
	foreach i $vFileList($x) {
		if {[string length $i] > $width} {
			set width [string length $i]
		}
	}
	incr width 30
	set wFileMain [frame $t.main]
	set wFileList [listbox $t$wList -selectmode extended -width $width -height 8 -listvariable vFileList($x)]
	set wFileBar [scrollbar $t.bar -command "$t$wList yview"]
	$wFileList configure -yscrollcommand "$wFileBar set"
	pack $wFileBar -fill both -expand 0 -side right -in $wFileMain
	pack $wFileList -fill both -expand 1 -side right -in $wFileMain

	set wFooter [frame $t.footer -height 200]
	set wFileAdd [ttk::button $wFooter.add -text "[word Add]" -width 5 -command "addFileSelect $x"]
	set wFilePlot [ttk::button $wFooter.plot -text "[term F]-h" -width 5 -style MyItalic.TButton -command "plotFileSelect $x"]
	set wFileSort [ttk::button $wFooter.sort -text "[word Sort]" -width 6 -command "sortFileSelect $x"]
	set wFileRemove [ttk::button $wFooter.remove -text "[word Remove]" -width 7 -command "removeFileSelect $x"]
	set wFileClear [ttk::button $wFooter.clear -text "[word {Remove All}]" -width 11 -command "clearFileSelect $x"]
	set wFileCancel [ttk::button $wFooter.cancel -text "[word Cancel]" -width 7 -command "cancelFileSelect $x"]
	set wFileBack [ttk::button $wFooter.back -text "OK" -width 4 -command "closeFileSelect $x"]
	pack $wFileBack $wFileCancel $wFileClear $wFileRemove $wFileSort $wFilePlot $wFileAdd -side right
	pack $wFooter -fill both -expand 0 -side bottom 
	pack $wFileMain -side bottom -expand 1 -fill both
	update
	windowPosition $t . cover
} else {
	myFocus $t
}
myWindowSize
set savedFileList $vFileList($x)
}

# closeFileSelect
proc closeFileSelect {x} {
global wData
destroy $wData$x
numFileStateUpdate $x
}

# cancelFileSelect
proc cancelFileSelect {x} {
global wData
global vFileList
global savedFileList
set vFileList($x) $savedFileList
destroy $wData$x
}

# addFileSelect
proc addFileSelect {x} {
global wRun wGraphAll wData wList
global xmlFileName
global vFileList
global vName
if {[llength $vFileList($x)] > 0} {
	set initdir [file dirname [lindex $vFileList($x) end]]
} else {
	set initdir [file dirname $xmlFileName]
}
set datatypes {
    {{Data files} {.csv .tsv .txt .xlsx .CSV .TSV .TXT .XLSX} }
}
foreach i [tk_getOpenFile -multiple 1 -filetypes $datatypes -initialdir $initdir -parent $wData$x] {
	if {[lsearch -exact $vFileList($x) $i] == -1} {
		$wData$x$wList insert end $i
		destroy $wRun $wGraphAll
	}
}
if {([string length $vName($x)] == 0) && ([llength $vFileList($x)] > 0)} {
	foreach i $vFileList($x) {
		set words [lreverse [file split $i]]
		foreach j $words {
			set vName($x) [regexp -inline -- {[a-zA-Z][a-zA-Z0-9]*} [file rootname $j]]
			if {[string length $vName($x)] > 0} {break}
		}
		if {[string length $vName($x)] > 0} {break}
	}
	if {[string length $vName($x)] == 0} {
		set vName($x) "specimen $x"
	}
}
}

# removeFileSelect
proc removeFileSelect {x} {
global wData wList
global vFileList
set select [lreverse [$wData$x$wList curselection]]
foreach i $select {
	$wData$x$wList delete $i
}
}

# sortFileSelect
proc sortFileSelect {x} {
global vFileList
set vFileList($x) [lsort -dictionary $vFileList($x)]
}

# plotFileSelect
proc plotFileSelect {x} {
global wData wList
global vFileList
set select [$wData$x$wList curselection]
set selList {}
foreach i $select {
	lappend selList [lindex $vFileList($x) $i]
}
if {[llength $selList] > 0} {
	forceAnalyze $selList
} else {
	if {[llength $vFileList($x)] > 0} {
		forceAnalyze $vFileList($x)
	}
}
}

# clearFileSelect
proc clearFileSelect {x} {
global wData wList
if {[$wData$x$wList size] > 0} {
	$wData$x$wList delete 0 end
}
}

# clearConfig
proc clearConfig {} {
global configName
global configVersion
global unloadLower
global unloadUpper
global autocolumn
global skipBad
global depthColumn
global forceColumn
global depthFactor
global forceFactor
global skipLine
global cfSpecified
global cfModeList
global cfMode
global youngModeList
global youngMode
global hardnessModeList
global hardnessMode
global vRefArea
global vRefAreaLast
global vRefHeq
global vRefHeqLast
global specList
global interpolateAh interpolateCh
global initialSpecNum
global pickCheck
global classCheck
global beta
#global optString

windowClear
foreach i $specList {delSpec $i}

set configName {}
set configVersion {}
set beta {}
set unloadLower 0.80
set unloadUpper 0.98
set autocolumn 1
set depthColumn {}
set forceColumn {}
set depthFactor 1
set forceFactor 1
set skipLine 0
set cfSpecified {}
set cfMode [lindex $cfModeList 0]
set youngMode [lindex $youngModeList 0]
set hardnessMode [lindex $hardnessModeList 0]
set vRefArea 0
set vRefAreaLast 0
set vRefHeq 0
set vRefHeqLast 0
set specList {}
set skipBad 0
set interpolateAh 0
set interpolateCh 0
array unset pickCheck
array unset classCheck

for {set i 1} {$i <= $initialSpecNum} {incr i} {addSpec $i}

autoColumnStateUpdate
youngStateUpdate
hardStateUpdate
}

# renew : clear all
proc renew {{filename ""}} {
global xmlFileName
set xmlFileName $filename
clearConfig
initPlot
mainWindowTitle
myWindowSize
myNoBusy
}

# readMatData
proc readMatData {} {
global execdir
global matdata

set matfile [file join $execdir "materials.txt"]
if {![file readable $matfile]} {return}
set fd [open $matfile]
while {![eof $fd]} {
	gets $fd line
	set v [split $line ,]
	set l [llength $v]
	if {$l < 2} {continue}
	set name [string trim [lindex $v 0]]
	if {[string length $name] == 0} {continue}
	if {[string index $name 0] == "#"} {continue}

	set keyword [string tolower $name]
	dict set matdata $keyword $v
}
}

# readxml : read XML file
proc readxml {} {
global xmlFileName
global configVersion
global configName
global unloadLower
global unloadUpper
global autocolumn
global depthColumn
global forceColumn
global depthFactor
global forceFactor
global skipBad
global skipLine
global cfSpecified
global vRefArea
global vRefHeq
global vPoiref
global vName
global vCf
global cfMode
global youngMode
global hardnessMode
global vYoung
global vPoisson
global vHardness
global vFileList
global specList
global beta
global readingXML
set backup $xmlFileName
set types {
    {{XML Files} {.xml .XML} }
}
set xmlFileName [tk_getOpenFile -filetypes $types -initialdir [file dirname $backup]]
if {![file readable $xmlFileName]} then {
	set xmlFileName $backup
	return
}
renew $xmlFileName
set fd [open $xmlFileName]
set d 0
set folder(0) {}
set readingXML 1
while {![eof $fd]} {
	gets $fd line
	if {[string first "<dataset" $line] > -1} {
		incr d
		addSpec $d
		set folder($d) {}
		if {[string first "cf=" $line] > -1} {set vCf($d) 1}
		if {[string first "poiref=" $line] > -1} {set vPoiref($d) 1}
		if {[string first "reference=" $line] > -1} {set vRefArea $d}
		if {[string first "hardref=" $line] > -1} {set vRefHeq $d}
	}
	set n [regsub {[^<]*<([a-z_]+)[^<>]*>(.*)</\1.*} $line {\1=\2} ret]
	if {$n > 0} {
		set word [regsub {=.*} $ret {}]
		set content [regsub {^[^=]+=} $ret {}]
		switch $word {
			version {set configVersion $content}
			name {set configName $content}
			beta {set beta $content}
			unload_lower {set unloadLower $content}
			unload_upper {set unloadUpper $content}
			depth_column {set depthColumn $content}
			force_column {set forceColumn $content}
			depth_factor {set depthFactor $content}
			force_factor {set forceFactor $content}
			skip_bad {set skipBad $content}
			skip_line {set skipLine $content}
			cf {set cfSpecified $content}
			cf_mode {set cfMode $content}
			young_mode {set youngMode $content}
			hardness_mode {set hardnessMode $content}
			specimen {set vName($d) $content}
			young {set vYoung($d) $content}
			poisson {set vPoisson($d) $content}
			hardness {set vHardness($d) $content}
			folder {set folder($d) $content}
			file {
				set p [file normalize [file join "." [file dirname $xmlFileName] $folder(0) $folder($d) $content]]
				set fl [glob -type f -nocomplain $p]
				foreach i $fl {
					if {$d <= [array size vFileList]} {
						if {[lsearch -exact $vFileList($d) $i] == -1} {
							lappend vFileList($d) $i
						}
					} else {
						set vFileList($d) $i
					}
				}
			}
		}
	}
}
close $fd
set autocolumn 0
if {[string length $depthColumn] == 0} {set autocolumn 1}
if {[string length $forceColumn] == 0} {set autocolumn 1}
unset readingXML

autoColumnStateUpdate
youngStateUpdate
hardStateUpdate
foreach i $specList {numFileStateUpdate $i}
myWindowSize
}

# saveasXML
proc saveasXML {} {
global xmlFileName
set types {
    {{XML Files} {.xml .XML} }
}
set xmlFileName [tk_getSaveFile -filetypes $types -initialdir [file dirname $xmlFileName] -initialfile [file tail $xmlFileName] -defaultextension .xml]
writeXML $xmlFileName
}

# overwriteXML
proc overwriteXML {} {
global xmlFileName
if {[file writable $xmlFileName]} {
	writeXML $xmlFileName
} else {
	saveasXML
}
}

# writeXML : write XML to file
proc writeXML {fname {selList ""}} {
if {[string length $fname] == 0} {return}
mainWindowTitle
set outfile [open $fname w]
chan configure $outfile -translation {auto lf} -encoding utf-8
puts $outfile [makeXML $selList]
close $outfile
}

# makeXML : return XML content from current setting
proc makeXML {{selList ""}} {
# selList: file list to print
global xmlFileName
global configName
global configVersion
global unloadLower
global unloadUpper
global autocolumn
global depthColumn
global forceColumn
global depthFactor
global forceFactor
global skipBad
global skipLine
global cfSpecified
global vRefArea
global vRefHeq
global vPoiref
global vName
global vPoisson
global vCf
global cfMode
global youngMode
global hardnessMode
global vYoung
global vHardness
global vFileList
global specList
global beta

set basedir [file split $xmlFileName]
set baselevel [llength $basedir]

set xmltext "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
append xmltext "<!DOCTYPE root SYSTEM \"tangent.dtd\">\n"
append xmltext "<root>\n<config>\n"
append xmltext "<version>$configVersion</version>\n"
append xmltext "<propername>[file tail $xmlFileName]</propername>\n"
append xmltext "<name>$configName</name>\n"
append xmltext "<beta>$beta</beta>\n"
append xmltext "<unload_lower>$unloadLower</unload_lower>\n"
append xmltext "<unload_upper>$unloadUpper</unload_upper>\n"
if {$autocolumn == 1} {
	append xmltext "<depth_column></depth_column>\n"
	append xmltext "<force_column></force_column>\n"
} else {
	append xmltext "<depth_column>$depthColumn</depth_column>\n"
	append xmltext "<force_column>$forceColumn</force_column>\n"
}
append xmltext "<depth_factor>$depthFactor</depth_factor>\n"
append xmltext "<force_factor>$forceFactor</force_factor>\n"
append xmltext "<skip_bad>$skipBad</skip_bad>\n"
append xmltext "<skip_line>$skipLine</skip_line>\n"
append xmltext "<cf>$cfSpecified</cf>\n"
append xmltext "<cf_mode>$cfMode</cf_mode>\n"
append xmltext "<young_mode>$youngMode</young_mode>\n"
append xmltext "<hardness_mode>$hardnessMode</hardness_mode>\n"
append xmltext "</config>\n"

foreach i $specList {
if {[string length $vName($i)] == 0} {continue}
set attr {}
if {[info exists vCf($i)]} {
	if {$vCf($i) > 0} {append attr { cf="1"}}
}
if {[info exists vPoiref($i)]} {
	if {$vPoiref($i) > 0} {append attr { poiref="1"}}
}
if {$vRefArea == $i} {append attr { reference="1"}}
if {$vRefHeq == $i} {append attr { hardref="1"}}
append xmltext "\n<dataset$attr>\n"
append xmltext "<specimen>$vName($i)</specimen>\n"
if {[string length $vYoung($i)] > 0} {append xmltext "<young>$vYoung($i)</young>\n"}
if {[string length $vPoisson($i)] > 0} {append xmltext "<poisson>$vPoisson($i)</poisson>\n"}
if {[string length $vHardness($i)] > 0} {append xmltext "<hardness>$vHardness($i)</hardness>\n"}
if [info exists vFileList($i)] {
foreach f $vFileList($i) {
	if {[string length $selList] > 0} {
		if {[lsearch -exact $selList $f] == -1} {continue}
	}
	set fdir [file split $f]
	set flevel [llength $fdir]
	set relativedir {}
	for {set j 0} {$j < $baselevel && $j < $flevel} {incr j} {
		if {[lindex $fdir $j] != [lindex $basedir $j]} {
			set difflevel [expr ($baselevel - $j - 1)]
			set updir {}
			if {$difflevel > 0} {
				set updir [lrepeat $difflevel ".."]
			}
			set f [file join {*}$updir {*}[lrange $fdir $j end]]
			break
		}
	}
	append xmltext "<file>$f</file>\n"
}
}
append xmltext "</dataset>\n"
}
append xmltext "</root>\n"
return $xmltext
}

# runSaveas : save content in run window as filename
proc runSaveas {type} {
global wRun
global wRunText
global xmlFileName
global progname
switch $type {
	.csv {set types {{{CSV Files} {.csv .CSV} }}}
	.xml {set types {{{XML Files} {.xml .XML} }}}
	default {set types {{{Text Files} {.txt .TXT} }}}
}
set resultfile [file rootname [file tail $xmlFileName]]
if {[string length $resultfile] == 0} {set resultfile $progname}
append resultfile "_[clock seconds]$type"
set resultfile [tk_getSaveFile -parent $wRun -filetypes $types -initialdir [file dirname $xmlFileName] -initialfile $resultfile -defaultextension $type]
if {[string length $resultfile] > 0} {
	set outfile [open $resultfile w]
	puts $outfile [$wRunText get 1.0 end]
	close $outfile
}
}

# smartDivide : smart divide plot axis
# x is max value for the axis
proc smartDivide {x {step 0}} {
# step should not be changed if fixed > 0
set fixed 1
if {$step <= 0} {
	if {$x == 0} {return [list 0 1]}
	set fixed 0
	set step [expr (pow(10, floor(log10(abs($x)))))]
}
if {$x == 0} {return [list 0 $step]}

# Data should be within this value of full scale, 0.834 > 10/12
set target 0.834

set safelock 0
while {$safelock < 10} {; # for safety
	incr safelock
# necessary : necessary number of step as a full scale, should be 3.5-7
	set necessary [expr (abs($x)/$step/$target)]
# enough : necessary integer number of step as a full scale, should be 4-7
	set enough [expr (ceil($necessary))]
	if {$fixed > 0} {break}; # can't touch a fixed step.
	if {$necessary <= 1.4} {set step [expr ($step * 0.2)]; continue}; # will be <=7
	if {$necessary > 16.5} {set step [expr ($step * 5.0)]; continue}; # will be >3.5, won't happen
	if {$necessary >  7.0} {set step [expr ($step * 2.0)]; continue}; # will be >3.5
	if {$necessary <= 3.5} {set step [expr ($step * 0.5)]; continue}; # will be <=7
	break
}
if {$x > 0} {
	set limit [expr ($enough * $step)]
} else {
	set limit [expr (-$enough * $step)]
}
return [list $limit $step]
}

# smartRange : smart range and step for plot axis
proc smartRange {min max} {
if {$max < 0} {set max 0}
if {$min > 0} {set min 0}
if {$max >= -$min} {
	lassign [smartDivide $max] max step
	if {$min < 0} {lassign [smartDivide $min $step] min}
} else {; # max >= 0 > min && max < -min
	lassign [smartDivide $min] min step
	if {$max > 0} {lassign [smartDivide $max $step] max}
}
return [list $min $max $step]
}

# initPlot : initialize plot variables
proc initPlot {} {
global textPlottedPos
global plotSymbol plotName plotNameFh plotXPos plotYPos plotTPos plotLine plotMean
global plotScale plotScaleMax plotScaleMin
global pickCheck

set textPlottedPos {}
set plotSymbol 0
set plotName 1
set plotNameFh 1
set plotXPos 0
set plotYPos 0
set plotTPos 0
set plotLine 1
set plotMean 0
set plotScale 0.88
set plotScaleMax 1.0
set plotScaleMin 0.8
array unset pickCheck
#array set pickCheck {}; # this does NOT clear existing array
}

# pickAll
proc pickAll {value} {
global pickingList pickCheck

if [info exist pickingList] {
	foreach i $pickingList {set pickCheck($i) $value}
}
}

# classSetAll
proc classSetAll {value} {
global classLabel classCheck

for {set i 0} {$i < [llength $classLabel]} {incr i} {
	set classCheck($i) $value
}
}

# picked : send picked list to plotFrame, send all if none selected
proc picked {} {
global pickingList pickCheck

set pickedList {}
foreach {i} $pickingList {
	if {$pickCheck($i) == 1} {
		lappend pickedList $i
	}
}
if {[llength $pickedList] == 0} {set pickedList $pickingList}

plotFrame $pickedList
}

# plotSave
proc plotSave {} {
global xmlFileName
global progname
global xyplot
global wGraph
global wGraphCanvas
set types {{{Postscript image files} {.ps} }}

set resultfile [file rootname [file tail $xmlFileName]]
if {[string length $resultfile] == 0} {set resultfile $progname}
append resultfile "_[clock seconds].ps"
set resultfile [tk_getSaveFile -parent $wGraph -filetypes $types -initialdir [file dirname $xmlFileName] -initialfile $resultfile -defaultextension ".ps"]
if {[string length $resultfile] > 0} {
	set outfile [open $resultfile w]
#ghostscript doesn't support 2-byte font names, so force encoding iso8859-1 to avoid bugs
#	chan configure $outfile -encoding iso8859-1
#-fontmap option won't help us.  Output font seems not controllable.
#puts -nonewline $outfile [$wGraphCanvas postscript -fontmap fontmap]
	set psout [$wGraphCanvas postscript]
	set l [lrange [split $psout \n] 0 20]
	foreach x $l {
		if {[regexp {^%%[^\s]+\s+font\s([^\s-]+)} $x match psfont]} {
			regsub -all $psfont $psout {Courier} psout; # force Courier font
		}
	}
	puts -nonewline $outfile $psout
	close $outfile
}
}

# plotToggle
proc plotToggle {varname limit} {
upvar $varname x
incr x
if {$x >= $limit} {set x 0}
picked
}

# plotRescale
proc plotRescale {r} {
global plotScale plotScaleMax plotScaleMin
global wGraphCanvas

set newScale [expr ($plotScale*$r)]
if {$newScale < $plotScaleMin} {set r [expr ($plotScaleMin/$plotScale)]}
if {$newScale > $plotScaleMax} {set r [expr ($plotScaleMax/$plotScale)]}
set plotScale [expr ($plotScale*$r)]

update
set sizex [winfo width $wGraphCanvas]
set sizey [winfo height $wGraphCanvas]
set centerx [expr (0.5*$sizex)]
set centery [expr (0.5*$sizey)]

$wGraphCanvas scale all $centerx $centery $r $r
}

# textOverlap
proc textOverlap {p q fontPixel} {
lassign $p x y l
lassign $q v w m
set dy [expr abs($y-$w)]
if {$fontPixel < $dy} {return 0}
set dx [expr abs($x-$v)-($l+$m)*$fontPixel/3]
if {$dx > 0} {return 0}
return 1
}

# textOverlapList
proc textOverlapList {plotted new fontPixel} {
foreach p $plotted {
if {[textOverlap $p $new $fontPixel]} {return 1}
}
return 0
}

# textCollision : find "safe" text plot position by slightly moving up
proc textCollision {plotted new {font MyDefault}} {
set fontPixel [expr (1.3*abs([font configure $font -size]))]
lassign $new x y l
for {set i 0} {[textOverlapList $plotted [list $x [expr ($y-$i)] $l] $fontPixel]} {incr i} {}
return [list $x [expr ($y-$i)]]
}

# classMean
proc classMean {data} {
set classData [dict create]
set meanList {}
foreach {d} $data {
	lassign [trimList $d] class x y
	if {$class < 0} {
		lappend meanList [list $class $x $y]
		continue
	}
	if (![dict exists $classData $class]) {
		dict set classData $class [list 0 0 0]
	}
	lassign [dict get $classData $class] sumx sumy n

	set sumx [expr ($sumx + $x)]
	set sumy [expr ($sumy + $y)]
	incr n
	dict set classData $class [list $sumx $sumy $n]
}
dict for {k v} $classData {
	lassign $v sumx sumy n
	set x [expr $sumx / $n]
	set y [expr $sumy / $n]
	lappend meanList [list $n $x $y]
}
return $meanList
}

# idTagList
proc idTagList {canvas id} {
return [$canvas gettags $id]
}

# tagContains
proc tagContains {taglist tag} {
if {[lsearch -glob $taglist $tag*] == -1} {; # prefix match with wildcard
	return 0
}
return 1
}

# getTagSeriesName
proc getTagSeriesName {taglist {kind "data"}} {
return [lsearch -glob -inline $taglist ${kind}_*]
# you can remove "data_" by [string replace $return_value 0 4]
}

# setCurveTag
proc setCurveTag {canvas} {
foreach id [$canvas find withtag data] {; # check object with data tag
	set taglist [idTagList $canvas $id]
	set series [string replace [getTagSeriesName $taglist] 0 4]
	if {[string length $series] != 0} {
		if {![tagContains $taglist curve]} {
			lappend taglist curve
			$canvas itemconfigure $id -tags $taglist
		}
	}
}
}

# setPlotTag
proc setPlotTag {canvas} {
foreach id [$canvas find withtag data] {; # check object with data tag
	if { [catch {$canvas itemconfigure $id -arrow none}] } {; # not line object, NG: plus cross 
		set taglist [idTagList $canvas $id]
		set series [string replace [getTagSeriesName $taglist] 0 4]
		if {[string length $series] != 0} {
			if {![tagContains $taglist plot]} {
				$canvas itemconfigure $id -tags [list {*}$taglist plot plot_$series]
			}
		}
	}
}
}

# deleteSymbol
proc deleteSymbol {canvas {series ""}} {
set target "plot_$series"
if {[string equal $series ""]} {set target "plot"}
foreach id [$canvas find withtag plot] {; # check object with plot tag
	if {[tagContains [idTagList $canvas $id] $target]} {
		catch {$canvas itemconfigure $id -fill "" -outline ""}
	}
}
}

# getPlotIndex
proc getPlotIndex {canvas id} {
set plottag [getTagSeriesName [idTagList $canvas $id] plot]
set plottaglist [$canvas find withtag $plottag]
return [lsearch -exact $plottaglist $id]
}

# plotPicked : plot specimen
proc plotPicked {spec} {
global colorList colorListMono symbolList symbolListMono
global plotData plotType
global pickingList
global xyplot
global xMaxEach yMaxEach yAtMax xAtMax xMinEach yMinEach
global plotName plotNameFh plotSymbol plotLine plotMean plotXPos plotYPos plotTPos plotOrder plotScale
global xaxis yaxis
global curveInfo
global textPlottedPos
global wGraphCanvas
global wGraphMean
global fitSymbol

if {[lsearch -exact [array names plotData] $spec] == -1} {return}

lassign $xaxis xz xm
lassign $yaxis yz ym
set sizex [winfo width $wGraphCanvas]
set sizey [winfo height $wGraphCanvas]
set xshift [expr ($sizex/40)]
set yshift [expr (-$sizey/30)]

# must use [$xyplot canvas] in coordsToPixel, otherwise text will be in wrong position!
if {[catch {set myCanvas [$xyplot canvas]}] != 0} { # for older plotchart
set myCanvas $wGraphCanvas
}

switch $plotSymbol {
# color
	0 {
		set color [lindex $colorList $plotOrder($spec)]
		set symbol [lindex $symbolList 0]
	}
# monochrome
	1 {
		set color [lindex $colorListMono $plotOrder($spec)]
		set symbol [lindex $symbolListMono $plotOrder($spec)]
	}
# all black dots
	2 {
		set color [lindex $colorList 0]
		set symbol [lindex $symbolList 0]
	}
}

set removeSymbol 0
switch $plotLine {
	0 {set type both; set removeSymbol 1}
	1 {set type symbol}
	2 {set type both}
}
if {[string equal $plotType "Fh"]} {set type line}
set datalist $plotData($spec)
if {($plotMean > 0) && (![string equal $plotType "Fh"])} {
set datalist [classMean $plotData($spec)]
}

set labeltext $spec
set plotSequence $spec

if {[string equal $spec $fitSymbol]} {
set type line
set color [lindex $colorList 0]
set labeltext ""
}

$xyplot dataconfig $plotSequence -type $type -color $color -symbol $symbol
set c 0
if {[string equal $plotType "CF"]} {
	set labeltext "$spec  [get1key CV $spec]"
}
foreach {d} $datalist {
	lassign [trimList $d] class x y
	switch $plotType {
		"Fh" {
			if {[string equal $x "00"]} {
				set plotSequence "${spec}:$c"; # separate plot for each curve
				$xyplot dataconfig $plotSequence -type $type -color $color -symbol $symbol
				lassign [trimList $d] dummy dummy dummy f h hr fn
				dict append curveInfo "$plotSequence" "$spec, \"$fn\", [term F]max=$f, hmax=$h, hr=$hr"
				incr c
				continue
			}
		}
	}
# ignore outside plot
	if {$x > $xm} {continue}
	if {$x < $xz} {continue}
	if {$y > $ym} {continue}
	if {$y < $yz} {continue}
	$xyplot plot $plotSequence $x $y
}

if {[string equal $plotType "Fh"]} {
switch $plotNameFh {
	0 {}
	1 {
		lassign [::Plotchart::coordsToPixel $myCanvas $xAtMax($spec) $yMaxEach($spec)] xPos yPos
		set len [string length $labeltext]
		set yPos [expr ($yPos + $yshift)]; # can't use incr because yPos is float
		lassign [textCollision $textPlottedPos [list $xPos $yPos $len]] xPos yPos
		$wGraphCanvas create text $xPos $yPos -text $labeltext -fill $color
		lappend textPlottedPos [list $xPos $yPos $len]
	}
	2 {
		set yPos [expr ($yMaxEach($spec)+($ym-$yz)*0.08)]
		$xyplot plaintext $xAtMax($spec) $yPos $labeltext; #plaintext can't change text color
	}
}
} else {
switch $plotName {
	0 {}
	1 {
		lassign [::Plotchart::coordsToPixel $myCanvas $xMaxEach($spec) $yAtMax($spec)] xPos yPos
		set len [string length $labeltext]
		set xPos [expr ($xPos + $xshift)]; # can't use incr because xPos is float
		lassign [textCollision $textPlottedPos [list $xPos $yPos $len]] xPos yPos
		$wGraphCanvas create text $xPos $yPos -text $labeltext -fill $color -anchor w
		lappend textPlottedPos [list $xPos $yPos $len]
	}
	2 {
		set xPos [expr ($xMaxEach($spec)+($xm-$xz)*(0.05+0.01*[string length $labeltext]))]
		$xyplot plaintext $xPos $yAtMax($spec) $labeltext; #plaintext can't change text color
	}
}
}
if {[string equal $plotType "Fh"]} {
	setCurveTag $wGraphCanvas
} else {
	setPlotTag $wGraphCanvas
	if {$removeSymbol} {deleteSymbol $wGraphCanvas $spec}
}
}

# clearGraphMessage : clear graphMessage
proc clearGraphMessage {} {
global graphMessage

set graphMessage {}
}

# setGraphMessage : set graphMessage
proc setGraphMessage {text} {
global graphMessage

set graphMessage $text
}

# debugClick
proc debugClick {x y} {
global wGraphCanvas
global userModeLevel

if {$userModeLevel < 2} {clearGraphMessage; return}
set id [$wGraphCanvas find closest $x $y]
set taglist [idTagList $wGraphCanvas $id]
setGraphMessage "$id $taglist"
}

# curveClick
proc curveClick {x y} {
global wGraphCanvas
global curveInfo

if {![winfo exists $wGraphCanvas]} {return}
set id [$wGraphCanvas find closest $x $y]
set taglist [idTagList $wGraphCanvas $id]
set series [string replace [getTagSeriesName $taglist] 0 4]
if {[dict exists $curveInfo $series]} {
	setGraphMessage "[dict get $curveInfo $series]"
} else {
	clearGraphMessage
}
}

# graphClick : return clicked point information on clickable graph
proc graphClick {x y} {
global wGraphCanvas
global plotData
global plotMean
global plotType

if {![winfo exists $wGraphCanvas]} {return}
set id [$wGraphCanvas find closest $x $y]
set taglist [idTagList $wGraphCanvas $id]
set series [string replace [getTagSeriesName $taglist] 0 4]
set index [getPlotIndex $wGraphCanvas $id]
set datalist $plotData($series)
if {($plotMean > 0) && (![string equal $plotType "Fh"])} {
	set datalist [classMean $datalist]
}
set data [lindex $datalist $index]
lassign [trimList $data] class x y
setGraphMessage "$series ($x, $y)"
}

# plotFrame : plot frame and picked specimens
proc plotFrame {plotList} {
global execError
global plotData plotType plotTitle plotSubTitle plotXAxis plotYAxis
global xyplot
global xMaxEach yMaxEach yAtMax xAtMax xMinEach yMinEach
global plotXPos plotYPos plotTPos plotScale plotName plotNameFh plotLine plotMean
global xaxis yaxis
global textPlottedPos
global hardnessMode
global wRun wGraph wPick wClass wInput wGraphAll
global classLabel classCheck
global wGraphCanvas
global wGraphRedraw
global wGraphLine
global wGraphMean
global wGraphLabel
global curveInfo
global userModeLevel
global graphTextDefault
global fitSymbol

set classCount 0
for {set i 0} {$i < [llength $classLabel]} {incr i} {
	if {$classCheck($i) > 0} {incr classCount}
}

array unset plotData
set curveInfo {}
lassign $graphTextDefault xaxisDefault yaxisDefault titleDefault subDefault
foreach l [split $execError "\n"] {
	set d [split $l ","]
	if {[string equal $plotType [lindex $d 0]] == 0} {continue}
	lassign [trimList $d] dummy s class x y
	if {[lsearch -exact $plotList $s] == -1} {continue}
	if {(![string equal $plotType "AE"]) && ($classCount > 0) && ($class > -1) && ($classCheck($class) == 0)} {continue}
	if {[lsearch -exact [array names plotData] $s] == -1} {
		set plotData($s) {}
		set xMaxEach($s) $x
		set yMaxEach($s) $y
		set xAtMax($s) $x
		set yAtMax($s) $y
		set xMinEach($s) $x
		set yMinEach($s) $y
	}
	lappend plotData($s) [lreplace $d 0 1]
	if {$xMaxEach($s) < $x} {
		set xMaxEach($s) $x
		set yAtMax($s) $y
	}
	if {$yMaxEach($s) < $y} {
		set yMaxEach($s) $y
		set xAtMax($s) $x
	}
	if {$xMinEach($s) > $x} {
		set xMinEach($s) $x
	}
	if {$yMinEach($s) > $y} {
		set yMinEach($s) $y
	}
}

if {[array size plotData] == 0} {
	show "[word {No data found}]" "[word Info]"
	return
}
foreach s $plotList {
	if {[lsearch -exact [array names plotData] $s] >= 0} {
		if {[string equal $plotType "Fh"] == 0} {
			set plotData($s) [lsort $plotData($s)]
		}
	} else {
		set idx [lsearch -exact $plotList $s]
		set plotList [lreplace $plotList $idx $idx]
	}
}

set xMax 0
set yMax 0
set xMin 0
set yMin 0

foreach s $plotList {
	if {$s == $fitSymbol && $plotList != $fitSymbol} {continue}
	if {$xMax < $xMaxEach($s)} {set xMax $xMaxEach($s)}
	if {$yMax < $yMaxEach($s)} {set yMax $yMaxEach($s)}
	if {$xMin > $xMinEach($s)} {set xMin $xMinEach($s)}
	if {$yMin > $yMinEach($s)} {set yMin $yMinEach($s)}
}

if {![winfo exists $wGraph]} {
	toplevel $wGraph -width 600 -height 400 -bg white -bd 0 -padx 0 -pady 0
	wm protocol $wGraph WM_DELETE_WINDOW "destroy $wGraphAll"
	set wGraphFooter [frame $wGraph.footer]
#	set wGraphTitle [ttk::button $wGraphFooter.title -width 5 -text "[word Title]" -command "plotToggle plotTPos 3"]
	set wGraphTitle [ttk::button $wGraphFooter.title -width 5 -text "[word Title]" -command "myInput plotTitle {[word Title]} {$titleDefault} picked"]
	set wGraphSubTitle [ttk::button $wGraphFooter.subtitle -width 8 -text "[word Subtitle]" -command "plotToggle plotTPos 2"]
	set wGraphX [ttk::button $wGraphFooter.x -width 6 -text "[word X-Text]" -command "plotToggle plotXPos 2"]
	set wGraphY [ttk::button $wGraphFooter.y -width 6 -text "[word Y-Text]" -command "plotToggle plotYPos 2"]
	set wGraphLabel [ttk::button $wGraphFooter.label -width 6 -text "[word Label]" -command "plotToggle plotName 3"]
	set wGraphSymbol [ttk::button $wGraphFooter.symbol -width 5 -text "[word Color]" -command "plotToggle plotSymbol 3"]
	set wGraphLine [ttk::button $wGraphFooter.line -width 4 -text "[word Line]" -command "plotToggle plotLine 3"]
	set wGraphMean [checkbutton $wGraphFooter.mean -text "[word Mean]" -variable plotMean -command picked]
	set wGraphRedraw [ttk::button $wGraphFooter.redraw -width 6 -text "[word Redraw]" -command "picked"]
	set wGraphPlus [ttk::button $wGraphFooter.plus -width 1 -text "+" -command "plotRescale 1.03"]
	set wGraphMinus [ttk::button $wGraphFooter.minus -width 1 -text "-" -command "plotRescale 0.971"]
	set wGraphHelp [ttk::button $wGraphFooter.help -width 1 -text "?" -command {openHelp [help graph]}]
	set wGraphSave [ttk::button $wGraphFooter.save -width 4 -text "[word Save]" -command "plotSave"]
	set wGraphClose [ttk::button $wGraphFooter.close -width 6 -text "[word Close]" -command "destroy $wGraph $wPick $wClass"]
	myTooltip $wGraphTitle "[word Title] / [word Subtitle]"
	myTooltip $wGraphSave "Postscript"
	set wGraphMessage [label $wGraph.message -bd 0 -pady 0 -bg white -textvariable graphMessage]
	pack $wGraphClose $wGraphSave $wGraphHelp $wGraphMinus $wGraphPlus $wGraphRedraw \
	$wGraphSymbol $wGraphLabel $wGraphY $wGraphX $wGraphSubTitle $wGraphTitle -side right -anchor e -padx 0
	pack $wGraphFooter -side bottom -anchor e
	pack $wGraphMessage -expand 0 -fill none -ipady 0 -pady 0 -side bottom
	set wGraphCanvas [canvas $wGraph.c -background white -bd 0 -highlightthickness 0 -width 600 -height 400]
	pack $wGraphCanvas -expand 1 -fill both -ipady 0 -pady 0 -side bottom
	$wGraphCanvas bind all <Button> {debugClick %x %y}
	$wGraphCanvas bind plot <Button> {graphClick %x %y; break}
	$wGraphCanvas bind curve <Button> {curveClick %x %y; break}
	windowPosition $wGraph . right
} else {
	$wGraphCanvas delete all
	myFocus $wGraph
}

clearGraphMessage
myWindowSize
windowPosition $wPick $wGraph left
windowPosition $wClass $wPick left
myBusy $wRun $wGraph $wPick $wClass

wm title $wGraph [string trim [cleanText $plotTitle]]

set xaxis [smartRange $xMin $xMax]
set yaxis [smartRange $yMin $yMax]
set xyplot [::Plotchart::createXYPlot $wGraphCanvas $xaxis $yaxis]

if {[string equal $plotType "Fh"]} {
	pack forget $wGraphLine
	pack forget $wGraphMean
	$wGraphLabel configure -command "plotToggle plotNameFh 3"
} else {
	pack $wGraphLine -side right -anchor e -after $wGraphRedraw
	pack $wGraphMean -side right -anchor e -after $wGraphRedraw
	$wGraphLabel configure -command "plotToggle plotName 3"
}

update
#set sizex [$wGraphCanvas cget -width]
#set sizey [$wGraphCanvas cget -height]
set sizex [winfo width $wGraphCanvas]
set sizey [winfo height $wGraphCanvas]
set centerx [expr (0.5*$sizex)]
set centery [expr (0.5*$sizey)]

set textPlottedPos {}
foreach {i} $plotList {
	plotPicked $i
	update
}

set subTitleX 0.97
set subTitleAlign r
if {[string equal $plotType "HQ"]} {
	set subTitleX 0.57
	set subTitleAlign c
}
if {[string equal $plotType "CF"]} {
	lassign [get1key CV MeanAll] ca
	set plotSubTitle "%iC%bF %N= $ca ([unit C])"
	set subTitleX 0.57
	set subTitleAlign c
}
if {[string equal $plotType "HC"]} {
	lassign [get1key Hf] a b c
	if {[string length $c] > 0} {
		set plotSubTitle "%iC%bH%P-1%N = $a %i[splitParam ${hardnessMode} 1]%P-$b%N [format "%+.3f" ${c}]"
	}
}

canvasText $wGraphCanvas 0.57 0.0 $plotTitle c
switch $plotTPos {
0 {
canvasText $wGraphCanvas $subTitleX 0.1 $plotSubTitle $subTitleAlign
}
1 {}
}
switch $plotXPos {
0 {
canvasText $wGraphCanvas 0.99 1.02 $plotXAxis r
}
1 {}
}
switch $plotYPos {
0 {
canvasText $wGraphCanvas 0.06 0.0 $plotYAxis c
}
1 {}
}
$wGraphCanvas scale all $centerx $centery $plotScale $plotScale
myNoBusy
}

# get1key : read one line keyword data from stderr
proc get1key {keyword {specimen ""}} {
global execError

foreach l [split $execError "\n"] {
	set d [split $l ","]
	if {[string equal $keyword [lindex $d 0]]} {
		set t [trimList $d]
		if {[string length $specimen] > 0} {
			if {[string equal $specimen [lindex $t 1]]} {
				return [lreplace $t 0 1]
			}
		} else {
			return [lreplace $t 0 0]
		}
	}
}
}

# $execError = captured StdErr of executed program
# $plotData($specimen) = keyword-hit (ex. CF) plot data
# $pickingList = all specimens
# $pickedList = picked specimens to plot chart (local)

# drawChart : search keyword from execError for plot data and draw chart 
proc drawChart {keyword xaxisname yaxisname title {subtitle ""}} {
global execError
global canPlot colorList
global pickingList
global plotType plotTitle plotSubTitle plotXAxis plotYAxis
global plotOrder pickCheck classCheck
global wRun wGraph wPick wClass wInput wGraphAll
global classLabel
global graphTextDefault
global fitSymbol

if {[string length $canPlot] == 0} {return}; # return if no Plotchart found

set plotType $keyword
set plotTitle $title
set plotSubTitle $subtitle
set plotXAxis $xaxisname
set plotYAxis $yaxisname
set graphTextDefault [list $xaxisname $yaxisname $title $subtitle]

# prepare pickingList
set n 0; # plotOrder
set pickingList {}
array unset plotOrder

# read specimen name in order
foreach l [split $execError "\n"] {
	set d [split $l ","]
	if {![string equal $keyword [lindex $d 0]]} {continue}
	lassign [trimList $d] dummy s class x y
# prepare a new specimen
	if {[lsearch -exact $pickingList $s] == -1} {
		lappend pickingList $s
		set plotOrder($s) $n
		incr n
	}
}

if {[llength $pickingList] == 0} {return}; # return if no specimens found

# prepare pick window
if {![winfo exists $wPick]} {
	toplevel $wPick
	windowPosition $wPick . right
	wm title $wPick "[word Specimen]"
	wm protocol $wPick WM_DELETE_WINDOW "destroy $wGraphAll"
} else {
	destroy {*}[grid slaves $wPick]
	myFocus $wPick
}
set n 0
foreach {i} $pickingList {
	set color [lindex $colorList $plotOrder($i)]
	if {[string eq $i $fitSymbol]} {set color [lindex $colorList 0]}
	checkbutton $wPick.b${n} -variable pickCheck($i) -text " $i" -foreground $color -activeforeground $color;# -anchor center
	grid x $wPick.b${n} - - -sticky w
	incr n
}
set wPickClear [ttk::button $wPick.reset -width 6 -text "[word Clear]" -command "pickAll 0"]
set wPickAll [ttk::button $wPick.all -width 5 -text "[word All]" -command "pickAll 1"]
set wPickDraw [ttk::button $wPick.draw -width 5 -text "[word Draw]" -command "picked"]
set wPickClose [ttk::button $wPick.close -width 6 -text "[word Close]" -command "destroy $wGraphAll"]
grid $wPickClear $wPickAll $wPickDraw $wPickClose

# prepare class window
if {[string equal $plotType "AE"]} {
	destroy $wClass
} else {
	if {![winfo exists $wClass]} {
		toplevel $wClass
		windowPosition $wClass $wPick left
		wm title $wClass "[word [term Force]] ([unit F])"
		wm protocol $wClass WM_DELETE_WINDOW "destroy $wGraphAll"
	} else {
		destroy {*}[grid slaves $wClass]
		myFocus $wClass
	}
	set classLabel [get1key CL]
	for {set n 0} {$n < [llength $classLabel]} {incr n} {
		set l [lindex $classLabel $n]
		checkbutton $wClass.b${n} -variable classCheck($n) -text " $l"
		grid x $wClass.b${n} - - -sticky w
	}
	set wClassClear [ttk::button $wClass.reset -width 6 -text "[word Clear]" -command "classSetAll 0"]
	set wClassAll [ttk::button $wClass.all -width 5 -text "[word All]" -command "classSetAll 1"]
	set wClassDraw [ttk::button $wClass.draw -width 5 -text "[word Draw]" -command "picked"]
	set wClassClose [ttk::button $wClass.close -width 6 -text "[word Close]" -command "destroy $wGraphAll"]
	grid $wClassClear $wClassAll $wClassDraw $wClassClose
}
myWindowSize
picked
}

# runDisp : 
proc runDisp {content type} {
#input: execOutput & execError
global tcl_platform
global canPlot
global execOutput
global execError
global cfMode
global youngMode
global hardnessMode
global wRun wGraph wPick wClass wInput wGraphAll
global wRunText
global wRunFooter
global wRunSave
global wRunCopy
global wRunClose
global wRunFh
global wRunCf
global wRunEit
global wRunHit
global wRunArea
global wRunEta
global wRunAlter 
global wRunPoisson
global wRunAlterFit
global wRunHeq
global wRunHI
global wRunChfunc

if {![winfo exists $wRun]} {
	toplevel $wRun
	wm title $wRun "[word Run]"
	wm protocol $wRun WM_DELETE_WINDOW "destroy $wRun $wGraphAll"
	set wMain [frame $wRun.main]
	set wRunText [text $wMain.text -width 80]
	set wRunBar [scrollbar $wMain.bar -command "$wRunText yview"]
	$wRunText configure -yscrollcommand "$wRunBar set"
	pack $wRunBar -fill both -expand 0 -side right
	pack $wRunText -fill both -expand 1 -side right
	set wRunFooter [frame $wRun.footer]
	set wRunFh [ttk::button $wRunFooter.fh -text "[term F]-h" -width 5 -style MyItalic.TButton -command "drawChart Fh {[term depth], %ih%N ([unit h])} {[term force], %i[term F]%N ([unit F])} {[term force] - [term depth] curve}"]
	set wRunCf [ttk::button $wRunFooter.cf -text "C[sub F]" -width 5 -style MyItalic.TButton -command "drawChart CF {[term force], %i[term F]%N ([unit F])} {%if%N (nm%P2%N/mN)} {Frame compliance calibration}"]
	set wRunEit [ttk::button $wRunFooter.eit -text "E[sub IT]" -width 5 -style MyItalic.TButton -command "drawChart ET {[splitParam $youngMode] ([unit [splitParam $youngMode]])} {EIT ([unit E])}  {Young's Modulus}"]
	set wRunHit [ttk::button $wRunFooter.hit -text "H[sub IT]" -width 6 -style MyItalic.TButton -command "drawChart HT {[splitParam $youngMode] ([unit [splitParam $youngMode]])} {HIT ([unit H])} {Indentation Hardness}"]
	set wRunArea [ttk::button $wRunFooter.area -text "A(h)" -width 6 -style MyItalic.TButton]
	set wRunEta [ttk::button $wRunFooter.eta -text "[greek h][sub IT]" -width 6 -style MyItalic.TButton -command "drawChart EL {[splitParam $youngMode] ([unit [splitParam $youngMode]])} \"[greek h]IT (%%)\" {Elastic work ratio}"]
	set wRunAlter [ttk::button $wRunFooter.alternative -text "E'" -width 5 -style MyItalic.TButton -command "drawChart EA {hr ([unit h])} {%iE%N ([unit E])} {Young's Modulus (alternative)}"]
	set wRunPoisson [ttk::button $wRunFooter.poisson -width 5 -style MyItalic.TButton -text "[greek n]" -command "drawChart PO {hr ([unit h])} %i[greek n] {Poisson's Ratio}"]
	set wRunAlterFit [ttk::button $wRunFooter.alterfit -text "E' fit" -style MyItalic.TButton -width 7 -command "drawChart AE {%iX} {log%be%iE} {Young's Modulus (alternative) fitting}"]
	set wRunHeq [ttk::button $wRunFooter.heq -text "Heq" -width 5 -style MyItalic.TButton]
	set wRunHI [ttk::button $wRunFooter.hraw -text "HI" -width 7 -style MyItalic.TButton -command "drawChart HI {[splitParam $hardnessMode 1] ([unit [splitParam $hardnessMode 1]])} {%i[hardnessName $hardnessMode]%N (arb. unit)} {Apparent Hardness}"]
	set wRunChfunc [ttk::button $wRunFooter.chfunc -text "C[sub H]" -width 5 -style MyItalic.TButton -command "drawChart HC {[splitParam $hardnessMode 1] ([unit [splitParam $hardnessMode 1]])} {%iC%bH%P-1%N (arb. unit)} {%iC%bH%N fitting function}"]
	set wRunSave [ttk::button $wRunFooter.save -text "[word Save]" -width 6 -command "runSaveas $type"]
	set wRunCopy [ttk::button $wRunFooter.copy -text "[word Copy]" -width 6 -command "toClip $wRunText"]
	myTooltip $wRunCopy "[word {to Clipboard}]"
	set wRunClose [ttk::button $wRunFooter.close -text "[word Close]" -width 6 -command "destroy $wRun $wGraphAll"]
	pack $wRunFooter -fill both -expand 0 -side bottom
	pack $wMain -fill both -expand 1 -side bottom
	update
	windowPosition $wRun . bottom
} else {
#necessary because command changes
	$wRunSave configure -command "runSaveas $type"
	pack forget {*}[pack slaves $wRunFooter]
	myFocus $wRun
}
pack $wRunClose $wRunCopy $wRunSave -fill both -expand 0 -side right

if {$canPlot} {
	destroy $wGraphAll
	set keywordList {}
	set lines [split $execError "\n"]
	foreach l $lines {
		set x [split $l ","]
		set k [string trim [lindex $x 0]]
		if {[lsearch -exact $keywordList $k] == -1} {
			lappend keywordList $k
		}
	}
	if {[lsearch -exact $keywordList "HC"] != -1} {
		myTooltip $wRunChfunc "[word {Equivalent Hardness}] [word Fitting]"
		pack $wRunChfunc -side right
	}
	if {[lsearch -exact $keywordList "HI"] != -1} {
		myTooltip $wRunHI "[word {Apparent Hardness}]"
		pack $wRunHI -side right
	}
	if {[lsearch -exact $keywordList "HQ"] != -1} {
		lassign [get1key HR] a b
		if {$b > 0} {
			$wRunHeq configure -command "drawChart HQ {%i[splitParam $hardnessMode 1] ([unit [splitParam $hardnessMode 1]])} {Heq} {Equivalent Hardness} {($a = $b)}"
		}
		myTooltip $wRunHeq "[word {Equivalent Hardness}]"
		pack $wRunHeq -side right
	}
	if {[lsearch -exact $keywordList "AE"] != -1} {
		myTooltip $wRunAlterFit "[word {Young's modulus}] ([word Alternate]) [word Fitting]"
		pack $wRunAlterFit -side right
	}
	if {[lsearch -exact $keywordList "PO"] != -1} {
		myTooltip $wRunPoisson "[word {Poisson's ratio}]"
		pack $wRunPoisson -side right
	}
	if {[lsearch -exact $keywordList "EA"] != -1} {
		myTooltip $wRunAlter "[word {Young's modulus}] ([word Alternate])"
		pack $wRunAlter -side right
	}
	if {[lsearch -exact $keywordList "EL"] != -1} {
		myTooltip $wRunEta "[word {Elastic work ratio}]"
		pack $wRunEta -side right
	}
	if {[lsearch -exact $keywordList "Ah"] != -1} {
		lassign [get1key AP] a b c
		if {[string length $c] > 0} {
			$wRunArea configure -command "drawChart Ah {log%be%N(1000 [splitParam $youngMode])} {log%be%N(10 %iA%P 1/2%N)} {Area function} {%iY%N = ${a} %iX%P ${b} %N[format "%+.3f" ${c}]}"
		}
		myTooltip $wRunArea "[word {Area function}]"
		pack $wRunArea -side right
	}
	if {[lsearch -exact $keywordList "HT"] != -1} {
		myTooltip $wRunHit "[word {Indentation Hardness}]"
		pack $wRunHit -side right
	}
	if {[lsearch -exact $keywordList "ET"] != -1} {
		myTooltip $wRunEit "[word {Young's modulus}]"
		pack $wRunEit -side right
	}
	if {[lsearch -exact $keywordList "CF"] != -1} {
		myTooltip $wRunCf "[word {Frame compliance}]"
		pack $wRunCf -side right
	}
	if {[lsearch -exact $keywordList "Fh"] != -1} {
		myTooltip $wRunFh "[word Curve]"
		pack $wRunFh -side right
	}
}
$wRunText delete 1.0 end
$wRunText insert end $content
myWindowSize
}

# see XML : show XML file content
proc seeXML {} {
global xmlFileName
global execOutput
global execError
set execError {}
if {[file readable $xmlFileName]} {
	set fd [open $xmlFileName]
	set execOutput [read $fd]
} else {
	set execOutput "File \"$xmlFileName\" not found."
}
runDisp $execOutput .xml
}

# seeCurrent : show current setting
proc seeCurrent {} {
global execError
set execError {}
runDisp [makeXML] .xml
}

# runTemporary
proc runTemporary {} {
global execOutput
execTangent "-s -D2"
runDisp $execOutput .txt
}

# runError
proc runError {} {
global execError
execTangent "-s -D2"
runDisp $execError .txt
}

# runSummary
proc runSummary {} {
global execOutput
execTangent "-so -D2"
runDisp $execOutput .txt
}

# forceDepth
proc forceDepth {} {
global execError
global wRunFooter
global wRunFh
execTangent "-curve -D2"
runDisp $execError .txt
if {[lsearch -exact [pack slaves $wRunFooter] $wRunFh]} {$wRunFh invoke}
}

# forceAnalyze
proc forceAnalyze {{selList ""}} {
global execOutput
global wRunFooter
global wRunFh
execTangent "-curve -D2" $selList
runDisp $execOutput .txt
if {[lsearch -exact [pack slaves $wRunFooter] $wRunFh]} {$wRunFh invoke}
}

# runDebug
proc runDebug {} {
global execOutput
execTangent "-s -d -D2"
runDisp $execOutput .txt
}

# runFile
proc runFile {} {
global xmlFileName
global execOutput
global execError
if {[file readable $xmlFileName]} {
	execTangentSavedXML "-s"
} else {
	set execOutput "File \"$xmlFileName\" not found."
	set execError {}
}
runDisp $execOutput .csv
}

# tempXML
proc tempXML {{flag ""}} {
global xmlFileName
set xmltemp [myTempFile]; # set temporary file name in current dir
writeXML $xmltemp $flag
return $xmltemp
}

# execTangent : exec tangent with temporal XML setting file (i.e., current conditions) 
proc execTangent {option {flag ""}} {
# option: sent to tangent binary
# flag: selList (= file list filter) passed to writeXML
global xmlFileName
global skipBad

if {$skipBad > 0} {set option "$option -b"}
set wd [pwd]
cd [file dirname $xmlFileName]
set xmltemp [tempXML $flag]
if {[string length $xmltemp] > 0} {
	callTangent $xmltemp $option
	file delete $xmltemp
}
cd $wd
}

# execTangentSavedXML : exec tangent with saved XML file
proc execTangentSavedXML {option} {
# option: sent to tangent binary
global xmlFileName
set wd [pwd]
cd [file dirname $xmlFileName]
callTangent [file tail $xmlFileName] $option
cd $wd
}

# callTangent : backend routine to exec tangent binary (output: execOutput & execError)
proc callTangent {xml option {flag ""}} {
global xmlFileName
global execfile
global optString
global execOutput
global execError
global canPlot
global termForce
global termDepth
global interpolateAh interpolateCh
global wRun wGraphAll
destroy $wRun $wGraphAll
set execError {}
set execOutput "[word Calculating]..."
runDisp $execOutput {}
update
if {[string length $canPlot] > 0} {append option " -chart"}
if {[string eq $termForce 1]} {append option " -load"}
if {[string eq $termDepth 1]} {append option " -displacement"}
if {[string eq $interpolateAh 1]} {append option " -D0"}
if {[string eq $interpolateCh 1]} {append option " -D1"}

myBusy $wRun
set outtemp [open "| \"$execfile\" $option $optString $xml" r]
chan configure $outtemp -translation {auto lf} -encoding utf-8 -buffering none
set execOutput [read $outtemp]
catch {close $outtemp} execError
myNoBusy
}

# openHelp : show text message to help/info window
proc openHelp {t} {
global wHelp
global wHelpText
if {![winfo exists $wHelp]} {
	toplevel $wHelp -width 600 -height 600
	wm title $wHelp "[word Info]"
	set wMain [frame $wHelp.main]
	set wHelpText [text $wMain.text -width 80]
	set wScrollBar [scrollbar $wMain.bar -command "$wHelpText yview"]
	$wHelpText configure -yscrollcommand "$wScrollBar set"
	pack $wScrollBar -fill both -expand 0 -side right
	pack $wHelpText -fill both -expand 1 -side right
	set wFooter [frame $wHelp.footer]
	set wOk [ttk::button $wFooter.ok -text "OK" -width 6 -command "destroy $wHelp"]
	set wCopy [ttk::button $wFooter.copy -text "[word Copy]" -width 6 -command "toClip $wHelpText"]
	myTooltip $wCopy "[word {to Clipboard}]"
	pack $wOk $wCopy -side right
	pack $wFooter -fill both -expand 0 -side bottom
	pack $wMain -fill both -expand 1 -side bottom
	update
	windowPosition $wHelp . cover
} else {
	myFocus $wHelp
}
$wHelpText delete 1.0 end
$wHelpText insert end $t
}

# updateCheck : check site for update
proc updateCheck {} {
global latestURL
global version
if {[catch {set token [::http::geturl $latestURL]}] != 0} {
show "[word {Update check failed}]" "[word Error]"
return
}
set content [::http::data $token]
http::cleanup $token
lassign [trimList [split $content ,]] latest
set compare "$version:  [word Current]\n$latest:  [word Latest]"
if {$version < $latest} {
	show "[word {New update avilable}]\n\n${compare}" "[word Update]"
} else {
	show "[word {No update necessary}]\n\n${compare}" "[word Update]"
}
}

# license : show license text
proc license {} {
global execfile
openHelp [exec $execfile -L]
}

# openReadMe : show readme text
proc openReadMe {} {
global execdir
global readme
set docfile [file join $execdir "doc" "[word {$readme}].txt"]
if {![file readable $docfile]} {
	set docfile [file join $execdir "doc" "${readme}.txt"]
}
if {![file readable $docfile]} {
	show "[word {No document}]" "[word Error]"
return
}
set fd [open $docfile]
openHelp [read $fd]
}

# about : show program information
proc about {} {
global progname
global author
global version
global years
global notice
show "$progname $version $author\n\n$years\n$notice" "[word About]"
}

#
# main program
#

# directry and exefile
set firstdir [file normalize .]
set execdir [file normalize [file dirname $argv0]]
set execfile [file join $execdir tangent.exe]
if {![file executable $execfile]} {set execfile [file join $execdir tangent]}
if {![file executable $execfile]} {show "[word {No main program}]" "[word Error]"; exit}
lassign [trimList [split [exec $execfile] ,\n]] progname version author years notice

# toplevel windows
# .r run result
# .g chart
# .p pick list of specimens for chart
# .c pick list of classes for chart
# .l help
# .i input
# .t data file list
# .t$x file select window for each specimen
# .z test
# .m menu (fixed)
set wRun .r
set wGraph .g
set wPick .p
set wClass .c
set wHelp .l
set wInput .i
set wData .t
set wList .list; # sub widget word of wData
set wTest .z
set wGraphAll "$wGraph $wPick $wClass $wInput"

# global variables
set xmlFileName {}
set homePage https://3zip.net/t/en.html
set latestURL http://3zip.net/t/version.txt
set prefFile tangent.ini
set readme readme
set subFontRatio 0.7
set specList {}
set maxSpec 20
set specColumnList {file}
set initialSpecNum 3
set fitSymbol "--fit"
set cfModeList {equal large max2}
#set cfModeList {equal large linear square cube percent}
#linear : same as large, priority function = Fmax
#square : same as large, priority function = Fmax[super 2]
#cube   : same as large, priority function = Fmax[super 3]
set youngModeList {hr hc}
set hardnessModeList {"hGeo" "hGeo_hr" "hGeo_hmax" "hGeo_Fmax" "hr" "hc" "hmax"}
set colorList [lrepeat 1 black red blue2 purple navy green deeppink saddlebrown darkslategray indianred steelblue springgreen cyan orange hotpink gray skyblue yellow darkkhaki navajowhite]
set colorListMono [concat [lrepeat 6 black] [lrepeat 6 gray32] [lrepeat 6 gray56] [lrepeat 6 gray80]]
set symbolList [concat [lrepeat 6 dot] [lrepeat 6 upfilled] [lrepeat 6 downfilled] [lrepeat 6 circle]]
set symbolListMono [lrepeat 4 dot circle upfilled up downfilled down]
# NG: plus cross, because these symbols cannot be distinguished from connecting lines

# ini file variables other than font
set optString {}
set language 0
set userModeLevel 0
set termForce 0
set termDepth 0
readPref

# material data
set matdata [dict create]
readMatData

# dictionary
dictInit

# font
set fontSizeChange 0; # starting font size change must be 0
fontListInit
set fontSizeInput $fontSizeChange; # wanted font size change from ini file
set fontSizeChange 0; # reset start font size as 0
fontSizeSet $fontSizeInput
fontInit

# screen
canvasTextInit
mainInit
renew
