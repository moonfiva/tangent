package main

import (
	"sort"
)

// Type Point : Point in N-dimension, float64 slice
type Point []float64

// Type Series : slice of Point
type Series []Point


// Sort: sort series, sortPos is sorting axis of Point
func (s *Series)Sort(sortPos int) {
	sort.Slice(*s, func(i, j int) bool { return (*s)[i][sortPos] < (*s)[j][sortPos] })
}

// ClassAverage : make averaged series, classPos is averaging axis of Point
func (s *Series)ClassAverage(classPos int) *Series {
	if len(*s) == 0 {return s}

// prepare class for averaging later
	c := NewClassification()

	maxRow := 0 // dimension of Point
	for _, p := range *s {
		c.Add(p[classPos])
		if maxRow < len(p) {maxRow = len(p)}
	}
// prepare average storage
	var classStat [][]Stat = make([][]Stat, c.Count())
	for i := 0; i < c.Count(); i++ {
		classStat[i] = make([]Stat, maxRow)
	}
// calc average
	for _, p := range *s {
		i := c.Search(p[classPos])
		for j, q := range p {
			classStat[i][j].Add(q)
		}
	}
// prepare return value
	var r Series = make([]Point, c.Count())
	for i := 0; i < c.Count(); i++ {
		var p Point = make([]float64, maxRow)
		for j, x := range classStat[i] {
			p[j] = x.Ave()
		}
		r[i] = p
	}
	return &r
}

// Min : must be sorted at sortPos
func (s *Series) Min(sortPos int) float64 {
	if len(*s) == 0 {return 0}
	return (*s)[0][sortPos]
}

// Max : must be sorted at sortPos
func (s *Series) Max(sortPos int) float64 {
	if len(*s) == 0 {return 0}
	return (*s)[len(*s)-1][sortPos]
}

// InRange : check range
func (s *Series) InRange(sortPos int, x float64) bool {
	if len(*s) == 0 {return true}
	if x < s.Min(sortPos) {return false}
	if x > s.Max(sortPos) {return false}
	return true
}

// InRangeWide : check range (wide)
func (s *Series) InRangeWide(sortPos int, x float64) bool {
	if len(*s) < 2 {return true}
	if x < 2 * (*s)[0][sortPos] - (*s)[1][sortPos] {return false}
	l := len(*s) - 1
	if x > 2 * (*s)[l][sortPos] - (*s)[l-1][sortPos] {return false}
	return true
}

// Search : find minimum index > x
func (s *Series) Search(sortPos int, x float64) int {
	return sort.Search(len(*s), func(j int) bool {return (*s)[j][sortPos] > x})
}

// Interpolate : s must be sorted before calling Interpolate()
// Check IsRange() if necessary
func (s *Series) Interpolate(sortPos, targetPos int, x float64) float64 {
	if len(*s) == 0 {return 0}
	if len(*s) == 1 {return (*s)[0][targetPos]}
// set fitted division
	i := s.Search(sortPos, x)
	i--
	if i < 0 { i = 0 }
	if i > len(*s) - 2 { i = len(*s) - 2 }
	x0 := (*s)[i][sortPos]
	xdiff := (*s)[i+1][sortPos] - x0
	if xdiff == 0.0 {return 0}
	y0 := (*s)[i][targetPos]
	ydiff := (*s)[i+1][targetPos] - y0
	y := ydiff / xdiff * (x - x0) + y0
	return y
}