package main

import (
	"math"
	"sort"
	"fmt"
)

// Stat : statistical calculation class
type Stat struct {
	Count int
	sum, sum2, base, ave, sdev, Max, Min float64
	updated bool
}

// NewStat : create a new instance
func NewStat() *Stat {
	t := &Stat {
	}
	return t
}

// Add : add a new data
func (t *Stat) Add(x float64) {
	if math.IsNaN(x) {return}
	if t.Count == 0 {
		t.base = x
		t.Max = x
		t.Min = x
	}
	if x > t.Max {t.Max = x}
	if x < t.Min {t.Min = x}
	x -= t.base	// use biased values
	t.sum += x
	t.sum2 += x * x
	t.Count++
	t.updated = false
}

// Sum : Sum
func (t *Stat) Sum() float64 {
	return t.sum + t.base * float64(t.Count)
}

// Sum2 : Sum of Squared
func (t *Stat) Sum2() float64 {
	return t.sum2 + t.base * (2 * t.Sum() - float64(t.Count) * t.base)
}

// Ave : average with cache
func (t *Stat) Ave() float64 {
	if t.updated == false {t.update()}
	return t.ave
}

// Sdev : standard deviation with cache
func (t *Stat) Sdev() float64 {
	if t.updated == false {t.update()}
	return t.sdev
}

// CV : coefficient of variation with cache
func (t *Stat) CV() float64 {
	if t.Ave() == 0.0 {return 0.0}
	return t.Sdev() / t.Ave()
}

// SdevOffset : Sdev() with offset
func (t *Stat) SdevOffset(c float64) float64 {
	s := math.Sqrt((t.sum2 - (c - t.base) * (2.0 * t.sum - (c - t.base) * float64(t.Count)) / (float64(t.Count) - 1.0)))
	return s
}

// String : string conversion
func (t *Stat) String() string {
	return fmt.Sprintf("N = %d, Ave = %s", t.Count, ftoa(t.Ave()))
}

// update : internal function to update values
func (t *Stat) update() {
	t.ave = t.sum / float64(t.Count) + t.base
	t.sdev = math.Sqrt((t.sum2 - t.sum * t.sum / float64(t.Count)) / (float64(t.Count - 1)))
	t.updated = true
}

// Type StatWeight : statistical calculation class with Weight
type StatWeight struct {
	Count int
	WeightCount float64
	sum, sum2, ave, sdev, Max, Min float64
	updated bool
}

// NewStatweight : create a new instance
func NewStatWeight() *StatWeight {
	t := &StatWeight {
	}
	return t
}

// Add : add a new data
func (t *StatWeight) Add(x float64, w float64) {
	if math.IsNaN(x) {return}
	if math.IsNaN(w) {return}
	if w <= 0.0 {return}
	if t.Count == 0 {
		t.Max = x
		t.Min = x
	}
	if x > t.Max {t.Max = x}
	if x < t.Min {t.Min = x}
	t.sum += x * w
	t.sum2 += x * x * w
	t.Count += 1
	t.WeightCount += w
	t.updated = false
}

// Ave : average with cache
func (t *StatWeight) Ave() float64 {
	if t.updated == false {t.update()}
	return t.ave
}

// Sdev : standard deviation with cache
func (t *StatWeight) Sdev() float64 {
	if t.updated == false {t.update()}
	return t.sdev
}

// CV : coefficient of variation with cache
func (t *StatWeight) CV() float64 {
	if t.Ave() == 0.0 {return 0.0}
	return t.Sdev() / t.Ave()
}

// update : internal function to update values
func (t *StatWeight) update() {
	t.ave = t.sum / t.WeightCount
	t.sdev = math.Sqrt((t.sum2 - t.sum * t.sum / t.WeightCount) / t.WeightCount * float64(t.Count) / float64(t.Count - 1.0))
	t.updated = true
}


// Type Stat2 : statistical calculation class for 2 parameters
type Stat2 struct {
	statx, staty *Stat
	Count int
	a, b float64
	updated bool
	sumxy float64
	corr float64
	basex, basey float64
}

// NewStat2 : create a new instance
func NewStat2() *Stat2 {
	t := &Stat2 {
		statx: NewStat(),
		staty: NewStat(),
	}
	return t
}

// Add : add a data point (x,y)
func (t *Stat2) Add(x, y float64) {
	if math.IsNaN(x) {return}
	if math.IsNaN(y) {return}
	if t.Count == 0 {
		t.basex = x
		t.basey = y
	}
	x -= t.basex
	y -= t.basey

	t.statx.Add(x)
	t.staty.Add(y)
	t.sumxy += x * y
	t.Count++
	t.updated = false
}

// update : internal function to calculate paramaters a,b of Y=aX+b
func (t *Stat2) update() {
	if !t.IsValid() {return}
	sxx := (float64(t.Count) * t.statx.Sum2() - t.statx.Sum() * t.statx.Sum())
	syy := (float64(t.Count) * t.staty.Sum2() - t.staty.Sum() * t.staty.Sum())
	sxy := (float64(t.Count) * t.sumxy - t.statx.Sum() * t.staty.Sum()) 
	t.a = sxy / sxx
	t.b = (t.staty.Sum() - t.a * t.statx.Sum()) / float64(t.Count) - t.a * t.basex + t.basey
	t.corr = sxy / math.Sqrt(sxx * syy)
	t.updated = true
}

// A : proportional constant
func (t *Stat2) A() float64 {
	if t.updated == false {t.update()}
	return t.a
}

// B : y offset
func (t *Stat2) B() float64 {
	if t.updated == false {t.update()}
	return t.b
}

// Offset : x offset
func (t *Stat2) Offset() float64 {
	if t.A() == 0.0 {return 0.0}
	return -t.B()/t.A()
}

// Corr : correlation coefficient
func (t *Stat2) Corr() float64 {
	c := ((t.sumxy - t.statx.Ave() * t.staty.Ave() * float64(t.Count)) / (float64(t.Count - 1))) / (t.statx.Sdev() * t.staty.Sdev())
	return c
}

// CorrSquared : squared correlation coefficient
func (t *Stat2) CorrSquared() float64 {
	c := t.Corr()
	return c * c
}

// Y : returns calculated value
func (t *Stat2) Y(x float64) float64 {
	return t.A() * x + t.B()
}

// IsValid : returns fitting is ok or not
func (t *Stat2) IsValid() bool {
	if t.Count < 2 {return false}
	return true
}

// Type StatMemory : median-supported stat class
type StatMemory struct {
	Sub *Stat
	memory Point
	median float64
	updated bool
}

// NewStatMemory
func NewStatMemory() *StatMemory {
	t := &StatMemory {
		Sub: NewStat(),
		memory: make(Point, 0),
	}
	return t
}

// Add
func (t *StatMemory) Add(x float64) {
	if math.IsNaN(x) {return}
	t.Sub.Add(x)
	t.memory = append(t.memory, x)
	t.updated = false
}

// Median : 
func (t *StatMemory) Median() float64 {
	if t.updated {return t.median}
	sort.Float64s(t.memory)
	n := t.Sub.Count
	if n%2 == 0 {
		t.median = (t.memory[n/2-1] + t.memory[n/2]) / 2
	} else {
		t.median = t.memory[(n-1)/2]
	}
	t.updated = true
	return t.median
}

// Type Result : to store result for summary
type Result struct {
	x *Stat
	y []*StatMemory
}

// NewResult
func NewResult() *Result {
	t := &Result {
		x: NewStat(),
		y: []*StatMemory {},
	}
	return t
}

// Type Summary : store result and output summary
type Summary struct{
	result map[string] Result
	order []string
}

// NewSummary
func NewSummary() *Summary {
	t := &Summary {
		result: map[string] Result{},
	}
	return t
}

// Add
func (t *Summary) Add(specimen string, x float64, y ...float64) {
	if math.IsNaN(x) {return}
	for _, v := range y {if math.IsNaN(v) {return}}
	_, isThere := t.result[specimen]
	if !isThere {
		t.result[specimen] = *NewResult()
		t.order = append(t.order, specimen)
	}
	t.result[specimen].x.Add(x)
	for i, v := range y {
		if i >= len(t.result[specimen].y) {
// Following code is necessary because the only way to change something in a map is assigning
			temp := t.result[specimen]
			temp.y = append(temp.y, NewStatMemory())
			t.result[specimen] = temp
		}
		t.result[specimen].y[i].Add(v)
	}
}

// String : string conversion of Summary
func (t *Summary) String() string {
	if Debug {show("Summary.String %s\n", t.order)}

	str := ""
	for _, s := range t.order {
		r, isThere := t.result[s]
		if isThere {
			str = str + fmt.Sprintf("%s, %d, %s, %s", s, r.x.Count, ftoa(r.x.Min), ftoa(r.x.Max))
			for _, v := range r.y {
				str = str + fmt.Sprintf(", %s, %s", ftoa(v.Median()), ftoa(v.Sub.Ave()))
			}
			str = str + "\n"
		}
	}
	return str
}
