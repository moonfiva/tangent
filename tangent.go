package main

import (
	"fmt"
	"os"
	"os/user"
	"math"
	"io/ioutil"
	"path/filepath"
	"encoding/xml"
	"strings"
	"strconv"
)

// Constants
const progname = "tangentGo"
const version = "v0.50"
const author = "by Kensuke Miyahara"
const years = "(c) 2019-2023"
const copyright = "National Institute for Materials Science"
const header = progname + ", " + version + ", " + author + "\n" + years + ", " + copyright + "\n"

const errorMsg = "*ERROR*,"
const warningMsg = "Warning,"
const notDefaultMsg = ", (NOT default)"
const summaryMsg = "# Summary"
const fitSymbol = "--fit"

var cfModeList = []string{"equal", "large", "max2"}
var youngModeList = []string{"hr", "hc"}
var hardnessModeList = []string{"hGeo", "hGeo_hr", "hGeo_hmax", "hGeo_Fmax", "hr", "hc", "hmax"}

// Global flags
const MaxSpecimen = 20
var Debug bool = false
var DebugFlags []bool = []bool {false, false, false}
const DebugInterpolateAh = 0
const DebugInterpolateCh = 1
const UseXMLProperName = 2
var ShowSummary bool = false
var ShowFolder bool = true
var ShowResult bool = true
var ShowChart bool = false
var ShowForceDepth bool = false
var NoAnalysis bool = false
var IncludeNegative bool = false
var SkipBadCurve bool = false
var forceWord string = "force"
var forceVar string = "F"
var depthWord string = "depth"

// Types
// Type Roots : root type of setting XML
type Roots struct {
	XMLName xml.Name `xml:"root"`
	Config Config `xml:"config"`
	Datasets []Dataset `xml:"dataset"`
}

// Type Config : configulation part of setting XML
type Config struct {
	XMLName xml.Name `xml:"config"`
	Version string `xml:"version"`
	ProperName string `xml:"propername"`
	Name string `xml:"name"`
	Folder string `xml:"folder"`
	Beta string `xml:"beta"`
	UnloadLower string `xml:"unload_lower"`
	UnloadUpper string `xml:"unload_upper"`
	DepthColumn string `xml:"depth_column"`
	ForceColumn string `xml:"force_column"`
	DepthFactor string `xml:"depth_factor"`
	ForceFactor string `xml:"force_factor"`
	SkipLine string `xml:"skip_line"`
	CF string `xml:"cf"`
	IndenterMaterial string `xml:"indenter_material"`
	YoungMode string `xml:"young_mode"`
	HardnessMode string `xml:"hardness_mode"`
	CfMode string `xml:"cf_mode"`
}

// Type Dataset : Dataset part of setting XML
type Dataset struct {
	XMLName xml.Name `xml:"dataset"`
	Specimen string `xml:"specimen"`
	CF string `xml:"cf,attr"`
	Poiref string `xml:"poiref,attr"`
	Reference string `xml:"reference,attr"`
	Hardref string `xml:"hardref,attr"`
	Young string `xml:"young"`
	Poisson string `xml:"poisson"`
	Hardness string `xml:"hardness"`
	Folder string `xml:"folder"`
	File []string `xml:"file"`
}

// Common functions
// minUInt32 : return minimum of two uint32 values
func minUInt32 (x, y uint32) uint32 {
	if x < y {return x}
	return y
}

// maxUInt32 : return maximum of two uint32 values
func maxUInt32 (x, y uint32) uint32 {
	if x > y {return x}
	return y
}

// searchString : search string from slice, return -1 if not found
func searchString(strSlice []string, x string) int {
	for i, v := range strSlice {
		if x == v {return i}
	}
	return -1
}

// show : output text to Stdout
func show(s string, a ...interface{}) {
	fmt.Printf(s, a...)
}

// showError : output text to Stderr
func showError(s string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, s, a...)
}

// fallback : return second text if first is blank, else first
func fallback(first, second string) string {
	if first == "" {return second}
	return first
}

// atoi : return integer from string
func atoi(str string) int {
	i, err := strconv.Atoi(str)
	if err != nil {
		show("%s atoi() string = %s\n", errorMsg, str)
		panic(err)
	}
	return i
}

// atof : return float64 from string, 0.0 if error
func atof(str string) float64 {
	f, err := strconv.ParseFloat(str, 64)
	if err != nil {return 0.0} // return 0.0 if error
	return f
}

// ftoa : return string from float64 in appropriate length
func ftoa(x float64) string {
	a := math.Abs(x)
	format := "%.3f"
	switch {
	case a == 0.0 :
	case a < 0.001 : format = "%.6f"
	case a < 0.01 : format = "%.5f"
	case a < 0.1 : format = "%.4f"
	case a >= 1000.0 : format = "%.0f"
	case a >= 100.0 : format = "%.1f"
	case a >= 10.0 : format = "%.2f"
	}
	return fmt.Sprintf(format, x)
}

// ftoaSlice : return string slice from float64 slice
func ftoaSlice(f []float64) []string {
	var s []string = make([]string, 0, len(f))
	for _, v := range f {
		s = append(s, ftoa(v))
	}
	return s
}

// replaceHome : replace "~" with home directory
func replaceHome(s string) string {
	if !strings.Contains(s, "~") {return s}
	usr, _ := user.Current()
	return strings.Replace(s, "~", usr.HomeDir, 1)
}

func main() {
// Constants
const papers = `  1) T. ISHIBASHI, Y. YOSHIKAWA, K. MIYAHARA, T. YAMAMOTO, S. KATAYAMA, M. YAMAMOTO: J. Mater. Test. Res. Vol.61, No.2, 60 (2016)
  2) T. ISHIBASHI, Y. YOSHIKAWA, S. KATAYAMA, K. MIYAHARA, M. OHKI, T. YAMAMOTO, M. YAMAMOTO: J. Mater. Test. Res. Vol.62, No.2, 68 (2017)
  3) T. ISHIBASHI, Y. YOSHIKAWA, M. YAMAMOTO, K. MIYAHARA, T. YAMAMOTO, M. OHKI, S. KATAYAMA: J. Mater. Test. Res. Vol.62, No.3, 164 (2017)
  4) T. ISHIBASHI, Y. YOSHIKAWA, K. MIYAHARA, T. YAMAMOTO, S. TAKAGI, M. FUJITSUKA, S. KATAYAMA: J. Mater. Test. Res. Vol.65, No.1, 4 (2020)`

const tangentGoCopyright = `MIT License

Copyright (c) 2019-2023 National Institute for Materials Science

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.`


const goCopyright = `Copyright (c) 2009 The Go Authors. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.
   * Neither the name of Google Inc. nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.`

const tcltkCopyright = `Tcl/Tk License Terms

This software is copyrighted by the Regents of the University of California, Sun Microsystems, Inc., Scriptics Corporation, and other parties. The following terms apply to all files associated with the software unless explicitly disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute, and license this software and its documentation for any purpose, provided that existing copyright notices are retained in all copies and that this notice is included verbatim in any distributions. No written agreement, license, or royalty fee is required for any of the authorized uses. Modifications to this software may be copyrighted by their authors and need not follow the licensing terms described here, provided that the new terms are clearly indicated on the first page of each file where they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

GOVERNMENT USE: If you are acquiring this software on behalf of the U.S. government, the Government shall have only "Restricted Rights" in the software and related documentation as defined in the Federal Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2). If you are acquiring the software on behalf of the Department of Defense, the software shall be classified as "Commercial Computer Software" and the Government shall have only "Restricted Rights" as defined in Clause 252.227-7013 (c) (1) of DFARs. Notwithstanding the foregoing, the authors grant the U.S. Government and others acting in its behalf permission to use and distribute the software in accordance with the terms specified in this license. `

cfModeOverride := -1
youngModeOverride := -1
hardnessModeOverride := -1

// get current directory
currentDir, _ := os.Getwd()

// check special options
for i := 1; i < len(os.Args); i++ {
	switch os.Args[i] {
	case "-print-version":
		show("%s\n", version)
		return
	}
}

// show header
show("%s", header)

// check normal options
for i := 1; i < len(os.Args); i++ {
	switch os.Args[i] {
	case "-h":
		fallthrough
	case "-?":
		show(" Analysis tool for instrumented indentation testing\n\n")
		show("Usage: tangent <config file(s) (*.xml)>\n")
		show(" Specify '--' to read from standard input.\n")
		fallthrough
	case "-?o":
		fallthrough
	case "-ho":
		show("\nOptions:\n")
		show("  -s, print short summary (-so for summary only)\n")
		show("  -n, don't show folder name (file name only)\n")
		show("  -d, turn debug messages on\n")
		show("  -b, skip bad curves\n")
		show("  -load, use 'load' instead of 'force'\n")
		show("  -displacement, use 'displacement' instead of 'depth'\n")
		show("  -chart, output chart data to stderr (internal use)\n")
		show("  -curve, output curve plot data to stderr (sparsely, no analysis, internal use)\n")
		show("  -Cn (n=0-%d), set CF method %s\n", len(cfModeList)-1, cfModeList)
		show("  -En (n=0-%d), set Young's modulus method %s\n", len(youngModeList)-1, youngModeList)
		show("  -Hn (n=0-%d), set Hardness method %s\n", len(hardnessModeList)-1, hardnessModeList)
		show("  -Dn (n=0-%d), set various Debug flag (internal use)\n", len(DebugFlags)-1)
		show("  -L, show copyright licenses\n")
		return
	case "-d":
		Debug = true
		continue
	case "-so":
		ShowResult = false
		fallthrough
	case "-s":
		ShowSummary = true
		continue
	case "-b":
		SkipBadCurve = true
		continue
	case "-n":
		ShowFolder = false
		continue
	case "-Negative": // Hidden option
		IncludeNegative = true
		continue
	case "-chart":
		ShowChart = true
		continue
	case "-curve":
		ShowForceDepth = true
		NoAnalysis = true
		continue
	case "-load":
		forceWord = "load"
		forceVar = "P"
		continue
	case "-displacement":
		depthWord = "displacement"
		continue
	case "-L":
		show("\nThis software is based on the following papers:\n%s\n", papers)
		show("\nThis software is released under the MIT license. (See below)\n")
		show("\nThis software uses following programs:\n")
		show("  Go (Golang) to compile the main program\n")
		show("  Tcl/Tk for graphical user interface (GUI)\n")
		show("\n******** tangentGo License ********\n%s\n\n", tangentGoCopyright)
		show("\n******** Go License ********\n%s\n", goCopyright)
		show("\n******** Tcl/Tk License ********\n%s\n", tcltkCopyright)
		return
	}
// check numbered options
	switch os.Args[i][:2] {
	case "-C":
		cfModeOverride = atoi(os.Args[i][2:])
		continue
	case "-E":
		youngModeOverride = atoi(os.Args[i][2:])
		continue
	case "-H":
		hardnessModeOverride = atoi(os.Args[i][2:])
		continue
	case "-D":
		i := atoi(os.Args[i][2:])
		if i >= 0 && i < len(DebugFlags) {
			DebugFlags[i] = true
		}
		continue
	}
// check wrong options
	if os.Args[i][0] == '-' {
		if os.Args[i] != "--" {
			show("%s Illegal option '%s'\n", warningMsg, os.Args[i])
			continue
		}
	}

// read setting XML
	xmlFile := os.Stdin
	var err error = nil
	if os.Args[i] != "--" {
		os.Chdir(currentDir)
		xmlFile, err = os.Open(os.Args[i])
		if err != nil {
			show("%s main() can't open %s.\n", errorMsg, os.Args[i])
			panic(err)
		}
	}
	defer xmlFile.Close()

	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		show("%s main() can't read %s.\n", errorMsg, os.Args[i])
		panic(err)
	}
	var roots Roots
	if !DebugFlags[UseXMLProperName] {
		show("XML, %s\n", filepath.Clean(os.Args[i]))
	}
	folder, _ := filepath.Split(os.Args[i])
	if len(folder) > 0 {
		err = os.Chdir(folder)
		if err !=nil {panic(err)}
	}

	err = xml.Unmarshal(byteValue, &roots)
	if err != nil {
		show("%s main() XML error %s.\n", errorMsg, os.Args[i])
		panic(err)
	}

// read config
// beta
	beta = atof(fallback(roots.Config.Beta, fmt.Sprintf(".6f", betaDefault)))
	if (beta <= 0.0) {
		beta = betaDefault
	}

// unload fitting
	unloadLower := atof(fallback(roots.Config.UnloadLower, fmt.Sprintf(".6f", lowerDefault)))
	unloadUpper := atof(fallback(roots.Config.UnloadUpper, fmt.Sprintf(".6f", upperDefault)))
	if (unloadLower > unloadUpper) {
		unloadLower, unloadUpper = unloadUpper, unloadLower
		show("%s UNLOAD lower & upper limit swapped.\n", warningMsg)
	}
	if unloadLower < 0.0 {
		unloadLower = 0.0
		show("%s UNLOAD lower limit was set to 0.\n", warningMsg)
	}
	if unloadUpper > 1.0 {
		unloadUpper = 1.0
		show("%s UNLOAD upper limit was set to 1.\n", warningMsg)
	}
	if unloadLower >= unloadUpper {
		unloadLower = lowerDefault
		unloadUpper = upperDefault
		show("%s UNLOAD fitting range was changed to default.\n", warningMsg)
	}

// deoth and force column, skip line
	depthColumn := strings.Split(fallback(roots.Config.DepthColumn, ""), ",")
	for i,s := range depthColumn {
		depthColumn[i] = strings.ToUpper(strings.TrimSpace(s))
	}
	forceColumn := strings.Split(fallback(roots.Config.ForceColumn, ""), ",")
	for i,s := range forceColumn {
		forceColumn[i] = strings.ToUpper(strings.TrimSpace(s))
	}
	skipLine := atoi(fallback(roots.Config.SkipLine, "0"))

// depth and force factor
	depthFactor := atof(fallback(roots.Config.DepthFactor, "1.0"))
	forceFactor := atof(fallback(roots.Config.ForceFactor, "1.0"))

// set CF Mode
	cfMode := fallback(roots.Config.CfMode, cfModeList[0])
	if searchString(cfModeList, cfMode) < 0 {
		show("%s CF Method '%s' is not supported.\n", warningMsg, cfMode)
		cfMode = cfModeList[0]
	}
	if cfModeOverride >= 0 && cfModeOverride < len(cfModeList) {
		cfMode = cfModeList[cfModeOverride]
	}

// set Young's Modulus Mode
	youngMode := fallback(roots.Config.YoungMode, youngModeList[0])
	if searchString(youngModeList, youngMode) < 0 {
		show("%s Young's Modulus Method '%s' is not supported.\n", warningMsg, youngMode)
		youngMode = youngModeList[0]
	}
	if youngModeOverride >= 0 && youngModeOverride < len(youngModeList) {
		youngMode = youngModeList[youngModeOverride]
	}

// set Hardness Mode
	hardnessMode := fallback(roots.Config.HardnessMode, hardnessModeList[0])
	if searchString(hardnessModeList, hardnessMode) < 0 {
		show("%s Hardness Method '%s' is not supported.\n", warningMsg, hardnessMode)
		hardnessMode = hardnessModeList[0]
	}
	if hardnessModeOverride >= 0 && hardnessModeOverride < len(hardnessModeList) {
		hardnessMode = hardnessModeList[hardnessModeOverride]
	}

// XML name
	if DebugFlags[UseXMLProperName] {
		show("XML, %s\n", filepath.Clean(fallback(roots.Config.ProperName, "omitted")))
	}

// print information
	show("Memo, %s\n", fallback(roots.Config.Name, "N/A"))
	show("Beta, %s\n", ftoa(beta))
	show("Unload, %s, %s\n", ftoa(unloadLower), ftoa(unloadUpper))
	show("%s, %.3g, %q\n", strings.Title(depthWord), depthFactor, strings.Join(depthColumn, ","))
	show("%s, %.3g, %q\n", strings.Title(forceWord), forceFactor, strings.Join(forceColumn, ","))
	show("Skip, %d\n", skipLine)
	show("Skip Bad Curve, %v", SkipBadCurve)
	if SkipBadCurve == true {show(notDefaultMsg)}
	show("\nCF Mode, %s", cfMode)
	if cfMode !=cfModeList[0] {show(notDefaultMsg)}
	show("\nYoung's Modulus Mode, %s", youngMode)
	if youngMode !=youngModeList[0] {show(notDefaultMsg)}
	show("\nHardness Mode, %s", hardnessMode)
	if hardnessMode !=hardnessModeList[0] {show(notDefaultMsg)}
	show("\n")
	if DebugFlags[DebugInterpolateAh] {
		show ("%s Interpolation (for debug) is on for A(h)%s\n", warningMsg, notDefaultMsg)
	}
	if DebugFlags[DebugInterpolateCh] {
		show ("%s Interpolation (for debug) is on for CH%s\n", warningMsg, notDefaultMsg)
	}

// read command text
	commandFile := filepath.Join(filepath.Dir(os.Args[0]), "commands", "*.txt")
	fileMatched, _ := filepath.Glob(commandFile)
	for _, f := range fileMatched {ReadCommands(f)}

//	read curves
	curve := NewCurve()
	curve.SetUnloadFit(unloadLower, unloadUpper)
	curve.SetColumn(depthColumn, forceColumn, skipLine)
	if curve.HasWrongColumn() {curve.SetColumn([]string {}, []string {}, 0)} // use SetAutoColumn
	curve.SetFactor(depthFactor, forceFactor)
	roots.Config.Folder = replaceHome(roots.Config.Folder)
	forceClassAll := NewClassification()

// read all curves first
	show("# Curve\n")
	var specCurves []Curves = make([]Curves, MaxSpecimen)
	for spec, d := range roots.Datasets {
		d.Folder = replaceHome(d.Folder)
		var curves Curves
		for _, fileWild := range d.File {
			curves = append(curves, curve.Open(folder, roots.Config.Folder, d.Folder, string(fileWild))...)
		}
		specCurves[spec] = curves
		for _, sub := range specCurves[spec].ClassByForce().classes {
			forceClassAll.Add(sub.stat.Ave())
		}
	}

// Show curves
	if ShowForceDepth || ShowChart {
		showError("CL, %s\n",forceClassAll)
		NoData := true
		if ShowForceDepth {
		show("count, hmax, %smax, [%s Column], %[2]s Factor, [%s Column], %[3]s Factor, Skip Line, Filename, (Sheet No.)\n", forceVar, strings.Title(depthWord), strings.Title(forceWord))
		}
		for spec, d := range roots.Datasets {
			specimen := d.Specimen
			forceClass := specCurves[spec].ClassByForce()
			for _, c := range specCurves[spec] {
				if c.IsValid() {
					NoData = false
					if ShowForceDepth {
						show("%s\n", c.Detail())
					}
					classGlobal := forceClassAll.Search(forceClass.Round(c.MaxF))
					label := fmt.Sprintf("%s, %3d", specimen, classGlobal)
					showError(c.PlotData(label))
				}
			}
		}
		if NoData {showError("%s No data\n", errorMsg)}
	}
	if NoAnalysis {return}

// Check bad curve after ForceDepth plot
	for spec, d := range roots.Datasets {
		specimen := d.Specimen
		specCurves[spec] = specCurves[spec].CheckBadCurve(specimen)
	}

// Summary of specimen
	if ShowSummary {
		show("%s\nspecimen, curve, %s\n", summaryMsg, forceWord)
		for spec, d := range roots.Datasets {
			specimen := d.Specimen
			forceClass := specCurves[spec].ClassByForce()
			show("%s, %d, %s\n", specimen, len(specCurves[spec]), forceClass)
		}
	}

// find optimum cf
	show("# CF\n")
	cfstat := NewStat()
	for spec, d := range roots.Datasets {
		if len(d.CF) > 0 {
			specimen := d.Specimen
			f := NewFindCF()
			f.SetMethod(cfMode)
			for _, c := range specCurves[spec] {
				if c.IsValid() {
					f.AddCurve(c)
					if Debug {show("%s\n", c)}
				}
			}
			if ShowChart {
				showError("%s", f.PlotData(specimen, specCurves[spec].ClassByForce(), forceClassAll))
			}
			if f.IsValid() {
				cfstat.Add(f.OptCF())
				show("CF, %s, %.3f\n", specimen, f.OptCF())
			} else {
				show("%s (plaease remove wrong data or add more curves for %s)\n", errorMsg, specimen)
				show("CF, %s, ignored\n", specimen)
			}
		}
	}
	var cf float64 = 0.0
	if cfstat.Count > 0 {
		cf = cfstat.Ave()
		show("CF, (averaged), %.3f\n", cf)
		if ShowChart {
			showError("CV, MeanAll, %.3f\n", cf)
		}
	} else {
		show("CF, (NOT calculated), %.3f\n", cf)
	}
	if len(roots.Config.CF) > 0 {
		cf = atof(roots.Config.CF)
		show("CF, (specified), %s\n", roots.Config.CF)
	}

// calc area function with reference
	show("# Area function\n")
	areafunc := NewAlterFit()
// d[] = {h, fmax, hmax, hr, cf, elastic}
// log(y) = a * log(x) ^ b + c
	areafunc.SetXRaw(func(d Point) (float64) {return math.Log(d[0] * areaFuncConstant1)})
	areafunc.SetXBFunc(func(f float64, b float64) (float64) {return math.Pow(f, b)})
	areafunc.SetXFunc(func(d Point, b float64) (float64) {return math.Pow(math.Log(d[0] * areaFuncConstant1), b)})
	areafunc.SetYFunc(func(d Point) (float64) {return math.Log(math.Sqrt(math.Pi) / 2.0 / beta * d[5] * d[1] / (d[2] - d[1] * d[4] / areaFuncDepth - d[3])* areaFuncConstant2)})
	areafunc.SetYInverse(func(f float64) (float64) {return math.Exp(f) / areaFuncConstant2})
	areafunc.SetClassValue(func(d Point) (float64) {return d[1]})
	areafunc.SetBFactor(2.4)
	var areaSeriesDummy Series = make([]Point, 0)
	areaSeries := &areaSeriesDummy

// read material data
	matdata := NewMaterialData()
	matdata.Add("diamond", 1140, 0.07)
	matdata.ReadMaterials(filepath.Join(filepath.Dir(os.Args[0]), "materials.txt"))
	indenterMaterial := fallback(roots.Config.IndenterMaterial, "diamond")
	indenter := matdata.Refer(indenterMaterial)
	//show("%s\n", indenter)

// reference material
	for spec, d := range roots.Datasets {
		if d.Reference > "" {
			specimen := d.Specimen
			reference := matdata.Refer(specimen)
			reference.SetPoissonStr(d.Poisson)
			reference.SetYoungStr(d.Young)
			if !reference.IsValid() {
				show("%s Wrong material properties, %s\n", errorMsg, reference)
				break
			}
			show("Reference, %s\n", reference)

			for _, c := range specCurves[spec] {
				if c.IsValid() {
					series := c.DataForAlterFit(youngMode, cf, indenter.ElasticParam() + reference.ElasticParam())
					areafunc.Add(series)
					if Debug {show("areafunc +data, %s\n", c)}
					x := areafunc.xRaw(series)
					y := areafunc.yFunc(series)
					*areaSeries = append(*areaSeries, Point {x, y, c.MaxF})
					if Debug {show("areaSeries +data, %s, %s\n", ftoa(x), ftoa(y))}
				}
			}
			if ShowChart {
				showError("%s", areafunc.PlotData("xY", "Ah", specimen, forceClassAll))
				showError("%s", areafunc.PlotData("xF", "Ah", fitSymbol, forceClassAll))
			}
		}
	}

// give up to next setting xml if area function is invalid
	if !areafunc.IsValid() {
		show("%s area function, N = %d\n%s\n", errorMsg, areafunc.Count(), areafunc.PlotData("", "areafunc-plot", "", forceClassAll))
		continue
	}
	show("AreaFunc, %s, %s, %s, %s\n", ftoa(areafunc.A()), ftoa(areafunc.B()), ftoa(areafunc.C()), ftoa(areafunc.CorrSquared()))
	if ShowChart {showError("%s", areafunc.PrintParam("AP"))}
	areaSeries.Sort(0)
	areaTable := areaSeries.ClassAverage(2)
	if Debug {show("areaTable = %v\n", areaTable)}

// calc Young's modulus
	show("# Young's modulus & Indentation Hardness\n")
	if ShowResult {
		show("specimen, E, H_IT(%s), Poisson, %smax, hr, hmax, hmax-cor, file\n", youngMode, forceVar)
	}
	summaryE := NewSummary()
	summaryHIT := NewSummary()
	for spec, d := range roots.Datasets {
		specimen := d.Specimen
		material := matdata.Refer(specimen) // Poisson := database or PoissonDefault
		material.SetPoissonStr(d.Poisson)
		p := material.Poisson
		if !IsValidPoisson(p) { // for sure
			show("%s Wrong Poisson's ratio: %s, using %s\n", errorMsg, ftoa(p), ftoa(PoissonDefault))
			p = PoissonDefault
		}
		forceClass := specCurves[spec].ClassByForce()
		for _, c := range specCurves[spec] {
			if c.IsValid() {
				series := c.DataForAlterFit(youngMode, cf, indenter.ElasticParam())
				calcedY := areafunc.Y(series)
				if DebugFlags[DebugInterpolateAh] {
					x := areafunc.xRaw(series)
					if Debug {show("areaTable %s\n", ftoa(x))}
					if areaSeries.InRange(0, x) {
						calcedY = areafunc.yInverse(areaTable.Interpolate(0, 1, x))
					}
				}
				e := c.Eit(calcedY, cf, indenter.ElasticParam(), p)
				h := c.Hit(calcedY)
				if !math.IsNaN(e) && !math.IsNaN(h) {
					if ShowChart {
						classGlobal := forceClassAll.Search(forceClass.Round(c.MaxF))
						showError("ET, %s, %3d, %10.3f, %10.3f\n", specimen, classGlobal, c.Hr(), e)
						showError("HT, %s, %3d, %10.3f, %10.3f\n", specimen, classGlobal, c.Hr(), h)
						eta := c.Eta(cf)
						if eta > 0.0 {
							showError("EL, %s, %3d, %10.3f, %10.3f\n", specimen, classGlobal, c.Hr(), eta)
						}
					}
					if ShowResult {
						show("%s, %s, %s, %s, %s, %s, %s, %s, %s\n",
						specimen, ftoa(e), ftoa(h), ftoa(p), ftoa(c.MaxF), 
						ftoa(c.Hr()), ftoa(c.Maxh), ftoa(c.MaxhCorr(cf)), c.FileName())
					}
					summaryE.Add(d.Specimen, c.MaxF, e)
					summaryHIT.Add(d.Specimen, c.MaxF, h)
				}
			}
		}
	}
	if ShowSummary {
		show("%s\nYoung's modulus, curves, Min(%s), Max(%[2]s), Median(E), Mean(E)\n%s", summaryMsg, forceVar, summaryE)
		show("%s\nIndentation Hardness, curves, Min(%s), Max(%[2]s), Median(H_IT), Mean(H_IT)\n%s", summaryMsg, forceVar, summaryHIT)
	}

// calc alternative Young's modulus
	show("# E' (alternative Young's modulus) fit\n")
	alterfit := NewAlterFit()
// d[] = {se, c.HrHmaxRatio(cf), reference.Young}
// log(y) = a * log(x1 / x2^b) + c
	alterfit.SetXRaw(func(d Point) (float64) {return 0})
	alterfit.SetXBFunc(func(f float64, b float64) (float64) {return 0})
	alterfit.SetXFunc(func(d Point, b float64) (float64) {return math.Log(d[0] / math.Pow(d[1], b))})
	alterfit.SetYFunc(func(d Point) (float64) {return math.Log(d[2])})
	alterfit.SetYInverse(func(f float64) (float64) {return math.Exp(f)})
	alterfit.SetClassValue(func(d Point) (float64) {return math.Log(d[2])})
	referenceCount := 0
	for spec, d := range roots.Datasets {
		if d.Poiref > "" {
			specimen := d.Specimen
			reference := matdata.Refer(specimen)
			reference.SetPoissonStr(d.Poisson)
			reference.SetYoungStr(d.Young)
			if !reference.IsValid() {
				show("%s Wrong material properties, %s\n", errorMsg, reference)
				continue
			}
			referenceCount += 1
			show("E' Reference, %s\n", reference)
			for _, c := range specCurves[spec] {
				if c.IsValid() {
					arearoot := areafunc.Y(c.DataForAlterFit(youngMode, cf, indenter.ElasticParam()))
					elastic := c.ElasticParam(cf, arearoot*arearoot)
					se := 1/(elastic - indenter.ElasticParam())
					x := Point {se, c.HrHmaxRatio(cf), reference.Young}
					alterfit.Add(x)
					if Debug {
						show("E' data, %f, %f, %f, %f\n", arearoot, elastic, se, reference.Young)
					}
				}
			}
		}
	}

	switch referenceCount {
		case 0:
// Do nothing
		case 1:
			show("%s Only %d reference specimen for alternative analysis. At lease 2 needed.\n",
			errorMsg, referenceCount)
		case 2:
			show("%s Only %d reference specimens for alternative analysis. 3+ recommended.\n",
			warningMsg, referenceCount)
	}

	if alterfit.IsValid() && referenceCount > 1 {
		show("E' function, %s, %s, %s, %s\n", ftoa(alterfit.A()), ftoa(alterfit.B()), ftoa(alterfit.C()), ftoa(alterfit.CorrSquared()))
		if ShowChart {
			showError("%s", alterfit.PlotData("XY", "AE", "references", NewClassification()))
			showError("%s", alterfit.PlotData("XF", "AE", fitSymbol, NewClassification()))
		}
		show("# E' (alternative Young's modulus)\n")
		if ShowResult {
			show("specimen, E, Poisson, %smax, hr, hmax, hmax-cor, file\n", forceVar)
		}
		summary := NewSummary()
		for spec, d := range roots.Datasets {
			specimen := d.Specimen
			forceClass := specCurves[spec].ClassByForce()
			for _, c := range specCurves[spec] {
				if c.IsValid() {
					arearoot := areafunc.Y(c.DataForAlterFit(youngMode, cf, indenter.ElasticParam()))
					elastic := c.ElasticParam(cf, arearoot*arearoot)
					se := 1/(elastic - indenter.ElasticParam())
					x := Point {se, c.HrHmaxRatio(cf)}
					y := alterfit.Y(x)
					p := math.Sqrt(1.0 - y / se)
					if !math.IsNaN(y) && !math.IsNaN(p) {
						if ShowChart {
							classGlobal := forceClassAll.Search(forceClass.Round(c.MaxF))
							showError("EA, %s, %3d, %10.3f, %10.3f\n", specimen, classGlobal, c.Hr(), y)
							showError("PO, %s, %3d, %10.3f, %10.3f\n", specimen, classGlobal, c.Hr(), p)
						}
						if ShowResult {
							show("%s, %s, %s, %s, %s, %s, %s, %s\n", 
			 				specimen, ftoa(y), ftoa(p), ftoa(c.MaxF), ftoa(c.Hr()), ftoa(c.Maxh), ftoa(c.MaxhCorr(cf)), c.FileName())
						}
						summary.Add(d.Specimen, c.MaxF, y, p)
					}
				}
			}
		}
		if ShowSummary {
			show("%s\nE', curves, Min(%s), Max(%[2]s), Median(E), Mean(E), Median(Poisson), Mean(Poisson)\n%s", summaryMsg, forceVar, summary)
		}
	}

// calc hardness
// d[] = {cDepth, chinverse, fmax}
// y = a x^(-b) + c
	show("# Hardness (Heq) Reference\n")
	var hardnessSeriesDummy Series = make([]Point, 0)
	hardnessSeries := &hardnessSeriesDummy
	hardnessfunc := NewAlterFit()
	hardnessfunc.SetXRaw(func(d Point) (float64) {return d[0]})
	hardnessfunc.SetXBFunc(func(f float64, b float64) (float64) {return math.Pow(f, -b)})
	hardnessfunc.SetXFunc(func(d Point, b float64) (float64) {return math.Pow(d[0], -b)})
	hardnessfunc.SetYFunc(func(d Point) (float64) {return d[1]})
	hardnessfunc.SetYInverse(func(f float64) (float64) {return f})
	hardnessfunc.SetClassValue(func(d Point) (float64) {return d[2]})
	referencehardness := 0.0
// calc Conversion factor with hardness reference
	for spec, d := range roots.Datasets {
		if d.Hardref > "" {
			specimen := d.Specimen
			if len(d.Hardness) > 0 {referencehardness = atof(d.Hardness)}
			if referencehardness <= 0 {
				show("%s hardness not properly specified for %s: %s\n", warningMsg, specimen, d.Hardness)
				break
			}
			show("Hardness Reference, %s, %s\n", specimen, ftoa(referencehardness))
			if ShowChart {
				showError("HR, %s, %s\n", specimen, d.Hardness)
			}
			for _, c := range specCurves[spec] {
				if c.IsValid() {
					cDepth := c.ChosenDepth(hardnessMode, cf)
					if CanCalcHardness(c.MaxF, cDepth) {
						chinverse := c.ChosenHardness(hardnessMode, cf) / referencehardness
						x := Point {cDepth, chinverse, c.MaxF}
						hardnessfunc.Add(x)
						*hardnessSeries = append(*hardnessSeries, x)
						if Debug {
							show("hardnessfunc, %s, %s, %s\n",
								c, ftoa(cDepth), ftoa(c.ChosenHardness(hardnessMode, cf)))}
						}
					}
				}

// Use HardFit() tuned for C_H fitting instead of AlterFit() fitting
				x1, x2, x3 := HardFit(hardnessSeries)
				hardnessfunc.SetParam(x1, x2, x3)
				if ShowChart {
					showError("%s", hardnessfunc.PlotData("xY", "HC", specimen, forceClassAll))
					showError("%s", hardnessfunc.PlotData("xF", "HC", fitSymbol, forceClassAll))
				}
//		if ShowChart {showError("%s", hardnessfunc.PlotData("xY", "Hh", specimen))}
			}
		}

		if hardnessfunc.IsValid() {
			show("HardnessFunc, %s, %s, %s, %s, %s\n", hardnessMode, ftoa(hardnessfunc.A()),
			ftoa(hardnessfunc.B()), ftoa(hardnessfunc.C()), ftoa(hardnessfunc.CorrSquared()))
			hardnessSeries.Sort(0)
			hardnessTable := hardnessSeries.ClassAverage(2)
// show("HardnessSeries, %v\n", *hardnessSeries)
// show("HardnessTable, %v\n", *hardnessTable)
			if ShowChart {showError("%s", hardnessfunc.PrintParam("Hf"))}

// calc Hardness
			show("# Hardness (Heq)\n")
			if ShowResult {
				show("specimen, H, H/Href, hGeo, %smax, hr, hmax, hmax-cor, file\n", forceVar)
			}
			summary := NewSummary()
			for spec, d := range roots.Datasets {
				specimen := d.Specimen
				forceClass := specCurves[spec].ClassByForce()
				for _, c := range specCurves[spec] {
					if c.IsValid() {
						cDepth := c.ChosenDepth(hardnessMode, cf)
						if CanCalcHardness(c.MaxF, cDepth) {
							x := Point {cDepth}
							hardnessRaw := c.ChosenHardness(hardnessMode, cf)
							h := hardnessRaw / (hardnessfunc.Y(x))
							if DebugFlags[DebugInterpolateCh] {
								if hardnessSeries.InRange(0, x[0]) {
									h = hardnessRaw / hardnessTable.Interpolate(0, 1, x[0])
								}
							}
							hardnessRatio := h / referencehardness
							if !math.IsNaN(h) {
								if ShowChart {
									classGlobal := forceClassAll.Search(forceClass.Round(c.MaxF))
									showError("HI, %s, %3d, %10.3f, %10.3f\n", 
										specimen, classGlobal, cDepth, hardnessRaw)
									showError("HQ, %s, %3d, %10.3f, %10.3f\n",
										specimen, classGlobal, cDepth, h)
								}
								if ShowResult {
									show("%s, %s, %s, %s, %s, %s, %s, %s, %s\n",
									specimen, ftoa(h), ftoa(hardnessRatio), ftoa(c.HGeo(cf)), ftoa(c.MaxF),
									ftoa(c.Hr()), ftoa(c.Maxh), ftoa(c.MaxhCorr(cf)), c.FileName())
								}
								summary.Add(d.Specimen, c.MaxF, h)
							}
						}
					}
				}
			}
			if ShowSummary {
				show("%s\nHardness, curves, Min(%s), Max(%[2]s), Median(H), Mean(H)\n%s",
				summaryMsg, forceVar, summary)
			}
		}
		show("# End\n")
	}
}
